# MicroAI is deprecated and superseded by Qualia
Please check the new Qualia repositories out at https://github.com/orgs/LEAT-EDGE/repositories and the documentation at https://leat-edge.github.io/qualia/ .

# MicroAI
End-to-end training, quantization and deployment framework for deep neural networks on microcontrollers.

Repository should be cloned with `--recursive` to get TensorFlow (for TFLite Micro deployment) and KerasCNN2C along with their dependencies.

## Dependencies

System:
```
python>=3.8.0
```

Python:
```
numpy
scikit-learn
tomlkit==0.4.0
colorful
gitpython
```

### Dataset

#### GTSRB

Python:
```
imageio
scikit-image
```

### Training

#### TensorFlow

Python:
```
tensorflow
tensorflow_addons
```

#### PyTorch

Python:
```
pytorch
pytorch_lightning==1.2.8
```

### Deployment

#### Embedded targets

##### SparkFun Edge 

Python:
```
pycryptodome
```

##### Nucleo-L452RE-P
System:
```
stm32cubeide
stm32cubeprog
```

#### Embedded frameworks

##### STM32Cube.AI

STM32CubeIDE extension pack:
```
X-CUBE-AI == 5.2.0
```

##### TensorFlow Lite Micro

System:
```
arm-none-eabi-binutils
arm-none-eabi-gcc
arm-none-eabi-newlib
libopenexr-dev
```

##### KerasCNN2C
Python:
```
jinja2
```

System:
```
arm-none-eabi-binutils
arm-none-eabi-gcc
arm-none-eabi-newlib
```

### Evaluation
Python:
```
pyserial
```

## Usage

If MicroAI installed with pip, you can run the `microai` command directly. Otherwise run `PYTHONPATH=. ./bin/microai <config.toml> <action>` from the microai directory.

### Dataset pre-processing
```
microai <config.toml> preprocess_data
```

### Training
```
microai <config.toml> train
```

### Prepare deployment (generate firmware)
```
microai <config.toml> prepare_deploy
```

### Deploy and evaluate
```
microai <config.toml> deploy_and_evaluate
```

### Run test suite
```
PYTHONHASHSEED=2 python -m unittest discover microai/tests
```


## Example

### UCI-HAR training and deployment onto Nucleo-L452RE-P for int16 inference

- Download the UCI-HAR dataset from https://archive.ics.uci.edu/ml/machine-learning-databases/00240/UCI%20HAR%20Dataset.zip .
- Extract it inside the `data/` directory.
- Run data pre-processing
    `microai conf/uci-har/ResNetv1_float32_train.toml preprocess_data`

    This will run the various pre-processing steps in the configuration and generate serialized numpy arrays in `out/data/UCI_HAR_raw/`.

- Run float32 training:
    `microai conf/uci-har/ResNetv1_float32_train.toml train`

    This will train 7 different networks 15 times in a row according to the configuration. Networks can be disabled by setting their `disabled` parameter to `true` and number of runs can be reduced with `last_run` in the configuration.

- Prepare deployment for Nucleo-L452RE-P:
    `microai conf/uci-har/ResNetv1_KerasCNN2C_NucleoL452REP_int16.toml prepare_deploy`

    This will perform Post-Training Quantization for int16, generate the source code for the C inference library in `out/kerascnn2c_fixed/`, and build the firmware in `out/deploy/NucleoL452REP/`.

- Deploy and evaluate for Nucleo-L452RE-P:
    `microai conf/uci-har/ResNetv1_KerasCNN2C_NucleoL452REP_int16.toml deploy_and_evaluate`

    This will deploy the firmware onto the Nucleo-L452RE-P board connected to the computer and send vectors from the test dataset one by one for inference.

## Included support for datasets, learning framework, neural networks, embedded frameworks and targets

### Datasets
- [UCI HAR](https://archive.ics.uci.edu/ml/datasets/human+activity+recognition+using+smartphones)
- [GTSRB](https://benchmark.ini.rub.de/)
- [Written and Spoken MNIST](https://zenodo.org/record/3515935)

### Learning frameworks
- TensorFlow.Keras
- PyTorch

### Neural networks
- MLP
- CNN (1D&2D)
- Resnetv1 (1D&2D)

### Embedded frameworks
- STM32Cube.AI
- TensorFlow Lite for Microcontrollers
- KerasCNN2C

### Targets
- Nucleo-L452RE-P
- SparkFun Edge

## Reference & Citation

[MicroAI, a software framework for end-to-end deep neural networks training, quantization and deployment onto embedded devices](https://doi.org/10.5281/zenodo.5507397), Pierre-Emmanuel Novac, Alain Pegatoquet and Benoît Miramond, version 1.0, 2021.

```
@software{microaisoftware,
  author       = {Novac, Pierre-Emmanuel and Pegatoquet, Alain and Miramond, Benoît},
  title        = {{MicroAI, a software framework for end-to-end deep neural networks training, quantization and deployment onto embedded devices}},
  month        = sept,
  year         = 2021,
  publisher    = {Zenodo},
  version      = {1.0},
  doi          = {10.5281/zenodo.5507397},
  url          = {https://doi.org/10.5281/zenodo.5507397}
}
```

[Quantization and Deployment of Deep Neural Networks on Microcontrollers](https://www.mdpi.com/1424-8220/21/9/2984), Pierre-Emmanuel Novac, Ghouthi Boukli Hacene, Alain Pegatoquet, Benoît Miramond and Vincent Gripon, Sensors, 2021.

```
@article{microaiarticle,
	author = {Novac, Pierre-Emmanuel and Boukli Hacene, Ghouthi and Pegatoquet, Alain and Miramond, Benoît and Gripon, Vincent},
	title = {Quantization and Deployment of Deep Neural Networks on Microcontrollers},
	journal = {Sensors},
	volume = {21},
	year = {2021},
	number = {9},
	article-number = {2984},
	url = {https://www.mdpi.com/1424-8220/21/9/2984},
	issn = {1424-8220},
	doi = {10.3390/s21092984}
}
```
