from pathlib import Path
import shutil
from collections import namedtuple
from microai.utils.process import subprocesstee

from microai.deployment.kerascnn2c import SparkFunEdge as SparkFunEdgePerLayer

class SparkFunEdge(SparkFunEdgePerLayer):
    def __init__(self, *args, **kwargs):
        super().__init__(
                    modeldir=Path('out')/'kerascnn2c_fixed',
                    projectdir=Path('third_party')/'kerascnn2c_fixed'/'examples'/'SparkFunEdge',
                    outdir=Path('out')/'deploy'/'SparkFunEdge',
                    *args, **kwargs)
