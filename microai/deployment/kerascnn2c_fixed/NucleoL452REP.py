from pathlib import Path
import shutil
from microai.utils.process import subprocesstee


from microai.deployment.kerascnn2c import NucleoL452REP as NucleoL452REPPerLayer

class NucleoL452REP(NucleoL452REPPerLayer):

    def __init__(self, *args, **kwargs):
        super().__init__(projectdir=Path('third_party')/'kerascnn2c_fixed'/'examples'/'KerasCNN2C-NucleoL452REP',
                    *args, **kwargs)
