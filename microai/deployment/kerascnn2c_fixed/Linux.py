from pathlib import Path
import shutil
from collections import namedtuple
from microai.utils.process import subprocesstee

from microai.deployment.kerascnn2c import Linux as LinuxPerLayer

class Linux(LinuxPerLayer):
    import microai.evaluation.host.MicroAI as evaluator # Suggested evaluator

    def __init__(self, cxxflags: list=None):
        super().__init__(cxxflags=cxxflags,
                         modeldir = Path('out')/'kerascnn2c_fixed',
                         projectdir=Path('third_party')/'kerascnn2c_fixed'/'examples'/'Linux',
                         outdir = Path('out')/'deploy'/'Linux')
