from pathlib import Path
import shutil
from microai.utils.process import subprocesstee

from microai.deployment.toolchain import STM32CubeIDE

class NucleoL452REP(STM32CubeIDE):
    import microai.evaluation.target.MicroAI as evaluator # Suggested evaluator

    def __init__(self,
                    projectdir=Path('third_party')/'kerascnn2c'/'examples'/'KerasCNN2C-NucleoL452REP',
                    *args, **kwargs):
        super().__init__(projectname='KerasCNN2C-NucleoL452REP',
                    projectdir=projectdir,
                    *args, **kwargs)

        self.__model_data = self._projectdir/'Core'/'Inc'/'model.h'

    def __write_model(self, model):
        with self.__model_data.open('w') as f:
            f.write(model.h)

    def prepare(self, tag, model, optimize: str, compression: int):
        if optimize:
            raise ValueError(f'No optimization available for {self.__class__.__name__}')
        if compression != 1:
            raise ValueError(f'No compression available for {self.__class__.__name__}')

        self._create_outdir()
        self._clean_workspace()
        self.__write_model(model=model)

        if not self._build(tag=tag):
            return None
        self._copy_buildproduct(tag=tag)
        return self
