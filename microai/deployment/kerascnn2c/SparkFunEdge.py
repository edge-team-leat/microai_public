from pathlib import Path
import shutil
from collections import namedtuple
from microai.utils.process import subprocesstee

class SparkFunEdge:
    import microai.evaluation.target.MicroAI as evaluator # Suggested evaluator

    def __init__(self,
                dev: str='/dev/ttyUSB0',
                cxxflags: list=None,
                modeldir = Path('out')/'kerascnn2c',
                projectdir = Path('third_party')/'kerascnn2c'/'examples'/'SparkFunEdge',
                outdir = Path('out')/'deploy'/'SparkFunEdge'):
        self.__dev = dev
        self.__modeldir = modeldir
        self.__projectdir = projectdir
        self.__outdir = outdir

    def __run(self, cmd, *args):
        print(cmd, *args)
        returncode, outputs = subprocesstee.run(str(cmd), *args)
        return returncode == 0

    def __create_outdir(self):
        self.__outdir.mkdir(parents=True, exist_ok=True)

    def __clean(self, tag: str, model):
        modeldir = self.__modeldir/model.name
        outdir = self.__outdir/tag
        return self.__run('make',
                          '-C', str(self.__projectdir),
                          f'MODELDIR={modeldir.absolute()}',
                          f'OUT={outdir.absolute()}',
                          'clean'
                        )

    def __build(self, tag: str, model):
        modeldir = self.__modeldir/model.name
        outdir = self.__outdir/tag
        return self.__run('make',
                          '-C', str(self.__projectdir),
                          f'MODELDIR={modeldir.absolute()}',
                          f'OUT={outdir.absolute()}'
                        )

    def __upload(self, tag: str):
        outdir = self.__outdir/tag
        return self.__run('make',
                          '-C', str(self.__projectdir),
                          f'OUT={outdir.absolute()}',
                          f'SERIAL_PORT={self.__dev}',
                          'bootload'
                        )

    def prepare(self, tag, model, optimize: str, compression: int):
        if optimize:
            raise ValueError(f'No optimization available for {self.__class__.__name__}')
        if compression != 1:
            raise ValueError(f'No compression available for {self.__class__.__name__}')

        print('model:', model)

        self.__create_outdir()

        self.__clean(tag=tag, model=model)
        if not self.__build(tag=tag, model=model):
            return None
        return self

    def deploy(self, tag):
        input('Put target in programming mode and press Enter…')
        self.__upload(tag=tag)

        return namedtuple('Deploy', ['rom_size', 'evaluator'])(self.__rom_size(tag), self.evaluator)

    def __rom_size(self, tag):
        return (self.__outdir/tag/'main.bin').stat().st_size

