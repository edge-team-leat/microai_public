from pathlib import Path
import shutil
from collections import namedtuple
from microai.utils.process import subprocesstee

class Linux:
    import microai.evaluation.host.MicroAI as evaluator # Suggested evaluator

    def __init__(self,
                cxxflags: list=None,
                modeldir = Path('out')/'kerascnn2c',
                projectdir = Path('third_party')/'kerascnn2c'/'examples'/'Linux',
                outdir = Path('out')/'deploy'/'Linux'):
        self.__cxxflags = cxxflags or ['-ftrapv', '-Wall', '-Wextra', '-Werror=double-promotion',
                                       '-pedantic', '-Ofast', '-ffunction-sections', '-fdata-sections']
        self.__modeldir = modeldir
        self.__projectdir = projectdir
        self.__outdir = outdir

    def __run(self, cmd, *args):
        print(cmd, *args)
        returncode, outputs = subprocesstee.run(str(cmd), *args)
        return returncode == 0

    def __create_outdir(self):
        self.__outdir.mkdir(parents=True, exist_ok=True)

    def __build(self, tag: str, model):
        modeldir = self.__modeldir/model.name
        return self.__run('g++',
                          '-o', str(self.__outdir/f'{tag}_Linux'),
                           str(self.__projectdir/'main.cpp'),
                           str(modeldir/'model.c'),
                           f'-I{modeldir}',
                          *self.__cxxflags,
                        )

    def prepare(self, tag, model, optimize: str, compression: int):
        if optimize:
            raise ValueError(f'No optimization available for {self.__class__.__name__}')
        if compression != 1:
            raise ValueError(f'No compression available for {self.__class__.__name__}')

        self.__create_outdir()

        if not self.__build(tag=tag, model=model):
            return None
        return self

    def deploy(self, tag):
        print(self.__class__.__name__, 'Info: running locally, nothing to deploy')

        return namedtuple('Deploy', ['rom_size', 'evaluator'])(self.__rom_size(tag), self.evaluator)

    def __rom_size(self, tag):
        return (self.__outdir/f'{tag}_Linux').stat().st_size

