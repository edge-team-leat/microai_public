from abc import ABC, abstractmethod
from pathlib import Path
import shutil
import sys
from collections import namedtuple
from microai.utils.process import subprocesstee

class STM32CubeIDE(ABC):

    def __init__(self,
        projectname: str,
        projectdir: Path,
        buildtype: str='Release',
        outdir: Path=Path('out')/'deploy'/'STM32CubeIDE'):

        self._projectname = projectname
        self._buildtype = buildtype
        self._workspacedir = outdir/'workspace'
        self._projectdir = projectdir

        self._outdir = outdir

        self.__stm32cubeide_dir = Path('/opt')/'stm32cubeide'
        self.__stm32cubeide_bin = self.__stm32cubeide_dir/'stm32cubeide'

        # STM32CubeProgrammer bundled with STM32CubeIDE 1.6.0 cannot reset MCU, use system-wide STM32CubeProgrammer 2.7.0
        self.__stm32cubeprogrammer_bin = Path('/')/'usr'/'bin'/'STM32_Programmer_CLI'
        #self.__stm32cubeprogrammer_bin = next((self.__stm32cubeide_dir/'plugins').glob('com.st.stm32cube.ide.mcu.externaltools.cubeprogrammer.linux64_*'))/'tools'/'bin'/'STM32_Programmer.sh'

    def _run(self, cmd, *args):
        print(cmd, *args)
        returncode, outputs = subprocesstee.run(str(cmd), *args)
        return returncode == 0

    def _create_outdir(self):
        self._outdir.mkdir(parents=True, exist_ok=True)
        self._workspacedir.mkdir(parents=True, exist_ok=True)
    
    def _clean_workspace(self):
        shutil.rmtree(self._workspacedir)

    def _build(self, tag: str):
        return self._run(self.__stm32cubeide_bin,
                            '--launcher.suppressErrors',
                            '-nosplash',
                            '-application', 'org.eclipse.cdt.managedbuilder.core.headlessbuild',
                            '-data', str(self._workspacedir),
                            '-import', str(self._projectdir),
                            '-cleanBuild', f'{self._projectname}/{self._buildtype}'
                        )

    def _copy_buildproduct(self, tag: str):
        shutil.copy(self._projectdir/self._buildtype/f'{self._projectname}.bin', self._outdir/f'{tag}.bin')

    def _upload(self, tag: str, logdir: Path):
        cmd = self.__stm32cubeprogrammer_bin
        args = ('--connect', 'port=SWD', 'mode=UR', 'reset=hwRst',
                '--download', str(self._outdir/f'{tag}.bin'), '0x08000000',
                '--verify',
                '-hardRst')

        print(cmd, *args)
        with (logdir/f'{tag}.txt').open('wb') as logfile:
            logfile.write(' '.join([str(cmd), *args, '\n']).encode('utf-8'))
            returncode, outputs = subprocesstee.run(cmd, *args, files={sys.stdout: logfile, sys.stderr: logfile})
        return returncode == 0

    @abstractmethod
    def prepare(self, tag):
        self._create_outdir()
        self._clean_workspace()

        if not self._build(tag=tag):
            return None
        self._copy_buildproduct(tag=tag)
        return self

    def deploy(self, tag):
        logdir = self._outdir/'upload'
        logdir.mkdir(parents=True, exist_ok=True)
        if not self._upload(tag, logdir=logdir):
            return None

        return namedtuple('Deploy', ['rom_size', 'evaluator'])(self.__rom_size(tag), self.evaluator)

    def __rom_size(self, tag):
        return (self._outdir/f'{tag}.bin').stat().st_size
