from unittest import TestCase
from pathlib import Path

class TestKerasCNN2CSparkFunEdge(TestCase):
    '''
        Must be executed from command line with PYTHONHASHSEED environment variable defined to the "seed" value of the "bench"
        section of the configuration file.
    '''

    def setUp(self):
        from importlib.util import spec_from_loader, module_from_spec
        from importlib.machinery import SourceFileLoader 

        spec = spec_from_loader("microai", SourceFileLoader("microai", "bin/microai"))
        self.microai = module_from_spec(spec)
        spec.loader.exec_module(self.microai)


    def test_uci_har_resnet_kerascnn2c_sparkfunedge_float32(self):
        config, configname = self.microai.parse_config(Path('conf')/'tests'/'UCI-HAR_ResNetv1_KerasCNN2C_SparkFunEdge_float32.toml')
        loggers = self.microai.main(('preprocess_data', 'train', 'prepare_deploy', 'deploy_and_evaluate'), config, configname)

        self.assertTrue('learningmodel' in loggers)
        self.assertTrue('evaluate' in loggers)

        self.assertEqual(len(loggers['learningmodel'].content), 1)
        self.assertEqual(len(loggers['evaluate'].content), 1)

        self.assertEqual(loggers['learningmodel'].content[0].name, 'uci-har_resnetv1_8')
        self.assertEqual(loggers['evaluate'].content[0].name, 'uci-har_resnetv1_8')

        self.assertEqual(loggers['learningmodel'].content[0].i, 1)
        self.assertEqual(loggers['evaluate'].content[0].i, 1)

        self.assertEqual(loggers['evaluate'].content[0].quantization, 'float32')

        self.assertEqual(loggers['learningmodel'].content[0].params, 1150)
        self.assertEqual(loggers['evaluate'].content[0].params, 1150)

        self.assertEqual(loggers['learningmodel'].content[0].mem_params, 4600)
        self.assertEqual(loggers['evaluate'].content[0].mem_params, 4600)

        self.assertGreaterEqual(loggers['learningmodel'].content[0].accuracy, 0.85)
        self.assertGreaterEqual(loggers['evaluate'].content[0].accuracy, 0.85)

        self.assertLessEqual(loggers['evaluate'].content[0].avg_time, 0.03)

        self.assertLessEqual(loggers['evaluate'].content[0].rom_size, 45000)

    def test_uci_har_resnet_kerascnn2c_sparkfunedge_int16(self):
        config, configname = self.microai.parse_config(Path('conf')/'tests'/'UCI-HAR_ResNetv1_KerasCNN2C_SparkFunEdge_int16.toml')
        loggers = self.microai.main(('preprocess_data', 'train', 'prepare_deploy', 'deploy_and_evaluate'), config, configname)

        self.assertTrue('learningmodel' in loggers)
        self.assertTrue('evaluate' in loggers)

        self.assertEqual(len(loggers['learningmodel'].content), 1)
        self.assertEqual(len(loggers['evaluate'].content), 1)

        self.assertEqual(loggers['learningmodel'].content[0].name, 'uci-har_resnetv1_8')
        self.assertEqual(loggers['evaluate'].content[0].name, 'uci-har_resnetv1_8')

        self.assertEqual(loggers['learningmodel'].content[0].i, 1)
        self.assertEqual(loggers['evaluate'].content[0].i, 1)

        self.assertEqual(loggers['evaluate'].content[0].quantization, 'int16')

        self.assertEqual(loggers['learningmodel'].content[0].params, 1150)
        self.assertEqual(loggers['evaluate'].content[0].params, 1150)

        self.assertEqual(loggers['learningmodel'].content[0].mem_params, 4600) # Model is float32 after training
        self.assertEqual(loggers['evaluate'].content[0].mem_params, 2300) # But quantized during deployment and int16 for evaluate

        self.assertGreaterEqual(loggers['learningmodel'].content[0].accuracy, 0.85)
        self.assertGreaterEqual(loggers['evaluate'].content[0].accuracy, 0.85)

        self.assertLessEqual(loggers['evaluate'].content[0].avg_time, 0.02)

        self.assertLessEqual(loggers['evaluate'].content[0].rom_size, 43000)

    def test_uci_har_resnet_kerascnn2c_sparkfunedge_int8(self):
        config, configname = self.microai.parse_config(Path('conf')/'tests'/'UCI-HAR_ResNetv1_KerasCNN2C_SparkFunEdge_int8.toml')
        loggers = self.microai.main(('preprocess_data', 'train', 'prepare_deploy', 'deploy_and_evaluate'), config, configname)

        self.assertTrue('learningmodel' in loggers)
        self.assertTrue('evaluate' in loggers)

        self.assertEqual(len(loggers['learningmodel'].content), 2) # float32 and int8 models
        self.assertEqual(len(loggers['evaluate'].content), 1)

        self.assertEqual(loggers['learningmodel'].content[0].name, 'uci-har_resnetv1_8')
        self.assertEqual(loggers['learningmodel'].content[1].name, 'uci-har_resnetv1_8_q8')
        self.assertEqual(loggers['evaluate'].content[0].name, 'uci-har_resnetv1_8_q8')

        self.assertEqual(loggers['learningmodel'].content[0].i, 1)
        self.assertEqual(loggers['learningmodel'].content[1].i, 1)
        self.assertEqual(loggers['evaluate'].content[0].i, 1)

        self.assertEqual(loggers['evaluate'].content[0].quantization, 'int8')

        self.assertEqual(loggers['learningmodel'].content[0].params, 1150)
        self.assertEqual(loggers['learningmodel'].content[1].params, 1150)
        self.assertEqual(loggers['evaluate'].content[0].params, 1150)

        self.assertEqual(loggers['learningmodel'].content[0].mem_params, 4600) # Model is float32 after training
        self.assertEqual(loggers['learningmodel'].content[1].mem_params, 1150) # Model is (virtually) int8 after QAT
        self.assertEqual(loggers['evaluate'].content[0].mem_params, 1150) # Quantized during deployment and int8 for evaluate

        self.assertGreaterEqual(loggers['learningmodel'].content[0].accuracy, 0.85)
        self.assertGreaterEqual(loggers['learningmodel'].content[1].accuracy, 0.85)
        self.assertGreaterEqual(loggers['evaluate'].content[0].accuracy, 0.85)

        self.assertLessEqual(loggers['evaluate'].content[0].avg_time, 0.02)

        self.assertLessEqual(loggers['evaluate'].content[0].rom_size, 39000)
