from unittest import TestCase

class TestTrainingFrameworks(TestCase):
    def setUp(self):
        import numpy as np
        from microai.datamodel import RawDataModel
        train = RawDataModel.Data(np.array([[[1.0]], [[1.0]]], dtype=np.float32), np.array([[1, 0], [1, 0]]))
        test = RawDataModel.Data(np.array([[[1.0]], [[1.0]]], dtype=np.float32), np.array([[1, 0], [0, 1]]))
        self.__data = RawDataModel(RawDataModel.Sets(train, test), name='test_train')

    def test_train_keras(self):
        from microai import microai
        from microai.learningframework import keras
        from tensorflow.keras.layers import Input, Flatten, Dense, Activation
        from tensorflow.keras.models import Sequential

        model = lambda input_shape, *a, **kwa: Sequential((Input(input_shape), Flatten(), Dense(2), Activation('softmax')))

        framework = keras(optimizer={'kind': 'Adam'})

        trainresult = microai.train(self.__data,
                        train_epochs=1,
                        iteration=1,
                        model_name='test_train',
                        model=model,
                        framework=framework,
                        )
        self.assertEqual(trainresult.name, 'test_train')
        self.assertEqual(trainresult.i, 1)
        self.assertEqual(trainresult.model.input_shape, (None, 1, 1))
        self.assertEqual(trainresult.model.output_shape, (None, 2))
        self.assertEqual(trainresult.mem_params, (2 + 2) * 4) # 2 weights, 2 biases, 4 bytes (float32)
        self.assertEqual(trainresult.acc, 0.5) # Same data in one or the other class, should have 50% acc
        self.assertEqual(trainresult.framework, framework)

    def test_train_pytorch(self):
        from microai import microai
        from microai.learningframework import pytorch
        import math
        from torch.nn import Sequential, Flatten, Linear, ReLU

        model = lambda input_shape, *a, **kwa: Sequential(Flatten(), Linear(math.prod(input_shape), 2))

        framework = pytorch(optimizer={'kind': 'Adam'})

        trainresult = microai.train(self.__data,
                        train_epochs=1,
                        iteration=1,
                        model_name='test_train',
                        model=model,
                        framework=framework,
                        )
        self.assertEqual(trainresult.name, 'test_train')
        self.assertEqual(trainresult.i, 1)
        #self.assertEqual(trainresult.model.input_shape, (None, 1, 1)) # Not supported in PyTorch
        #self.assertEqual(trainresult.model.output_shape, (None, 2)), Not supported in PyTorch
        self.assertEqual(trainresult.mem_params, (2 + 2) * 4) # 2 weights, 2 biases, 4 bytes (float32)
        self.assertEqual(trainresult.acc, 0.5) # Same data in one or the other class, should have 50% acc
        self.assertEqual(trainresult.framework, framework)
