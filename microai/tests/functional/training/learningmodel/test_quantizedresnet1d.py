from unittest import TestCase

class TestModelsQuantizedResnet1D(TestCase):
    def setUp(self):
        import numpy as np
        from microai.datamodel import RawDataModel
        train = RawDataModel.Data(np.ones((2, 64, 1), dtype=np.float32), np.array([[1, 0], [1, 0]]))
        test = RawDataModel.Data(np.ones((2, 64, 1), dtype=np.float32), np.array([[1, 0], [0, 1]]))
        self.__data = RawDataModel(RawDataModel.Sets(train, test), name='test_train')
        self.__model_params = {
            'filters': (4, 6),
            'kernel_size': 5,

            'num_blocks': (2,),
            'strides': (1, 2),
            'padding': 2,

            'prepool': 1,
            'batch_norm': False,
            'bn_momentum': 0.95,

            'dims': 1,

            'bits': 8,
            'quantize_bias': True,
            'quantize_linear': True,
            'quantize_add': True,
            }

    def test_quantized_resnet_1d_pytorch(self):
        from microai import microai
        from microai.learningframework import pytorch
        from microai.learningmodel.pytorch import QuantizedResNet
        from microai.learningmodel.pytorch.quantized_layers1d import QuantizedConv1d
        from microai.learningmodel.pytorch.quantized_layers import QuantizedLinear, QuantizedAdd
        from torch.nn import Flatten, Linear, ReLU, AvgPool1d, MaxPool1d

        model = QuantizedResNet

        framework = pytorch(optimizer={'kind': 'Adam'})

        trainresult = microai.train(self.__data,
                        train_epochs=1,
                        iteration=1,
                        model_name='test_train',
                        model=model,
                        model_params=self.__model_params,
                        framework=framework,
                        )
        self.assertEqual(trainresult.name, 'test_train')
        self.assertEqual(trainresult.i, 1)
        #self.assertEqual(trainresult.model.input_shape, (None, 1, 1)) # Not supported in PyTorch
        #self.assertEqual(trainresult.model.output_shape, (None, 2)), Not supported in PyTorch

        self.assertEqual(len(list(trainresult.model.children())), 6)
        self.assertEqual(len(list(trainresult.model.layers.children())), 1)
        self.assertEqual(len(list(trainresult.model.layers[0].children())), 2)
        self.assertEqual(len(list(trainresult.model.layers[0][0].children())), 8)
        self.assertEqual(len(list(trainresult.model.layers[0][1].children())), 7)

        # prepool
        self.assertFalse(hasattr(trainresult.model, 'prepool'))

        # preconv
        self.assertEqual(type(trainresult.model.conv1), QuantizedConv1d)
        self.assertEqual(tuple(trainresult.model.conv1.padding), (2,))
        self.assertEqual(tuple(trainresult.model.conv1.weight.shape), (4, 1, 5)) # 4*1*5 = 20
        self.assertEqual(tuple(trainresult.model.conv1.bias.shape), (4, )) # 4
        self.assertFalse(hasattr(trainresult.model, 'bn1'))
        self.assertEqual(type(trainresult.model.relu1), ReLU)

        # 1st res block
        # 1st conv
        self.assertEqual(type(trainresult.model.layers[0][0].conv1), QuantizedConv1d)
        self.assertEqual(tuple(trainresult.model.layers[0][0].conv1.padding), (2,))
        self.assertEqual(tuple(trainresult.model.layers[0][0].conv1.weight.shape), (6, 4, 5)) # 6*4*5 = 120
        self.assertEqual(tuple(trainresult.model.layers[0][0].conv1.bias.shape), (6, )) # 6
        self.assertFalse(hasattr(trainresult.model.layers[0][0], 'bn1'))
        self.assertEqual(type(trainresult.model.layers[0][0].pool1), MaxPool1d)
        self.assertEqual(trainresult.model.layers[0][0].pool1.kernel_size, 2)
        self.assertEqual(trainresult.model.layers[0][0].pool1.stride, 2)
        self.assertEqual(type(trainresult.model.layers[0][0].relu1), ReLU)
        # 2nd conv
        self.assertEqual(type(trainresult.model.layers[0][0].conv2), QuantizedConv1d)
        self.assertEqual(tuple(trainresult.model.layers[0][0].conv2.padding), (2,))
        self.assertEqual(tuple(trainresult.model.layers[0][0].conv2.weight.shape), (6, 6, 5)) # 6*6*5 = 180
        self.assertEqual(tuple(trainresult.model.layers[0][0].conv2.bias.shape), (6, )) # 6
        self.assertFalse(hasattr(trainresult.model.layers[0][0], 'bn2'))
        # shortcut
        self.assertEqual(type(trainresult.model.layers[0][0].sconv), QuantizedConv1d)
        self.assertEqual(tuple(trainresult.model.layers[0][0].sconv.padding), (0,))
        self.assertEqual(tuple(trainresult.model.layers[0][0].sconv.weight.shape), (6, 4, 1)) # 6*4*1 = 24
        self.assertEqual(tuple(trainresult.model.layers[0][0].sconv.bias.shape), (6, )) # 6
        self.assertFalse(hasattr(trainresult.model.layers[0][0], 'sbn'))
        self.assertEqual(type(trainresult.model.layers[0][0].smax), MaxPool1d)
        self.assertEqual(trainresult.model.layers[0][0].smax.kernel_size, 2)
        self.assertEqual(trainresult.model.layers[0][0].smax.stride, 2)
        # add
        self.assertEqual(type(trainresult.model.layers[0][0].add), QuantizedAdd)
        self.assertEqual(type(trainresult.model.layers[0][0].relu), ReLU)


        # 2nd conv block
        # 1st conv
        self.assertEqual(type(trainresult.model.layers[0][1].conv1), QuantizedConv1d)
        self.assertEqual(tuple(trainresult.model.layers[0][1].conv1.padding), (2,))
        self.assertEqual(tuple(trainresult.model.layers[0][1].conv1.weight.shape), (6, 6, 5)) # 6*6*5 = 180
        self.assertEqual(tuple(trainresult.model.layers[0][1].conv1.bias.shape), (6, )) # 6
        self.assertFalse(hasattr(trainresult.model.layers[0][1], 'bn1'))
        self.assertEqual(type(trainresult.model.layers[0][1].pool1), MaxPool1d)
        self.assertEqual(trainresult.model.layers[0][1].pool1.kernel_size, 1)
        self.assertEqual(trainresult.model.layers[0][1].pool1.stride, 1)
        self.assertEqual(type(trainresult.model.layers[0][1].relu1), ReLU)
        # 2nd conv
        self.assertEqual(type(trainresult.model.layers[0][1].conv2), QuantizedConv1d)
        self.assertEqual(tuple(trainresult.model.layers[0][1].conv2.padding), (2,))
        self.assertEqual(tuple(trainresult.model.layers[0][1].conv2.weight.shape), (6, 6, 5)) # 6*6*5 = 180
        self.assertEqual(tuple(trainresult.model.layers[0][1].conv2.bias.shape), (6, )) # 6
        self.assertFalse(hasattr(trainresult.model.layers[0][1], 'bn2'))
        # shortcut
        self.assertEqual(type(trainresult.model.layers[0][1].smax), MaxPool1d)
        self.assertEqual(trainresult.model.layers[0][1].smax.kernel_size, 1)
        self.assertEqual(trainresult.model.layers[0][1].smax.stride, 1)
        # add
        self.assertEqual(type(trainresult.model.layers[0][1].add), QuantizedAdd)
        self.assertEqual(type(trainresult.model.layers[0][1].relu), ReLU)

        self.assertEqual(type(trainresult.model.max_pool), MaxPool1d)
        self.assertEqual(tuple(trainresult.model.max_pool.kernel_size), (32,))
        self.assertEqual(tuple(trainresult.model.max_pool.stride), (32,))

        self.assertEqual(type(trainresult.model.flatten), Flatten)

        # fc
        self.assertEqual(type(trainresult.model.linear), QuantizedLinear)
        self.assertEqual(trainresult.model.linear.weight.shape, (2, 6)) # 2*6 = 12
        self.assertEqual(trainresult.model.linear.bias.shape, (2, )) # 2


        self.assertEqual(trainresult.mem_params, (5*1*4 + 4 + 5*4*6 + 6 + 5*6*6 + 6 + 1*4*6 + 6 + 5*6*6 + 6 + 5*6*6 + 6 + 6*2 + 2) * 4)
        self.assertEqual(trainresult.acc, 0.5) # Same data in one or the other class, should have 50% acc
        self.assertEqual(trainresult.framework, framework)
