
class ResBlock: # Not an actual Keras model because it is harder to serialize and get it to work on third-party tools
    expansion = 1

    def __init__(self,
                layers_t,
                filters,
                kernel_size,
                stride,
                padding,
                batch_norm: bool=False,
                bn_momentum: float=0.99):
        super().__init__()
        from tensorflow.keras.layers import BatchNormalization, Activation
        from tensorflow.python.keras.backend import unique_object_name

        self.filters = filters
        self.stride = stride
        self.batch_norm = batch_norm
        
        self.padding_1 = layers_t.ZeroPadding(padding=padding)
        self.conv_1 = layers_t.Conv(filters=filters, kernel_size=kernel_size, strides=1, use_bias=not batch_norm)
        self.maxpool_1 = layers_t.MaxPooling(stride)
        if batch_norm:
            self.batchnorm_1 = BatchNormalization(momentum=bn_momentum)
        self.relu_1 = Activation('relu')

        self.padding_2 = layers_t.ZeroPadding(padding=padding)
        self.conv_2 = layers_t.Conv(name=unique_object_name('conv_ref', zero_based=True), filters=filters, kernel_size=kernel_size, strides=1, use_bias=not batch_norm)
        if batch_norm:
            self.batchnorm_2 = BatchNormalization(momentum=bn_momentum)

        self.maxpool_shortcut = layers_t.MaxPooling(stride)
        if self.stride != 1 or True:
            self.conv_shortcut = layers_t.Conv(name=unique_object_name('conv_shortcut', zero_based=True), filters=self.expansion * filters, kernel_size=1, strides=1, use_bias=not batch_norm)
            if batch_norm:
                self.batchnorm_shortcut = BatchNormalization(momentum=bn_momentum)

        self.relu_2 = Activation('relu')


    def __call__(self, inputs):
        x = self.padding_1(inputs)
        x = self.conv_1(x)
        if self.batch_norm:
            x = self.batchnorm_1(x)
        x = self.maxpool_1(x)
        x = self.relu_1(x)
        
        x = self.padding_2(x)
        x = self.conv_2(x)
        if self.batch_norm:
            x = self.batchnorm_2(x)

        # shortcut
        if self.stride != 1 or inputs.shape[-1] != self.expansion * self.filters:
            y = self.conv_shortcut(inputs)
            if self.batch_norm:
                y = self.batchnorm_shortcut(y)
            y = self.maxpool_shortcut(y)
        else:
            y = self.maxpool_shortcut(inputs)

        x = x + y
        x = self.relu_2(x)

        return x

    @property
    def output_shape(self):
        return self.relu_2.output_shape

def ResNet(input_shape,
        output_shape,
        filters: int=(15, 15, 30, 60, 120), # + (120,)
        kernel_size: int=3,

        num_blocks: tuple=(2, 2, 2, 2), # + (2,)
        strides: tuple=(1, 1, 2, 2, 2), # + (2,)
        padding: int=1,

        prepool: int=1,
        batch_norm: bool=False,
        bn_momentum: float=0.99,

        dims: int=1):

    import tensorflow as tf
    from tensorflow.keras import Model
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.layers import Input, Flatten, Dense, BatchNormalization, Activation
    import tensorflow.keras.backend as backend

    if dims == 1:
        import microai.learningmodel.keras.layers1d as layers_t
    elif dims == 2:
        import microai.learningmodel.keras.layers2d as layers_t
    else:
        raise ValueError('Only dims=1 or dims=2 supported')

    # Do not track moving statistics (avg/var) for inference, use batch statistics only like PyTorch's track_running_stats
    track_running_stats ={} # {'training': True}

    # Actual model definition
    # Conv → BatchNorm → ResBlock → ResBlock → ResBlock → AveragePooling → Flatten → FullyConnected(out)

    inputs = Input(shape=input_shape)

    if prepool > 1:
        x = layers_t.AveragePooling(pool_size=prepool)(inputs)
    else:
        x = inputs

    # First non-residual convolution
    x = layers_t.ZeroPadding(padding=padding)(x)
    x = layers_t.Conv(filters=filters[0], kernel_size=kernel_size, strides=strides[0], use_bias=not batch_norm)(x)
    if batch_norm:
        x = BatchNormalization(momentum=bn_momentum)(x)
    x = Activation('relu')(x)

    # Residual layers
    for f, layer_stride, num_blocks in zip(filters[1:], strides[1:], num_blocks):
        for block_stride in [layer_stride] + [1] * (num_blocks - 1):
            x = ResBlock(layers_t=layers_t,
                        filters=f,
                        kernel_size=kernel_size,
                        stride=block_stride,
                        padding=padding,
                        batch_norm=batch_norm,
                        bn_momentum=bn_momentum)(x) # 1st block of layer

    # Reduce last conv layer output feature maps
    x = layers_t.MaxPooling(x.shape[1])(x)
    x = Flatten()(x)
    x = Dense(output_shape[0], use_bias=True)(x)
    outputs = Activation('softmax')(x)

    model = Model(inputs=inputs, outputs=outputs, name='ResNet')

    return model
