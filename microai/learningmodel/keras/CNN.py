def CNN(input_shape,
        output_shape,
        filters: tuple=(64, 64),
        kernel_sizes: tuple=(3, 3),
        batch_norm: bool=False,
        dropout: float=0,
        pool_sizes: tuple=(4, 4),
        fc_units: int=(120, ),
        prepool: int=0,

        dims: int=1):
    import tensorflow as tf
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.layers import Flatten, Dense, BatchNormalization, Dropout, Activation, Input
    from tensorflow.keras.optimizers import Adadelta, SGD, Adam
    from tensorflow.keras.callbacks import LambdaCallback
    import tensorflow.keras.backend as backend

    if dims == 1:
        import microai.learningmodel.keras.layers1d as layers_t
    elif dims == 2:
        import microai.learningmodel.keras.layers2d as layers_t
    else:
        raise ValueError('Only dims=1 or dims=2 supported')


    optimizer = Adam()

    model = Sequential()

    # Conv → MaxPool → Conv → MaxPool → Conv → Flatten → FullyConnected → FullyConnected(out)
    # No Dropout/BatchNormalization: doesn't improve test acc on UCI-HAR (even worse)

    model.add(Input(shape=input_shape))

    if prepool:
        model.add(layers_t.AveragePooling(pool_size=prepool))

    for f, ks, ps  in zip(filters, kernel_sizes, pool_sizes):
        model.add(layers_t.Conv(filters=f, kernel_size=ks))
        model.add(Activation('relu'))
        if batch_norm:
            model.add(BatchNormalization())
        if dropout:
            model.add(Dropout(dropout))
        if ps: # Optional MaxPool, must specify None in pool_sizes param when no MaxPool layer should be generated after Conv/Act
            model.add(layers_t.MaxPooling(pool_size=ps))

    model.add(Flatten())

    for fc_unit in fc_units:
        model.add(Dense(fc_unit))
        model.add(Activation('relu'))

    # Output classifier
    model.add(Dense(output_shape[0]))
    model.add(Activation('softmax'))

    return model
