from .MLP import MLP
from .CNN import CNN
from .ResNet import ResNet
