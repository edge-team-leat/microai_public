import torch
import torch.nn 
import sys

from .quantizer import IntNoGradient, maxWeight, quantifier

class QuantizedConv1d(torch.nn.Conv1d):	

    def __init__(self, in_channels, out_channels, kernel_size=3,stride=1, padding = 0,dilation=1, groups=1, bias=True, bits=-1, quantize_bias=True, force_q=None, activation=None):
        super().__init__(in_channels, out_channels, stride = stride, bias = bias, padding = padding, kernel_size = kernel_size,groups=groups)
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_width = kernel_size
        self.groups = groups
        self.max_activation = -float('inf')
        self.bits=bits
        self.max_activation2 = -float('inf')
        self.quantize_bias = quantize_bias
        self.force_q = force_q
        self.activation = activation
        #self.n_bit_w = torch.nn.parameter.Parameter(torch.FloatTensor([8]))
        #self.n_bit_w.data.fill_(8)
        #self.n_bit_a = torch.nn.parameter.Parameter(torch.FloatTensor([8]))
        #self.n_bit_a.data.fill_(8)
                #b=torch.max(self.attentionWeights,dim=2)
        #print(b.shape)
        #self.attentionWeights.data[:,:,4]=b[0][:,:]
 
    def forward(self,input):
        #if self.quan_train and self.quand_test:

        max_weight = max(torch.max(torch.abs(self.weight)), torch.max(torch.abs(self.bias)) if self.bias is not None else 0)

        if self.training:
            maxi = torch.max(torch.abs(input)).item()
            if self.max_activation < maxi:
                self.max_activation = maxi
        #else:
        #print(torch.max(torch.abs(input)))
        #print(torch.max(torch.abs(input)).item())
        #    input = torch.clamp(input,-self.max_activation,self.max_activation)

        bias = quantifier(self.bias, self.bits, self.training, max_weight, force_q=self.force_q) if self.quantize_bias and self.bias is not None else self.bias
        y = torch.nn.functional.conv1d(
                quantifier(input, self.bits, self.training, self.max_activation, force_q=self.force_q),
                quantifier(self.weight, self.bits, self.training, max_weight, force_q=self.force_q),
                bias = bias,
                stride = self.stride, padding = self.padding,groups=self.groups)

        if self.activation:
            y = self.activation(y)

        if self.training:
            maxi = torch.max(torch.abs(y)).item()
            if self.max_activation2 < maxi:
                self.max_activation2 = maxi
        #else:
        #    y = torch.clamp(y, -self.max_activation2, self.max_activation2)
        return quantifier(y, self.bits, self.training, self.max_activation2, force_q=self.force_q)
        #else:
        #return torch.nn.functional.conv1d(input,self.weight, bias = self.bias, stride = self.stride, padding = self.padding,groups=self.groups)

    @property
    def input_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation, training=False)

    @property
    def activation_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation2, training=False)

    @property
    def weights_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            maxi = max(torch.max(torch.abs(self.weight)), torch.max(torch.abs(self.bias)) if self.bias is not None else 0)
            return self.bits - 1 + maxWeight(torch.zeros(1), maxi, training=False)


class QuantizedMaxPool1d(torch.nn.MaxPool1d):
    def __init__(self, kernel_size, bits=-1, force_q=None, activation=None):
        super().__init__(kernel_size)
        self.max_activation = -float('inf')
        self.bits = bits
        self.max_activation2 = -float('inf')
        self.force_q = force_q
        self.activation = activation
 
    def forward(self, input):
        if self.training:
            maxi = torch.max(torch.abs(input)).item()
            if self.max_activation < maxi:
                self.max_activation = maxi
        y = super().forward(quantifier(input, self.bits, self.training, self.max_activation, force_q=self.force_q))

        if self.activation:
            y = self.activation(y)

        if self.training:
            maxi = torch.max(torch.abs(y)).item()
            if self.max_activation2 < maxi:
                self.max_activation2 = maxi
        #else:
        #    y = torch.clamp(y, -self.max_activation2, self.max_activation2)
        return quantifier(y, self.bits, self.training, self.max_activation2, force_q=self.force_q)

    @property
    def input_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation, training=False)

    @property
    def activation_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation2, training=False)

    @property
    def weights_q(self):
        return 0

