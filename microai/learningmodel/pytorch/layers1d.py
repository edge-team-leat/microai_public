from torch.nn import Conv1d as Conv
from torch.nn import BatchNorm1d as BatchNorm
from torch.nn import AvgPool1d as AvgPool
from torch.nn import MaxPool1d as MaxPool
from .quantized_layers1d import QuantizedConv1d as QuantizedConv
from .quantized_layers1d import QuantizedMaxPool1d as QuantizedMaxPool
