import torch
import torch.nn 
import sys

from .quantizer import IntNoGradient, maxWeight, quantifier

class QuantizedLinear(torch.nn.Linear):	
    def __init__(self, in_features, out_features, bias=True, bits=-1, quantize_bias=True, force_q=None, activation=None):
        super().__init__(in_features, out_features, bias = bias)
        self.in_features = in_features
        self.out_features = out_features
        self.max_activation = -float('inf')
        self.bits=bits
        self.max_activation2 = -float('inf')
        self.quantize_bias = quantize_bias
        self.force_q = force_q
        self.activation = activation

    def forward(self,input):
        #if self.quan_train and self.quand_test:

        max_weight = max(torch.max(torch.abs(self.weight)), torch.max(torch.abs(self.bias)) if self.bias is not None else 0)

        if self.training:
            maxi = torch.max(torch.abs(input)).item()
            if self.max_activation < maxi:
                self.max_activation = maxi
        #else:
        #print(torch.max(torch.abs(input)))
        #print(torch.max(torch.abs(input)).item())
        #    input = torch.clamp(input, -self.max_activation, self.max_activation)
        bias = quantifier(self.bias, self.bits, self.training, max_weight, force_q=self.force_q) if self.quantize_bias and self.bias is not None else self.bias
        y = torch.nn.functional.linear(
                quantifier(input, self.bits, self.training, self.max_activation, force_q=self.force_q),
                quantifier(self.weight, self.bits, self.training, max_weight, force_q=self.force_q),
                bias = bias)
        if self.activation:
            y = self.activation(y)

        if self.training:
            maxi = torch.max(torch.abs(y)).item()
            if self.max_activation2 < maxi:
                self.max_activation2 = maxi
        #else:
        #    y = torch.clamp(y, -self.max_activation2, self.max_activation2)
        return quantifier(y, self.bits, self.training, self.max_activation2, force_q=self.force_q)
        return out

    @property
    def input_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation, training=False)

    @property
    def activation_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation2, training=False)

    @property
    def weights_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            maxi = max(torch.max(torch.abs(self.weight)), torch.max(torch.abs(self.bias)) if self.bias is not None else 0)
            return self.bits - 1 + maxWeight(torch.zeros(1), maxi, training=False)

from microai.learningmodel.pytorch.layers import Add
class QuantizedAdd(Add):
    def __init__(self, bits=-1, force_q=None, activation=None):
        super().__init__()
        self.max_activation_a = -float('inf')
        self.max_activation_b = -float('inf')
        self.bits=bits
        self.max_activation2 = -float('inf')
        self.force_q = force_q
        self.activation = activation
 
    def forward(self, a, b):
        if self.training:
            # Each input can have its own scale factor
            maxi_a = torch.max(torch.abs(a)).item()
            maxi_b = torch.max(torch.abs(b)).item()
            if self.max_activation_a < maxi_a:
                self.max_activation_a = maxi_a
            if self.max_activation_b < maxi_b:
                self.max_activation_b = maxi_b
        #else:
        #    a = torch.clamp(a, -self.max_activation, self.max_activation)
        #    b = torch.clamp(b, -self.max_activation, self.max_activation)
        y = super().forward(quantifier(a, self.bits, self.training, self.max_activation_a, force_q=self.force_q),
                            quantifier(b, self.bits, self.training, self.max_activation_b, force_q=self.force_q))

        if self.activation:
            y = self.activation(y)

        qinputa = quantifier(a, self.bits, self.training, self.max_activation_a, force_q=self.force_q),
        qinputb = quantifier(b, self.bits, self.training, self.max_activation_b, force_q=self.force_q),

        if self.training:
            maxi = torch.max(torch.abs(y)).item()
            if self.max_activation2 < maxi:
                self.max_activation2 = maxi
        #else:
        #    y = torch.clamp(y, -self.max_activation2, self.max_activation2)
        return quantifier(y, self.bits, self.training, self.max_activation2, force_q=self.force_q)

    @property
    def max_activation(self):
        # No single max activation to return
        return 0

    @property
    def input_q(self):
        #if self.force_q is not None:
        #    return self.force_q
        #else:
        #    return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation, training=False)
        # No input scale factor to return because there can be different ones for each input
        return 0

    @property
    def activation_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation2, training=False)

    @property
    def weights_q(self):
        return 0

class QuantizedReLU(torch.nn.ReLU):
    def __init__(self, bits=-1, force_q=None):
        super().__init__()
        self.max_activation = -float('inf')
        self.bits = bits
        self.max_activation2 = -float('inf')
        self.force_q = force_q
 
    def forward(self, input):
        if self.training:
            maxi = torch.max(torch.abs(input)).item()
            if self.max_activation < maxi:
                self.max_activation = maxi
        y = super().forward(quantifier(input, self.bits, self.training, self.max_activation, force_q=self.force_q))
        if self.training:
            maxi = torch.max(torch.abs(y)).item()
            if self.max_activation2 < maxi:
                self.max_activation2 = maxi
        #else:
        #    y = torch.clamp(y, -self.max_activation2, self.max_activation2)
        return quantifier(y, self.bits, self.training, self.max_activation2, force_q=self.force_q)

    @property
    def input_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation, training=False)

    @property
    def activation_q(self):
        if self.force_q is not None:
            return self.force_q
        else:
            return self.bits - 1 + maxWeight(torch.zeros(1), self.max_activation2, training=False)

    @property
    def weights_q(self):
        return 0


from .quantized_layers1d import QuantizedConv1d, QuantizedMaxPool1d
from .quantized_layers2d import QuantizedConv2d, QuantizedMaxPool2d

quantized_layers = (QuantizedConv1d, QuantizedConv2d, QuantizedMaxPool1d, QuantizedMaxPool2d, QuantizedLinear, QuantizedAdd, QuantizedReLU)
