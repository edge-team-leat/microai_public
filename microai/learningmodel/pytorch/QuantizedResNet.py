import math

import torch.nn as nn
import torch.nn.functional as F
from microai.learningmodel.pytorch.ResNet import ResNet, BasicBlock
from microai.learningmodel.pytorch.layers import Add
from microai.learningmodel.pytorch.quantized_layers import QuantizedLinear, QuantizedAdd, QuantizedReLU

class QuantizedBasicBlock(BasicBlock):
    def __init__(self,
                    layers_t,
                    in_planes,
                    planes,
                    kernel_size,
                    stride,
                    padding,
                    batch_norm,
                    bn_momentum,
                    quantize_bias,
                    quantize_add,
                    bits,
                    force_q,
                    fused_relu=True):
        super().__init__(layers_t, in_planes, planes, kernel_size, stride, padding, batch_norm, bn_momentum)
        self.fused_relu = fused_relu

        self.conv1 = layers_t.QuantizedConv(in_planes, planes, kernel_size=kernel_size, stride=1, padding=padding, bias=not batch_norm, quantize_bias=quantize_bias, bits=bits, force_q=force_q)
        self.pool1 = layers_t.QuantizedMaxPool(stride, bits=bits, force_q=force_q, activation=nn.ReLU() if fused_relu else None)
        if fused_relu:
            del self.relu1
        else:
            self.relu1 = QuantizedReLU(bits=bits, force_q=force_q)
        self.conv2 = layers_t.QuantizedConv(planes, planes, kernel_size=kernel_size, stride=1, padding=padding, bias=not batch_norm, quantize_bias=quantize_bias, bits=bits, force_q=force_q)
        self.smax = layers_t.QuantizedMaxPool(stride, bits=bits, force_q=force_q)
        if self.stride != 1 or self.in_planes != self.expansion*self.planes:
            self.sconv = layers_t.QuantizedConv(in_planes, self.expansion*planes, kernel_size=1, stride=1, bias=not batch_norm, quantize_bias=quantize_bias, bits=bits, force_q=force_q)
        # Replace Add with the quantized variant
        if quantize_add:
            self.add = QuantizedAdd(bits=bits, force_q=force_q, activation=nn.ReLU() if fused_relu else None)
        if fused_relu:
            del self.relu
        else:
            self.relu = QuantizedReLU(bits=bits, force_q=force_q)

    def forward(self, x):
        out = self.conv1(x)
        if self.batch_norm:
            out = self.bn1(out)
        out = self.pool1(out)
        if not self.fused_relu:
            out = self.relu1(out)

        out = self.conv2(out)
        if self.batch_norm:
            out = self.bn2(out)

        # shortcut
        if self.stride != 1 or self.in_planes != self.expansion*self.planes:
            tmp = self.sconv(x)
            if self.batch_norm:
                tmp = self.sbn(tmp)
            tmp = self.smax(tmp)
        else:
            tmp = self.smax(x)

        out = self.add(out, tmp)
        if not self.fused_relu:
            out = self.relu(out)
        return out

class QuantizedResNet(ResNet):
    def __init__(self,
        input_shape,
        output_shape,
        filters: tuple=(15, 15, 30, 60, 120),
        kernel_size: int=3,

        num_blocks: tuple=(2, 2, 2, 2),
        strides: tuple=(1, 1, 2, 2, 2),
        padding: int=1,

        prepool: int=1,
        batch_norm: bool=False,
        bn_momentum: float=0.99,

        dims: int=1,

        bits: int=0,
        quantize_bias: bool=True,
        quantize_linear: bool=True,
        quantize_add: bool=True,
        force_q: int=None,
        fused_relu: bool=True):

        if batch_norm:
            raise ValueError('BatchNorm unsupported in quantized Resnet')
        if prepool > 1:
            raise ValueError('AvgPool unsupported in quantized Resnet')
        bits = int(bits) # Force conversion from TOML int to plain Python int
        if bits < 1:
            raise ValueError('bits must be set to a strictly positive integer')

        if dims == 1:
            import microai.learningmodel.pytorch.layers1d as layers_t
        elif dims == 2:
            import microai.learningmodel.pytorch.layers2d as layers_t
        else:
            raise ValueError('Only dims=1 or dims=2 supported')

        super().__init__(input_shape=input_shape,
                         output_shape=output_shape,
                         filters=filters,
                         kernel_size=kernel_size,
                         num_blocks=num_blocks,
                         strides=strides,
                         padding=padding,
                         prepool=prepool,
                         batch_norm=batch_norm,
                         bn_momentum=bn_momentum,
                         dims=dims,
                         basicblockbuilder=lambda in_planes, planes, stride: QuantizedBasicBlock(
                                                                                layers_t=layers_t,
                                                                                in_planes=in_planes,
                                                                                planes=planes,
                                                                                kernel_size=kernel_size,
                                                                                stride=stride,
                                                                                padding=padding,
                                                                                batch_norm=batch_norm,
                                                                                bn_momentum=bn_momentum,
                                                                                quantize_bias=quantize_bias,
                                                                                quantize_add=quantize_add,
                                                                                bits=bits,
                                                                                force_q=force_q,
                                                                                fused_relu=fused_relu)
                         )
        self.fused_relu = fused_relu

        self.conv1 = layers_t.QuantizedConv(input_shape[-1], filters[0], kernel_size=kernel_size, stride=strides[0], padding=padding, bias=not batch_norm, quantize_bias=quantize_bias, bits=bits, force_q=force_q, activation=nn.ReLU() if fused_relu else None)
        if fused_relu:
            del self.relu1
        else:
            self.relu1 = QuantizedReLU(bits=bits, force_q=force_q)

        self.max_pool = layers_t.QuantizedMaxPool([d // math.prod((prepool, *strides)) for d in input_shape[:-1]], bits=bits, force_q=force_q)
        
        # Replace Linear with the quantized variant
        if quantize_linear:
            self.linear = QuantizedLinear(self.in_planes*QuantizedBasicBlock.expansion, output_shape[0], quantize_bias=quantize_bias, bits=bits, force_q=force_q)

    def forward(self, x):
        if hasattr(self, 'prepool'):
            x = self.prepool(x)

        out = self.conv1(x)
        if self.batch_norm:
            out = self.bn1(out)
        if not self.fused_relu:
            out = self.relu1(out)

        for i in range(len(self.layers)):
            out = self.layers[i](out)

        out = self.max_pool(out)
        out = self.flatten(out)
        out = self.linear(out)
        return out

