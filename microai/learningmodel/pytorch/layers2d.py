from torch.nn import Conv2d as Conv
from torch.nn import BatchNorm2d as BatchNorm
from torch.nn import AvgPool2d as AvgPool
from torch.nn import MaxPool2d as MaxPool
from .quantized_layers2d import QuantizedConv2d as QuantizedConv
from .quantized_layers2d import QuantizedMaxPool2d as QuantizedMaxPool
