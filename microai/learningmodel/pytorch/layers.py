import torch.nn as nn

class Add(nn.Module):
    def __init__(self):
        super().__init__()
        self.functional = nn.quantized.FloatFunctional()

    def forward(self, a, b):
        return self.functional.add(a, b)
