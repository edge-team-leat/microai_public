import torch

class IntNoGradient(torch.autograd.Function):
    
    @staticmethod
    def forward(ctx, x):
#        return x.int().float()
#        return x.floor()
        return x.floor().int().float()
    
    @staticmethod
    def backward(ctx, g):
        return g

"""
def maxWeight(weight,maxi, training):
    #liste_max = []
    #index=0
    #maxi = 0
    w = weight
    v = w.view(-1)
    if(maxi==0 or abs(maxi) == float('inf') or training):
        maxi=torch.max(torch.abs(v))
    if abs(maxi) == float('inf'):
        return 0
    #maxi = torch.max(torch.abs(v)) #.cpu().data.numpy()

    return -torch.log2(maxi)

    n=0
    if(maxi<1 and maxi!=0):
        while(maxi<1):
            maxi*=2
            n+=1
        return n-1
    elif(maxi>=2):
        while(maxi>=2):
           maxi/=2
           n-=1
        return n-1
    else:
        return n-1
"""
def maxWeight(weight, maxi, training):
    maxi = torch.tensor(maxi)

#    if(maxi==0 or abs(maxi) == float('inf') or training):
#        maxi = torch.max(torch.abs(weight.view(-1)))

    if maxi == 0:
        maxi = -1
    elif abs(maxi) != float('inf'):
        maxi = -(torch.log2(maxi).floor().int() + 1)
    else:
        maxi = 0
    return maxi


def quantifier(w, n_bit,training,maxi=0, force_q=None):
    n_bit = torch.tensor(n_bit)

    if force_q is not None:
        v = torch.pow(2, torch.tensor(force_q))
    else:
        maxi=maxWeight(w,maxi,training)
        v = torch.pow(2, n_bit - 1 + maxi)
    
    #w = weight.clone().cuda()
    #a = w.shape
    #v = torch.zeros(a, device='cuda')
    #v = v + pow(2, n_bit-1 + maxi)
    #v = torch.full_like(w, pow(2, n_bit-1 + maxi))
    #v = v.float() #FloatNoGradient.apply(v)
    #v = v.cuda()	
    w = w*v
    w = IntNoGradient.apply(w)
    w = torch.clamp(w, -torch.pow(2, n_bit - 1), torch.pow(2, n_bit - 1) - 1)
    #w = w.int().float()
    #w = FloatNoGradient.apply(w)
    w = w / v
    return w

