import numpy as np
import math
import torch.nn as nn
from microai.learningmodel.pytorch.quantized_layers import QuantizedLinear

class QuantizedCNN(nn.Sequential):
    def __init__(self,
        input_shape,
        output_shape,

        filters: list=[6, 16, 120],
        kernel_sizes: list=[3, 3, 5],
        batch_norm: bool=False,
        dropout: float=0,
        pool_sizes: list=[2, 2],
        fc_units: list=[84],
        prepool: int=1,

        dims: int=1,

        bits: int=0,
        quantize_bias: bool=True,
        quantize_linear: bool=True,
        force_q: int=None):

        self.input_shape = input_shape
        self.output_shape = output_shape

        if batch_norm:
            raise ValueError('BatchNorm unsupported in quantized CNN')
        if prepool > 1:
            raise ValueError('AvgPool unsupported in quantized CNN')
        bits = int(bits) # Force conversion from TOML int to plain Python int
        if bits < 1:
            raise ValueError('bits must be set to a strictly positive integer')

        if dims == 1:
            import microai.learningmodel.pytorch.layers1d as layers_t
        elif dims == 2:
            import microai.learningmodel.pytorch.layers2d as layers_t
        else:
            raise ValueError('Only dims=1 or dims=2 supported')

        layers = []

        if prepool > 1:
            layers.append(layers_t.AvgPool(prepool))

        layers.append(layers_t.QuantizedConv(in_channels=input_shape[-1], out_channels=filters[0], kernel_size=kernel_sizes[0], quantize_bias=quantize_bias, bits=bits, force_q=force_q))
        layers.append(nn.ReLU())

        if batch_norm:
            layers.append(layers_t.BatchNorm(filters[0]))
        if dropout:
            layers.append(nn.Dropout(dropout))
        if pool_sizes[0]:
            layers.append(layers_t.MaxPool(pool_sizes[0]))

        for in_filters, out_filters, kernel, pool_size in zip(filters, filters[1:], kernel_sizes[1:], pool_sizes[1:]):
            layers.append(layers_t.QuantizedConv(in_channels=in_filters, out_channels=out_filters, kernel_size=kernel, quantize_bias=quantize_bias, bits=bits, force_q=force_q))
            layers.append(nn.ReLU())
            if batch_norm:
                layers.append(layers_t.BatchNorm(out_filters))
            if dropout:
                layers.append(nn.Dropout(dropout))
            if pool_size:
                layers.append(layers_t.MaxPool(pool_size))

        layers.append(nn.Flatten())

        in_features = np.array(input_shape[:-1]) // prepool
        for kernel, pool in zip(kernel_sizes, pool_sizes):
            in_features -= 2 * (kernel // 2)
            if pool:
                in_features = in_features // pool
        in_features = in_features.prod()
        in_features *= filters[-1]

        for in_units, out_units in zip((in_features, *fc_units), fc_units):
            if quantize_linear:
                layers.append(QuantizedLinear(in_units, out_units, quantize_bias=quantize_bias, bits=bits, force_q=force_q))
            else:
                layers.append(nn.Linear(in_units, out_units))
            layers.append(nn.ReLU())

        if quantize_linear:
            layers.append(QuantizedLinear(fc_units[-1] if len(fc_units) > 0 else in_features, output_shape[0], quantize_bias=quantize_bias, bits=bits, force_q=force_q))
        else:
            layers.append(nn.Linear(fc_units[-1] if len(fc_units) > 0 else in_features, output_shape[0]))

        super().__init__(*layers)
