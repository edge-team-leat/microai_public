import math


import torch.nn as nn
import torch.nn.functional as F
from microai.learningmodel.pytorch.layers import Add

class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, layers_t, in_planes, planes, kernel_size, stride, padding, batch_norm, bn_momentum):
        super().__init__()
        self.batch_norm = batch_norm
        self.in_planes = in_planes
        self.planes = planes
        self.stride = stride
        self.conv1 = layers_t.Conv(in_planes, planes, kernel_size=kernel_size, stride=1, padding=padding, bias=not batch_norm)
        self.pool1 = layers_t.MaxPool(stride)
        self.relu1 = nn.ReLU()
        self.conv2 = layers_t.Conv(planes, planes, kernel_size=kernel_size, stride=1, padding=padding, bias=not batch_norm)
        self.smax = layers_t.MaxPool(stride)
        if self.stride != 1 or self.in_planes != self.expansion*self.planes:
            self.sconv = layers_t.Conv(in_planes, self.expansion*planes, kernel_size=1, stride=1, bias=not batch_norm)
            if batch_norm:
                self.sbn = layers_t.BatchNorm(self.expansion*planes, momentum=bn_momentum)
        if batch_norm:
            self.bn1 = layers_t.BatchNorm(planes, momentum=bn_momentum)
            self.bn2 = layers_t.BatchNorm(planes, momentum=bn_momentum)
        self.add = Add()
        self.relu = nn.ReLU()

        #self.shortcut = nn.Sequential(nn.MaxPool1d(stride))
        #if stride != 1 or in_planes != self.expansion*planes:
        #    if not bns:
        #        self.shortcut = nn.Sequential(
        #            nn.Conv1d(in_planes, self.expansion*planes, kernel_size=1, stride=1, bias=bias),
        #            nn.MaxPool1d(stride)
        #        )
        #    else:
        #        self.shortcut = nn.Sequential(
        #            nn.Conv1d(in_planes, self.expansion*planes, kernel_size=1, stride=1, bias=bias),
        #            nn.MaxPool1d(stride),
        #            nn.BatchNorm1d(self.expansion*planes, momentum = momentum, track_running_stats = True)
        #        )

    def forward(self, x):
        out = self.conv1(x)
        if self.batch_norm:
            out = self.bn1(out)
        out = self.pool1(out)
        out = self.relu1(out)

        out = self.conv2(out)
        if self.batch_norm:
            out = self.bn2(out)

        # shortcut
        if self.stride != 1 or self.in_planes != self.expansion*self.planes:
            tmp = self.sconv(x)
            if self.batch_norm:
                tmp = self.sbn(tmp)
            tmp = self.smax(tmp)
        else:
            tmp = self.smax(x)

        out = self.add(out, tmp)
        out = self.relu(out)
        return out

class ResNet(nn.Module):
    def __init__(self,
        input_shape,
        output_shape,
        filters: tuple=(15, 15, 30, 60, 120),
        kernel_size: int=3,

        num_blocks: tuple=(2, 2, 2, 2),
        strides: tuple=(1, 1, 2, 2, 2),
        padding: int=1,

        prepool: int=1,
        batch_norm: bool=False,
        bn_momentum: float=0.99,
        
        dims: int=1,
        basicblockbuilder=None):
        super().__init__()

        self.input_shape = input_shape
        self.output_shape = output_shape

        if dims == 1:
            import microai.learningmodel.pytorch.layers1d as layers_t
        elif dims == 2:
            import microai.learningmodel.pytorch.layers2d as layers_t
        else:
            raise ValueError('Only dims=1 or dims=2 supported')

        if basicblockbuilder is None:
            basicblockbuilder=lambda in_planes, planes, stride: BasicBlock(
                                                                    layers_t=layers_t,
                                                                    in_planes=in_planes,
                                                                    planes=planes,
                                                                    kernel_size=kernel_size,
                                                                    stride=stride,
                                                                    padding=padding,
                                                                    batch_norm=batch_norm,
                                                                    bn_momentum=bn_momentum)

        self.in_planes = filters[0]
        self.batch_norm = batch_norm
        self.num_blocks = num_blocks
        if prepool > 1:
            self.prepool = layers_t.AvgPool(prepool)
        self.conv1 = layers_t.Conv(input_shape[-1], filters[0], kernel_size=kernel_size, stride=strides[0], padding=padding, bias=not batch_norm)
        self.relu1 = nn.ReLU()
        
        if self.batch_norm:
            self.bn1 = layers_t.BatchNorm(self.in_planes, momentum=bn_momentum)
        self.layers = []
        for planes, stride, num_block in zip(filters[1:], strides[1:], num_blocks):
            self.layers += [self._make_layer(basicblockbuilder, num_block, planes, stride)]
        self.layers = nn.ModuleList(self.layers)

        self.max_pool = layers_t.MaxPool([d // math.prod((prepool, *strides)) for d in input_shape[:-1]])
        self.flatten = nn.Flatten()
        self.linear = nn.Linear(self.in_planes*BasicBlock.expansion, output_shape[0])

    def _make_layer(self, basicblockbuilder, num_blocks, planes, stride):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            block = basicblockbuilder(in_planes=self.in_planes, planes=planes, stride=stride)
            layers.append(block)
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        if hasattr(self, 'prepool'):
            x = self.prepool(x)

        out = self.conv1(x)
        if self.batch_norm:
            out = self.bn1(out)
        out = self.relu1(out)

        for i in range(len(self.layers)):
            out = self.layers[i](out)

        out = self.max_pool(out)
        out = self.flatten(out)
        out = self.linear(out)
        return out
