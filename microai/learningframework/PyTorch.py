import sys
import numpy as np
import importlib

from .LearningFramework import LearningFramework

class PyTorch(LearningFramework):
    from pytorch_lightning import LightningModule
    from torch.nn.functional import cross_entropy, softmax
    from torch import stack, save
    import torch.optim as optimizers
    #import torch_optimizer as optimizers
    if importlib.util.find_spec("torch_optimizer") is None:
        print('Warning: torch_optimizer not found, not import additional optimizers', file=sys.stderr)
    else:
        optimizers.__dict__.update(importlib.import_module('torch_optimizer').__dict__) # Merge additional torch_optimizer

    import torch.optim.lr_scheduler as schedulers

    trainer = None

    class TrainerModule(LightningModule):
        from torch.nn import Softmax, LogSoftmax, CrossEntropyLoss
        import torch

        def __init__(self, model, max_epochs: int=0, optimizer: dict=None, mixup=False):
            super().__init__()
            self.model = model
            self.max_epochs = max_epochs
            self.optimizer = optimizer
            self.mixup = mixup

            try:
                import torchmetrics as metrics
            except ImportError:
                print('Warning: torchmetrics not available, using pytorch_lightning.metrics', file=sys.stderr)
                from pytorch_lightning import metrics
            import torch.nn as nn
            #self.train_accuracy = metrics.Accuracy()
            #self.val_accuracy = metrics.Accuracy()#compute_on_step=False)
            #self.test_accuracy = metrics.Accuracy()#compute_on_step=False)
            self.train_acc = metrics.Accuracy()
            self.valid_acc = metrics.Accuracy()
            self.test_acc = metrics.Accuracy()
            #self.loss = nn.BCELoss()
            #self.loss = self.CrossEntropyLoss()
            self.crossentropyloss = self.CrossEntropyLoss()
            if mixup:
                self.loss = self.cross_entropy_g
            else:
                self.loss = self.crossentropyloss
            self.softmax = self.Softmax(dim=1)
            self.logsoftmax = self.LogSoftmax(dim=1)

        def __mix_criterion(self, criterion, pred, y_a, y_b, lam):
            lam = lam.mean() # same for whole batch
            return lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)

        def cross_entropy_g(self, pred, targets):
            return self.__mix_criterion(self.crossentropyloss, pred, *targets)

        def cross_entropy_b(self, pred, soft_targets):
            return self.torch.mean(self.torch.sum(- soft_targets * self.logsoftmax(pred), 1))

        
        #def __step(self, mode, batch, batch_nb, accuracy_metric):
        #    x, y = batch
        #    y = y.argmax(axis=-1) # not categorical in PyTorch
        #    logits = self.model(x)
        #    #accuracy_metric(logits, y)
        #    #self.log(f'{mode}_acc', accuracy_metric(logits, y), on_step=True, on_epoch=False, prog_bar=True)
        #    self.log(f'{mode}_acc', accuracy_metric(logits, y), on_step=True, on_epoch=True, prog_bar=True)
        #    return PyTorch.cross_entropy(logits, y)

        #def __epoch_end(self, mode, outputs, accuracy_metric):
        #    #self.log(f'{mode}_acc', accuracy_metric.compute(), prog_bar=True)
        #    pass

        def training_step(self, batch, batch_nb):
            #x, y = batch
            #y = y.argmax(axis=-1) # not categorical in PyTorch
            #pred = self.model(x)
            #acc = y.eq(pred.argmax(axis=-1)).sum().float() / y.numel()
            #return {'loss': PyTorch.cross_entropy(pred, y), 'acc': acc}

            #return self.__step('train', batch, batch_nb, self.train_accuracy)

            x, y = batch
            #y_b = y.argmax(axis=-1) # not categorical in PyTorch
            logits = self.model(x)
            #print(f'{y=}, {logits=}')
            #loss = PyTorch.cross_entropy(logits, y_b)
            #loss = PyTorch.softmax(logits)
            #print(loss)
            #loss = self.loss(loss, y)
            #self.log(f'train_acc', self.train_acc(logits, y))#, on_step=True, on_epoch=True, prog_bar=True)
            if not self.mixup:
                self.train_acc(self.softmax(logits), y) # lightning 1.2 requires preds in [0, 1]

            loss = self.loss(logits, y)
            #self.log(f'train_acc_step', self.train_acc(logits, y_b), prog_bar=False)#, on_step=True, on_epoch=True, prog_bar=True)
            return loss

        def validation_step(self, batch, batch_nb):
            x, y = batch
            #y = y.argmax(axis=-1) # not categorical in PyTorch
            logits = self.softmax(self.model(x)) # lightning 1.2 requires preds in [0, 1]
            self.log(f'valid_acc', self.valid_acc(logits, y), prog_bar=True)#, on_step=True, on_epoch=True, prog_bar=True)

        def test_step(self, batch, batch_nb):
            x, y = batch
            #y = y.argmax(axis=-1) # not categorical in PyTorch
            logits = self.softmax(self.model(x)) # lightning 1.2 requires preds in [0, 1]
            self.log(f'test_acc', self.test_acc(logits, y), prog_bar=True)#, on_step=True, on_epoch=True, prog_bar=True)

        #def training_epoch_end(self, outputs):
            #avg_loss = PyTorch.stack([x['loss'] for x in outputs]).mean()
            #avg_acc = PyTorch.stack([x['acc'] for x in outputs]).mean()
            #results = {'loss': avg_loss, 'acc': avg_acc, 'progress_bar': {'acc': avg_acc}}
            #return results
        #    self.log(f'train_acc', self.train_acc.compute(), prog_bar=True)#, on_step=True, on_epoch=True, prog_bar=True)

        #    self.__epoch_end('train', outputs, self.train_accuracy)
        def training_epoch_end(self, outputs):
            if not self.mixup:
                self.log(f'train_acc', self.train_acc.compute(), prog_bar=True)
            super().training_epoch_end(outputs)
            for param_group in self.trainer.optimizers[0].param_groups:
                self.log('lr', param_group['lr'], prog_bar=True)

        #def test_step(self, batch, batch_nb):
        #    #return self.training_step(batch, batch_nb)
        #    self.__step('test', batch, batch_nb, self.test_accuracy)

        #def test_epoch_end(self, outputs):
            #return self.training_epoch_end(outputs)
        #    self.__epoch_end('test', outputs, self.test_accuracy)

        #def validation_step(self, batch, batch_nb, dataloader_idx=None):
        #    #return self.training_step(batch, batch_nb)
        #    self.__step('val', batch, batch_nb, self.val_accuracy)

        #def validation_epoch_end(self, outputs):
            #results = [self.training_epoch_end(output) for output in outputs]
            #for i, result in enumerate(results):
            #    result['progress_bar'][f'val_acc{i}'] = result['progress_bar'].pop('acc')
            #return results
        #    self.__epoch_end('val', outputs, self.val_accuracy)

        def on_epoch_end(self):
            super().on_epoch_end()
            print()

        def configure_optimizers(self):
            #from torch.optim import Adam, SGD
            #from torch.optim.lr_scheduler import MultiStepLR
            #optimizer = Adam(self.parameters(), lr=0.001)
            #scheduler = MultiStepLR(optimizer, [self.max_epochs*6/10, self.max_epochs*7/10, self.max_epochs*8/10, self.max_epochs*9/10], 0.5)
            #optimizer = SGD(self.parameters(), lr = 0.1, momentum = 0.9, weight_decay = 5e-4)
            #scheduler = MultiStepLR(optimizer, [self.max_epochs*1/3, self.max_epochs*2/3, self.max_epochs*5/6], 0.1)
            if self.optimizer is None:
                return

            optimizer = getattr(PyTorch.optimizers, self.optimizer['kind'])(self.parameters(), **self.optimizer.get('params', {}))
            if 'scheduler' in self.optimizer:
                scheduler = getattr(PyTorch.schedulers, self.optimizer['scheduler']['kind'])(optimizer, **self.optimizer['scheduler']['params'])
                print('Optimizer:', optimizer, self.optimizer.get('params', {}))
                print('Scheduler:', scheduler, self.optimizer['scheduler']['params'])
                return [optimizer], [scheduler]
            else:
                print('Optimizer:', optimizer)
                return [optimizer]

    #from torch.utils.data import IterableDataset

    #class DatasetFromTF(IterableDataset):
    #    import tensorflow as tf
    #    from tensorflow.python.data.experimental.ops.cardinality import cardinality

    #    def __init__(self, tfdataset):
    #        self.__tfdataset = tfdataset

    #    def __channels_last_to_channels_first(self, x, y):
    #        return self.tf.einsum('a...j->aj...', x), y # data already batched

    #    def __iter__(self):
    #        return self.__tfdataset.map(self.__channels_last_to_channels_first).as_numpy_iterator()

    #    def __len__(self):
    #        return PyTorch.DatasetFromTF.cardinality(self.__tfdataset)


    from torch.utils.data import Dataset

    class DatasetFromArray(Dataset):
        import torch

        def __init__(self, dataset, mixup=False):
            if len(dataset.x.shape) == 4:
                self.x = dataset.x.transpose(0, 3, 1, 2)
            elif len(dataset.x.shape) == 3: 
                self.x = dataset.x.swapaxes(1, 2)
            else:
                raise ValueError(f'Unsupported number of axes in dataset: {len(dataset.x.shape)}, must be 3 or 4')
            self.y = dataset.y.argmax(axis=-1)
            self.mixup = mixup

        def __mixup_data(self, x, y, alpha=1.0, use_cuda = True):
            '''Returns mixed inputs, pairs of targets, and lambda'''
            if alpha > 0:
                lam = np.random.beta(alpha, alpha)
            else:
                lam = 1.0

            batch_size = x.shape[0]
            index = self.torch.randperm(batch_size)

            mixed_x = lam * x + (1 - lam) * x[index, :]
            y_a, y_b = y, y[index]
            return mixed_x, y_a, y_b, lam

        def __len__(self):
            #print('Len called!')
            if self.mixup: # Len called at each epoch, reshuffle mixup
                self.data, self.target_a, self.target_b, self.lam = self.__mixup_data(self.x, self.y)
            return len(self.x)

        def __getitem__(self, index):
            if self.mixup:
                return self.data[index], (self.target_a[index], self.target_b[index], self.lam)
            else:
                return self.x[index], self.y[index]


    def __init__(self, optimizer, mixup: bool=False):
        self.optimizer = optimizer
        self.mixup = mixup

    def train(self, model, trainset, validationset, epochs, batch_size, gpus=None):
        from pytorch_lightning import Trainer
        from torch.utils.data import DataLoader

        self.trainer = Trainer(max_epochs=epochs, gpus=gpus, deterministic=True)
        trainer_module = self.TrainerModule(model, max_epochs=epochs, optimizer=self.optimizer, mixup=self.mixup)
        #self.trainer.fit(trainer_module,
        #                    DataLoader(self.DatasetFromTF(trainset), batch_size=None), [
        #                        DataLoader(self.DatasetFromTF(originalset), batch_size=None),
        #                        DataLoader(self.DatasetFromTF(validationset), batch_size=None)
        #                    ])
        # Bug in PyTorch-Lightning 1.0 with multiple dataloaders
        self.trainer.fit(trainer_module,
                            DataLoader(self.DatasetFromArray(trainset, mixup=self.mixup), batch_size=batch_size, shuffle=True),
                            DataLoader(self.DatasetFromArray(validationset), batch_size=batch_size)
                        )

    def evaluate(self, model, testset, batch_size, gpus=None):
        from pytorch_lightning import Trainer
        from torch.utils.data import DataLoader

        if self.trainer is None:
            trainer = Trainer(gpus=gpus)
        else:
            trainer = self.trainer

        trainer_module = self.TrainerModule(model)
        metrics = trainer.test(trainer_module, DataLoader(self.DatasetFromArray(testset), batch_size=batch_size))
        return metrics[0]['test_acc']

    def load(self, name, model):
        import torch
        state_dict = torch.load(f'out/learningmodel/{name}.pth')
        model.load_state_dict(state_dict)
        return model

    def export(self, model, name):
        PyTorch.save(model.state_dict(), f'out/learningmodel/{name}.pth')

    def summary(self, model):
        '''Print model summary'''
        print(model)
        print(f'Number of parameters: {self.n_params(model)}')

    def n_params(self, model):
        import numpy as np
        return np.sum([params.numel() for params in model.parameters()])

    def save_graph_plot(self, model, model_save):
        import sys
        print('Warning: saving PyTorch graph plot unsupported', file=sys.stderr)
