from datetime import datetime
from pathlib import Path

from .LearningFramework import LearningFramework

class Keras(LearningFramework):
    def __init__(self, optimizer: dict=None):
        import tensorflow as tf
        self.optimizer = optimizer
        # Cleanup, especially reset graph uid for uniquely generated layer names
        tf.keras.backend.clear_session()

    def __configure_optimizers(self):
        import tensorflow as tf

        if self.optimizer is None:
            from .DummyOptimizer import DummyOptimizer
            return DummyOptimizer()

        optimizer = getattr(tf.keras.optimizers, self.optimizer['kind'])(**self.optimizer.get('params', {}))
        if 'scheduler' in self.optimizer:
            raise ValueError('Custom learningrate scheduler not implemented in', self.__class__.__name__)
            scheduler = getattr(PyTorch.schedulers, self.optimizer['scheduler']['kind'])(optimizer, **self.optimizer['scheduler']['params'])
            print('Optimizer:', optimizer, self.optimizer.get('params', {}))
            print('Scheduler:', scheduler, self.optimizer['scheduler']['params'])
            return [optimizer], [scheduler]
        else:
            print('Optimizer:', optimizer)
            return optimizer


    def train(self, model, trainset, validationset, epochs, batch_size, gpus: int=None):
        # gpus handled by TensorFlowInitializer
        import tensorflow as tf

        # You can plot the quantize training graph on tensorboard
        logdir="out/logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)

        callbacks=[]
        #callbacks.append(tensorboard_callback)
        # The patience parameter is the amount of epochs to check for improvement
        #callbacks.append(tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', patience=5))
        #callbacks.append(tfdocs.modeling.EpochDots())

        optimizer = self.__configure_optimizers()
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['categorical_accuracy'])

        model.fit(trainset.x,
                trainset.y,
                epochs=epochs,
                batch_size=batch_size,
                validation_data=validationset.astuple(),
                callbacks=callbacks)

        # Remove softmax
        #compile_params = {'loss': model.loss, 'optimizer': model.optimizer, 'metrics': model.compiled_metrics.metrics}
        #model = tf.keras.Model(model.input, model.layers[-2].output, name=model.name)
        #model.compile(**compile_params)
        #model.summary()

    def load(self, name, model=None, path: Path=Path('out')/'learningmodel'):
        from tensorflow.keras.models import load_model
        model = load_model(path/f'{name}.h5', custom_objects=getattr(model, 'get_custom_objects', lambda: {})())
        return model

    def evaluate(self, model, testset, batch_size):
        import tensorflow as tf

        acc = model.evaluate(testset.x, testset.y, batch_size=batch_size)[1]
        # Predict and show confusion matrix
        train_predictions = model.predict(testset.x)
        print(tf.math.confusion_matrix(testset.y.argmax(axis=1), train_predictions.argmax(axis=1)))
        return acc

    def export(self, model, name, path: Path=Path('out')/'learningmodel'):
        model.save(path/f'{name}.h5')

    def summary(self, model):
        model.summary()

    def n_params(self, model):
        return model.count_params()

    def save_graph_plot(self, model, model_save):
        import tensorflow as tf
        tf.keras.utils.model_to_dot(model, show_shapes=True, expand_nested=True).write(f'{model_save}.dot')
