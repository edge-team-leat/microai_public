from abc import ABC, abstractmethod

class LearningFramework(ABC):
    @abstractmethod
    def train(self, model, trainset, validationset, originalset, epochs, batch_size):
        pass

    @abstractmethod
    def load(self, name, model):
        pass

    @abstractmethod
    def evaluate(self, model, testset, batch_size):
        pass

    @abstractmethod
    def export(self, model, name):
        pass

    @abstractmethod
    def summary(self, model):
        pass

    @abstractmethod
    def n_params(self, model):
        pass

    @abstractmethod
    def save_graph_plot(self, model, model_save):
        pass
