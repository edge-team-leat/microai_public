import numpy as np

from .Preprocessing import Preprocessing

class Class2BinMatrix(Preprocessing):
    def __call__(self, datamodel):
        for _, s in datamodel:
            if len(s.y.shape) == 2:
                s.y = np.array([np.bincount(w).argmax() for w in s.y])
            elif len(s.y.shape) != 1:
                raise ValueError(f'Unsupported dimensions for {self.__class__.__name__}: {len(dataY.shape)}')
            if len(s.y) <= 0: # Handle empty sets
                continue
            s.y = np.eye(np.max(s.y) + 1)[s.y]
        return datamodel
