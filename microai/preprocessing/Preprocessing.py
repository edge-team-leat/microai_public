from abc import ABC

class Preprocessing(ABC):
    def import_data(self, dataset):
        """no-op if the preprocessing doesn't modify the way of importing the dataset"""
        return dataset

