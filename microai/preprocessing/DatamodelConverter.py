import numpy as np
from dataclasses import dataclass

from microai.datamodel import RawDataModel

from .Preprocessing import Preprocessing

class DatamodelConverter(Preprocessing):

    def __call__(self, datamodel):
        # First generate the class numbers according to the activities present in the dataset
        activitylabels = set()
        for sname, s in datamodel.sets:
            for subject in s:
                for activity in subject.activities:
                    activitylabels.add(activity.kind)
        activitylabels = sorted(activitylabels)
        print('Activity labels: ', activitylabels)

        sets = {}
        for name, s in datamodel:
            dataX = []
            dataY = []
            for subject in s:
                for activity in subject.activities:
                    dataX += activity.get_raw_array()
                    #dataY += [int(activity.kind)] * len(activity.samples)
                    dataY += [activitylabels.index(activity.kind)] * len(activity.samples)
            sets[name] = RawDataModel.Data(np.array(dataX), np.array(dataY))
        return RawDataModel(sets=RawDataModel.Sets(**sets), name=datamodel.name)

    def import_data(self, dataset):
        return lambda: RawDataModel.import_data(dataset.name)
