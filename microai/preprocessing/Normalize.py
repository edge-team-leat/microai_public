from .Preprocessing import Preprocessing

class Normalize(Preprocessing):
    def z_score(self, data):
        if self.debug:
            print(f'Before normalization: train mean={data.sets.train.x.mean()}, train std={data.sets.train.x.std()}, test mean={data.sets.test.x.mean()}, test std={data.sets.test.x.std()}')

        x_mean = data.sets.train.x.mean(axis=self.__axis, keepdims=True)
        x_std = data.sets.train.x.std(axis=self.__axis, keepdims=True)

        data.sets.train.x -= x_mean
        data.sets.test.x  -= x_mean
        data.sets.train.x /= x_std
        data.sets.test.x  /= x_std


        if self.debug:
            print(f'After normalization: train mean={data.sets.train.x.mean()}, train std={data.sets.train.x.std()}, test mean={data.sets.test.x.mean()}, test std={data.sets.test.x.std()}')

        return data

    methods = {
        'z-score': z_score,
#        'min-max': min_max
    }

    def __init__(self, method: str='z-score', axis: int=0, debug: bool=False):
        self.__refdata = None
        self.debug = debug
        if method not in self.methods:
            raise ValueError(f'{self.__class__.__name__}: Method {method} is not supported. Supported methods: {", ".join(methods)}')
        else:
            self.__method = self.methods[method].__get__(self)
        self.__axis = axis

    def __call__(self, *args, **kwargs):
        return self.__method(*args, **kwargs)
