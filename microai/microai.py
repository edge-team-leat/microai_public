import sys
from pathlib import Path
from collections import namedtuple

def train(eh,
        train_epochs,
        iteration: int,
        model,
        model_name=None,
        model_params={},
        batch_size=None,
        framework=None,
        load=False,
        train=True,
        gpus=None):
    if batch_size is None:
        batch_size = 32


    train_dataset = eh.sets.train
    test_dataset = eh.sets.test
    input_shape = train_dataset.x.shape[1:]
    output_shape = test_dataset.y.shape[1:]
    print(f'{input_shape=} {output_shape=}', test_dataset.y.shape)

    # Instanciate model
    model = model(input_shape=input_shape,
                  output_shape=output_shape,
                  **model_params)

    # Load model if instructed
    if load:
        model = framework.load(f'{model_name}_r{iteration}', model)

    # Show model architecture
    framework.summary(model)

    # And export visualization to dot file
    framework.save_graph_plot(model, f'{model_name}_r{iteration}')

    # You can plot the quantize training graph on tensorboard
    if train:
        framework.train(model,
                        trainset=train_dataset,
                        validationset=test_dataset,
                        epochs=train_epochs,
                        batch_size=batch_size,
                        gpus=gpus)


    print('Evaluation on train dataset')
    acc = framework.evaluate(model, train_dataset, batch_size=batch_size, gpus=gpus)

    if len(test_dataset.x) > 0: # Don't evaluate if testset is empty
        print('Evaluation on test dataset')
        acc = framework.evaluate(model, test_dataset, batch_size=batch_size, gpus=gpus)

    # Do not save loaded model that hasn't been retrained
    if train == True or load == False:
        framework.export(model, f'{model_name}_r{iteration}')

    return namedtuple('TrainResult',
                        ['name', 'i', 'model', 'params', 'mem_params', 'acc', 'trainset', 'testset', 'framework',
                        'log', 'gpus'])(
                            name=model_name, i=iteration, model=model,
                            params=framework.n_params(model), mem_params=framework.n_params(model) * 4,
                            acc=acc, 
                            trainset=train_dataset, testset=test_dataset,
                            framework=framework, log=True, gpus=gpus)

def prepare_deploy(
    model_kind,
    model_name,
    framework,
    iteration,
    deploy_target,
    quantize='float32',
    optimize=None,
    compress=1,
    tag='main',
    converter=None,
    deployers=None,
    representative_dataset=None):

    # use custom_objects property if present to resolve custom objects (layers…) in saved model
    model = framework.load(f'{model_name}_r{iteration}', model_kind)

    if not converter: # no custom converter passed as parameter, check if model suggests custom converter
        converter = getattr(model_kind, 'converter', False)

    if converter:
        ca = converter(quantize=quantize).convert(model, f'{model_name}_r{iteration}', representative_dataset=representative_dataset)
    else: # No conversion taking place since no converter was specified
        ca = model

    if not deployers:
        if not converter:
            print('Error: no converter and no deployers specified', file=sys.stderr)
            return None
        elif not hasattr(ca, 'deployers'):
            print('Error: no deployers specified and converter does not suggest a deployer', file=sys.stderr)
            return None
        else:
            deployers = ca.deployers
    return getattr(deployers, deploy_target)().prepare(tag=tag, model=ca, optimize=optimize, compression=compress)

def deploy(model_kind, deploy_target, tag='main', deployers=None, deployer_params={}):
    if not deployers: # no custom deployers passed as parameter, check if model suggests custom converter
        converter = getattr(model_kind, 'converter', False)
        if converter and converter.deployers: # Converter suggested deployers
            deployers = converter.deployers

    return getattr(deployers, deploy_target)(**deployer_params).deploy(tag=tag)

def evaluate(model_kind, dataset, target, tag, limit=None, compress=None, model_path=None, evaluator=None, evaluator_params={}):
    if not evaluator: # no custom deployers passed as parameter, check if model suggests custom converter
        converter = getattr(model_kind, 'converter', False)
        if converter and converter.evaluator: # Converter suggested deployers
            evaluator = converter.evaluator

    if not evaluator:
        raise ValueError('No evaluator')
    return evaluator(**evaluator_params).evaluate(model_kind=model_kind, dataset=dataset, target=target, tag=tag, limit=limit)
