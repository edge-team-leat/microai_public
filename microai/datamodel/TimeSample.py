class TimeSample:
    def __init__(self, t: float=0.0, sensors: 'List'=None):
        self.t = t
        self.sensors = sensors if sensors is not None else []

    def get_raw_array(self):
        return [v for s in self.sensors for v in s.get_raw_array()] 
