from dataclasses import dataclass

from . import Sensor

@dataclass
class Accelerometer(Sensor):
    x: float
    y: float
    z: float
