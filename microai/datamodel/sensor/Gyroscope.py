from dataclasses import dataclass

from . import Sensor

@dataclass
class Gyroscope(Sensor):
    x: float
    y: float
    z: float
