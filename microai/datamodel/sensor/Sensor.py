from dataclasses import dataclass, fields, asdict, astuple
import numpy as np

##
# Warning: dataclasses.asdict() and dataclasses.astuple() are recursive and may copy
##

@dataclass
class Sensor:
    @classmethod
    def fieldnames(cls):
        return tuple((f.name for f in fields(cls)))

    def asdict(self):
        return asdict(self)

    def get_raw_array(self):
        return np.array(astuple(self))
