from dataclasses import dataclass

from . import Sensor

@dataclass
class Barometer(Sensor):
    p: float
