from dataclasses import dataclass

from . import Sensor

@dataclass
class GPS(Sensor):
    latitude: float
    longitude: float
    altitude: float
    speed: float
    bearing: float
    accuracy: float
