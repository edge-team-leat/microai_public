from dataclasses import dataclass

from . import Sensor

@dataclass
class Magnetometer(Sensor):
    x: float
    y: float
    z: float
