from dataclasses import dataclass, field, asdict, astuple
from typing import Iterable

from . import Subject

from microai.datamodel import DataModel

class HARDataModel(DataModel):
    def __init__(self, sets: DataModel.Sets, name='HAR', window_size: int=0):
        super().__init__(sets, name)
        self.name = name
