class Subject:
    def __init__(self, name: str, activities: 'List[Activity]'=None, part=None):
        self.name = name
        self.activities = activities if activities is not None else []
        self.part = part
