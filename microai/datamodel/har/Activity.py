from .Activities import Activities

class Activity:
    def __init__(self, kind: Activities, samples: 'List[TimeSample]'=None):
        self.kind = kind
        self.samples = samples if samples is not None else []

    def get_raw_array(self):
        return [s.get_raw_array() for s in self.samples]
