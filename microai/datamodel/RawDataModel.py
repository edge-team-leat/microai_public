from dataclasses import dataclass, astuple
import numpy as np
from pathlib import Path

from .DataModel import DataModel

class RawDataModel(DataModel):
    @dataclass
    class Data:
        x: np.array
        y: np.array

        @property
        def data(self):
            return self.x

        @data.setter
        def data(self, data):
            self.x = data

        @property
        def labels(self):
            return self.y

        @labels.setter
        def labels(self, labels):
            self.y = labels

        def export(self, path: Path):
            np.savez_compressed(path/'data.npz', data=self.data)
            np.savez_compressed(path/'labels.npz', labels=self.labels)

        @classmethod
        def import_data(cls, path: Path):
            with np.load(path/'data.npz') as datanpz:
                data = datanpz['data']
            with np.load(path/'labels.npz') as labelsnpz:
                labels = labelsnpz['labels']

            return cls(x=data, y=labels)

        def astuple(self):
            return astuple(self)

    @classmethod
    def import_data(cls, name):
        return cls(sets=cls.Sets(**{sname: cls.Data.import_data(Path('out')/'data'/name/sname) for sname in cls.Sets.fieldnames()}), name=name)
