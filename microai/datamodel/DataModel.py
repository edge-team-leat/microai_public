from dataclasses import dataclass, field, fields, asdict, astuple
from typing import Iterable
from pathlib import Path

class DataModel:
    @dataclass
    class Sets:
        train: Iterable = field(default_factory=list)
        test: Iterable = field(default_factory=list)

        @classmethod
        def fieldnames(cls):
            return tuple((f.name for f in fields(cls)))

        def asdict(self):
            # Explicitely use vars rather than dataclasses.asdict() since we don't want to recurse nor copy
            return vars(self)

        def __iter__(self):
            return self.asdict().items().__iter__()

        def export(self, path: Path):
            for sname, sdata in self:
                (path/sname).mkdir(parents=True, exist_ok=True)
                sdata.export(path/sname)

        @classmethod
        def import_data(cls, path: Path):
            for sname in self:
                setattr(self, sname, sdata.import_data(path/sname))
            return self

    def __init__(self, sets: Sets, name='HAR'):
        self.sets = sets
        self.name = name

    def __iter__(self):
        return self.sets.__iter__()

    def export(self):
        self.sets.export(Path('out')/'data'/self.name)
