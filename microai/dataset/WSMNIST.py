import numpy as np
from pathlib import Path

from microai.datamodel import RawDataModel

class WSMNIST:
    def __init__(self, path: str, variant: str='spoken'):
        self.__path = Path(path)
        self.__variant = variant

    def __load_spoken(self, path):
        trainX = np.load(path/'data_sp_train.npy').reshape(-1, 39, 13).astype(np.float32)
        trainY = np.load(path/'labels_train.npy')
        testX = np.load(path/'data_sp_test.npy').reshape(-1, 39, 13).astype(np.float32)
        testY = np.load(path/'labels_test.npy')

        train = RawDataModel.Data(trainX, trainY)
        test = RawDataModel.Data(testX, testY)

        return RawDataModel(sets=RawDataModel.Sets(train=train, test=test), name='WSMNIST_spoken')

    def __load_written(self, path):
        trainX = np.load(path/'data_wr_train.npy').reshape(-1, 28, 28, 1).astype(np.float32)
        trainY = np.load(path/'labels_train.npy')
        testX = np.load(path/'data_wr_test.npy').reshape(-1, 28, 28, 1).astype(np.float32)
        testY = np.load(path/'labels_test.npy')

        train = RawDataModel.Data(trainX, trainY)
        test = RawDataModel.Data(testX, testY)

        return RawDataModel(sets=RawDataModel.Sets(train=train, test=test), name='WSMNIST_written')

    def __call__(self, path: str=''):
        if self.__variant == 'spoken':
            return self.__load_spoken(self.__path)
        if self.__variant == 'written':
            return self.__load_written(self.__path)
        else:
            raise ValueError('Only spoken or written variants supported')

    def import_data(self):
        return RawDataModel.import_data(name=self.name)

    @property
    def name(self):
        return f'{self.__class__.__name__}_{self.__variant}' 
