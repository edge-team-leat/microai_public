from microai.utils.file import DirectoryReader
from microai.utils.file import CSVReader

from microai.datamodel import FeatureSample, WindowedSample
from microai.datamodel.sensor import Accelerometer, Gyroscope, Barometer
from microai.datamodel.har import Activities
from microai.datamodel.har import Activity
from microai.datamodel.har import Subject
from microai.datamodel.har import HARDataModel

from collections import namedtuple
from pathlib import Path

import numpy as np

class UCI_HAR:
    activitylist = { '1': Activities.WALKING,
                   '2': Activities.WALKING_UPSTAIRS,
                   '3': Activities.WALKING_DOWNSTAIRS,
                   '4': Activities.SITTING,
                   '5': Activities.STANDING,
                   '6': Activities.LYING,
                 }

    def __init__(self, path: str='', variant: str='features'):
        self.__dr = DirectoryReader()
        self.__cr = CSVReader()
        self.__path = Path(path)
        self.__variant = variant

    def __load_features_data(self, part: str, path: str=''):
        # load features, labels and subjects
        with open(path + '/' + part + '/X_' + part + '.txt', 'r') as fxtrain, \
             open(path + '/' + part + '/y_' + part + '.txt', 'r') as fytrain, \
             open(path + '/' + part + '/subject_' + part + '.txt', 'r') as fsubject:

             fxtrain = fxtrain.read().splitlines()
             fytrain = fytrain.read().splitlines()
             fsubject = fsubject.read().splitlines()

        # populate datamodel
        subjects = []
        activities = []
        samples = []
        lastact = fytrain[0]
        lastsub = fsubject[0]

        for data, label, subject in zip(fxtrain, fytrain, fsubject):
            if lastact != label or lastsub != subject:
                activities.append(Activity(UCI_HAR.activitylist[lastact], samples))
                samples = []
                lastact = label

            if lastsub != subject:
                subjects.append(Subject(subject, activities))
                activities = []
                lastsub = subject

            samples.append(FeatureSample([float(d) for d in data.split()]))

        activities.append(Activity(UCI_HAR.activitylist[label], samples))

        subjects.append(Subject(subject, activities, part=part))

        return subjects
    
    def __load_features(self, path: str=''):
        return HARDataModel(trainsubjects=self.__load_features_data('train', path=path),
                            testsubjects=self.__load_features_data('test', path=path),
                            name=self.name)

    def __load_raw_data(self, part: str, path: str):
        filenames = [f'{sensorname}_{dim}_{part}.txt'
                        for sensorname in ('body_acc', 'body_gyro', 'total_acc')
                            for dim in ('x', 'y', 'z')]

        # Load body_acc,body_gyro,total_acc x,y,z data and concatenate to create (,128,7)
        fxtrain = []
        for fn in filenames:
            with (path/part/'Inertial Signals'/fn).open() as f:
                fxtrain.append(np.array([[float(d) for d in l.rstrip().split()] for l in f.readlines()], dtype=np.float32))
        fxtrain = np.dstack(fxtrain)
        print(fxtrain.shape)

        

        # load labels and subjects
        with open(path/part/f'y_{part}.txt', 'r') as fytrain, \
             open(path/part/f'subject_{part}.txt', 'r') as fsubject:

            fytrain = fytrain.read().splitlines()
            fsubject = fsubject.read().splitlines()

        # populate datamodel
        subjects = []
        activities = []
        samples = []
        lastact = fytrain[0]
        lastsub = fsubject[0]

        for data, label, subject in zip(fxtrain, fytrain, fsubject):
            if lastact != label or lastsub != subject:
                #activities.append(Activity(UCI_HAR.activitylist[lastact], samples))
                activities.append(Activity(int(float(lastact))-1, samples))
                samples = []
                lastact = label

            if lastsub != subject:
                subjects.append(Subject(subject, activities))
                activities = []
                lastsub = subject
            samples.append(WindowedSample(data))

        #activities.append(Activity(UCI_HAR.activitylist[label], samples))
        activities.append(Activity(int(float(lastact))-1, samples))

        subjects.append(Subject(subject, activities, part=part))

        return subjects

    def __load_raw(self, path: str=''):
        return HARDataModel(sets=HARDataModel.Sets(train=self.__load_raw_data('train', path=path),
                                                                test=self.__load_raw_data('test', path=path)),
                            name=self.name)

    def __call__(self):
        if self.__variant == 'features':
            return self.__load_features(self.__path)
        elif self.__variant == 'raw':
            return self.__load_raw(self.__path)

    @property
    def name(self):
        return f'{self.__class__.__name__}_{self.__variant}' 
