import numpy as np
from pathlib import Path
from collections import namedtuple
import time

from microai.datamodel import RawDataModel
from microai.utils.file import DirectoryReader, CSVReader

class GTSRB:
    def __init__(self, path: str='', width: int=32, height: int=32):
        self.__dr = DirectoryReader()
        self.__csvreader = CSVReader()
        self.__path = Path(path)
        self.__width = width
        self.__height = height

    def __load_train(self, path):
        import imageio
        from skimage import transform
        start = time.time()
        train_dir = self.__dr.read(path/'Final_Training'/'Images', ext='.ppm', recursive=True)

        trainX = []
        trainY = []
        for image in train_dir:
            imdata = imageio.imread(image)
            imdata = transform.resize(imdata, (self.__width, self.__height))
            imdata = imdata.astype(np.float32)
            trainX.append(imdata)
            trainY.append(int(image.parent.name))
        trainX = np.array(trainX)
        trainY = np.array(trainY)

        print(time.time() - start)

        return RawDataModel.Data(trainX, trainY)

    def __load_test(self, path):
        import imageio
        from skimage import transform
        start = time.time()
        test_dir = path/'Final_Test'/'Images'
        gt = self.__csvreader.read(path/'GT-final_test.csv',
                                   delimiter=';',
                                   labels=namedtuple('Labels',
                                            ['Filename', 'Width', 'Height', 'Roi_X1', 'Roi_Y1', 'Roi_X2', 'Roi_Y2', 'ClassId']),
                                   has_header=True
                                  )
        testX = []
        testY = []
        for img in gt.content:
            imdata = imageio.imread(test_dir/(img.Filename))
            imdata = transform.resize(imdata, (self.__width, self.__height))
            imdata = imdata.astype(np.float32)
            testX.append(imdata)
            testY.append(int(img.ClassId))
        testX = np.array(testX)
        testY = np.array(testY)

        print(time.time() - start)

        return RawDataModel.Data(testX, testY)

    def __call__(self, path: str=''):
        return RawDataModel(sets=RawDataModel.Sets(
                                    train=self.__load_train(self.__path),
                                    test=self.__load_test(self.__path)
                                ), name='GTSRB')

    def import_data(self):
        return RawDataModel.import_data(name='GTSRB')
