from .PostProcessing import PostProcessing
from microai.utils.logger import CSVLogger

from microai.utils.merge_dict import merge_dict

class QuantizationAwareTraining(PostProcessing):
    log = CSVLogger(name=__name__)

    def __init__(self, width: int=8, epochs: int=1, batch_size: int=32, model: dict=None, optimizer: dict=None, force_q: int=None):
        if width not in (8, 16):
            raise ValueError('{width}-bit quantization not supported')
        self.width = width
        self.epochs = epochs
        self.batch_size = batch_size
        self.qmodel = model if model is not None else {}
        self.qoptimizer = optimizer if optimizer is not None else {}
        self.force_q = force_q

    def __export_activations_range(self, model, model_name):
        import inspect
        from microai.learningmodel.pytorch.quantized_layers import quantized_layers

        with open(f'out/learningmodel/{model_name}_activations_range.txt', 'w') as f:
            for name,m in model.named_modules():
                if isinstance(m, quantized_layers):
                    print(f'{name},{m.max_activation},{m.max_activation2},{m.input_q},{m.activation_q},{m.weights_q}', file=f)

    def __evaluate(self, framework, model, trainset, testset, gpus):
        print('Evaluation on train dataset')
        acc = framework.evaluate(model, trainset, batch_size=self.batch_size, gpus=gpus)

        if testset is not None: # Don't evaluate if testset is empty
            print('Evaluation on test dataset')
            acc = framework.evaluate(model, testset, batch_size=self.batch_size, gpus=gpus)
        return acc

    def __call__(self, trainresult, model_conf):
        from microai.learningframework import PyTorch
        import microai.learningmodel.pytorch as learningmodels

        framework = trainresult.framework
        optimizer = framework.optimizer

        model = trainresult.model
        state_dict = model.state_dict()

        # Log baseline
        print("Before quantization: ")
        acc = self.__evaluate(framework, model, trainresult.trainset, trainresult.testset, gpus=trainresult.gpus)
        self.log(trainresult.i, trainresult.name, 'baseline', self.width, acc)

        # Create quantized model with parameters from original model
        quantized_model = getattr(learningmodels, 'Quantized' + model_conf['kind'])( # Use model name from config prefixed with Quantized
                                          input_shape=model.input_shape,
                                          output_shape=model.output_shape,
                                          bits=self.width,
                                          force_q=self.force_q,
                                          **model_conf.get('params', {}), **self.qmodel.get('params', {}))
        quantized_model.load_state_dict(state_dict, strict=False)
        framework.summary(quantized_model)

        # Evaluation without post-QAT
        print("After quantization without training: ")
        # Must train to update activation range
        framework.optimizer = None # disable optimizer
        framework.train(quantized_model,
                        trainset=trainresult.trainset,
                        validationset=trainresult.testset,
                        epochs=1,
                        batch_size=self.batch_size,
                        gpus=trainresult.gpus)
        framework.optimizer = self.qoptimizer
        merge_dict(self.qoptimizer, optimizer) # restore optimizer with overwritten params from QAT config
        print(framework.optimizer)
        acc = self.__evaluate(framework, quantized_model, trainresult.trainset, trainresult.testset, gpus=trainresult.gpus)
        self.log(trainresult.i, trainresult.name, 'notrain', self.width, acc)

        # You can plot the quantize training graph on tensorboard
        framework.train(quantized_model,
                        trainset=trainresult.trainset,
                        validationset=trainresult.testset,
                        epochs=self.epochs,
                        batch_size=self.batch_size,
                        gpus=trainresult.gpus)

        print("After post quantization aware training: ")
        acc = self.__evaluate(framework, quantized_model, trainresult.trainset, trainresult.testset, gpus=trainresult.gpus)
        self.log(trainresult.i, trainresult.name, 'post', self.width, acc)


        model_name = f'{trainresult.name}_q{self.width}'
        save_name = f'{model_name}_r{trainresult.i}'
        self.__export_activations_range(quantized_model, save_name)

        return trainresult._replace(model=quantized_model,
                                    name=model_name,
                                    params=framework.n_params(quantized_model),
                                    mem_params=(framework.n_params(quantized_model) * self.width) // 8,
                                    acc=acc)

    def process_name(self, name):
        return f'{name}_q{self.width}'

    def process_mem_params(self, mem_params):
        return lambda framework, model: (framework.n_params(model) * self.width) // 8
