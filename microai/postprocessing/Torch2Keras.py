import numpy as np
import tomlkit

import re
from pathlib import Path


from .PostProcessing import PostProcessing

class Torch2Keras(PostProcessing):
    def __init__(self, mapping):
        with Path(mapping).open() as f:
            self.__mapping = tomlkit.parse(f.read())

    def __map_object_to_dict(self, lookup_dict: dict, target_dict: dict, result_dict: dict=None):
        '''
        Get each layer of the PyTorch model according to the hierarchy defined in the 'mapping' parameter and create a flat dict
        with each layer as key and the name of the corresponding Keras layer as value (the leaves of the 'mapping' dict)
        '''
        result_dict = result_dict if result_dict is not None else {}

        if not isinstance(lookup_dict, dict):
            return {target_dict: lookup_dict}
        for k, v in lookup_dict.items():
            if isinstance(target_dict, list):
                result_dict = {**self.__map_object_to_dict(v, target_dict[int(k)], result_dict), **result_dict}
            else:
                result_dict = {**self.__map_object_to_dict(v, getattr(target_dict, k), result_dict), **result_dict}
        return result_dict

    def evaluate(self, model, x_test, y_test):

        return (model(x_test).max(dim=1)[1] == y_test).float().mean()

    def __extract_weight_type_from_name(self, name: str):
        wt = re.search('^.+?/(.*):.+?$', name)
        if wt is None:
            return None
        return wt.group(1)

    def __reformat_fc_weights_data(self, tf_layers):
        # After Flatten comes Dense, must reshape weights and swap axes for channels_first
        from tensorflow.keras.layers import Flatten, Dense
        for layer in tf_layers:
            if isinstance(layer, Flatten):
                outnodes = [n for n in layer._outbound_nodes]
                for outnode in outnodes:
                    dense = outnode.layer
                    assert isinstance(dense, Dense)
                    kernel = dense.kernel.numpy()
                    kernel = kernel.reshape(layer.input_shape[-1:] + layer.input_shape[1:-1] + (dense.units, )) # reshape using Flatten input shape (for example last Conv output)
                    kernel = kernel.swapaxes(0, 1)
                    kernel = kernel.reshape((-1, dense.units))
                    dense.set_weights([kernel] + dense.get_weights()[1:])

    def __call__(self, trainresult, model_conf):
        from microai.learningframework import Keras, PyTorch
        from microai.learningmodel.keras import ResNet as KerasResNet
        import microai.learningmodel.keras as keras_learningmodels
        from tensorflow.keras.layers import Conv1D, Dense

        framework = trainresult.framework
        model = trainresult.model
        model_nparams = framework.n_params(model)
        model.eval()

        name_to_layer_torch = {name: m for name, m in model.named_modules()}

        # Evaluate original model
        #ac = evaluate(model, x_test[:1473], y_test[:1473])
        #bc = evaluate(model, x_test[1473:], y_test[1473:])
        #print((ac + bc) / 2)

        framework = Keras()
        tf_model = getattr(keras_learningmodels, model_conf['kind'])(
                        input_shape=model.input_shape, output_shape=model.output_shape, **model_conf.get('params', {}))
        tf_model_nparams = framework.n_params(tf_model)

        framework.summary(tf_model)

        if model_nparams != tf_model_nparams:
            raise RuntimeError('Different number of parameters: PyTorch={model_nparams}, Keras={tf_model_nparams}')

        weights_name_tf_to_torch = { 'kernel': 'weight', 'bias': 'bias' }
        weights_transpose = {
            Conv1D: { 'kernel': (2, 1, 0), 'bias': (0,) },
            Dense: {'kernel': (1, 0), 'bias': (0,)}
        }

        layers_torch_to_tf = self.__map_object_to_dict(self.__mapping, model)
        layers_tf_to_torch = {v: k for k, v in layers_torch_to_tf.items()} # Reverse lookup

        for layer in tf_model.layers:
            if len(layer.weights) > 0: #layer need weights
                for weight in layer.weights:
                    weight_type = self.__extract_weight_type_from_name(weight.name)
                    torch_layer = layers_tf_to_torch[layer.name]
                    weight_values_torch = getattr(torch_layer, weights_name_tf_to_torch[weight_type])
                    weight_values_np = weight_values_torch.data.cpu().numpy()

                    weight.assign(weight_values_np.transpose(weights_transpose[layer.__class__][weight_type]))

        self.__reformat_fc_weights_data(tf_model.layers)

        # Compile Keras model
        tf_model.compile(loss='categorical_crossentropy', metrics=['categorical_accuracy'])

        # Evaluate TF model
        #x_test = x_test.data.cpu().numpy().transpose((0, 2, 1))
        #y_test = to_categorical(y_test.data.cpu().numpy())
        #print(tf_model.evaluate(x_test, y_test))
        #test_predictions = tf_model.predict(x_test)
        #print(tf.math.confusion_matrix(y_test.argmax(axis=1), test_predictions.argmax(axis=1)))

        ### Convert Activations Range file with TF layer names
        activations_range = {}
        try:
            with open(f'out/learningmodel/{trainresult.name}_r{trainresult.i}_activations_range.txt') as f:
                for l in f:
                    r = l.split(',')
                    activations_range[r[0]] = (float(r[1]), float(r[2]), int(r[3]), int(r[4]), int(r[5]))
            with open(f'out/learningmodel/{trainresult.name}_r{trainresult.i}_activations_range.h5.txt', 'w') as f:
                for lname, arange in activations_range.items():
                    layer_torch = name_to_layer_torch[lname]
                    if layer_torch not in layers_torch_to_tf:
                        print('Warning: unused layer', lname)
                    else:
                        print(f'{layers_torch_to_tf[layer_torch]},{arange[0]},{arange[1]},{arange[2]},{arange[3]},{arange[4]}', file=f)
        except FileNotFoundError:
            print('No activation range file found, skipping conversion')

        return trainresult._replace(model=tf_model, framework=framework, log=False)

    def process_framework(self, framework):
        from microai.learningframework import Keras
        return Keras()
