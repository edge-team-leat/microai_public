from .PostProcessing import PostProcessing

class RemoveKerasSoftmax(PostProcessing):
    def __init__(self):
        pass

    def __call__(self, trainresult, model_conf):
        from tensorflow.keras import Model
        from tensorflow.keras.layers import Activation
        from tensorflow.keras.activations import softmax

        model = trainresult.model
        assert isinstance(model.layers[-1], Activation)
        assert model.layers[-1].activation == softmax

        require_compile = model._is_compiled
        if require_compile:
            compile_params = {'loss': model.loss, 'optimizer': model.optimizer, 'metrics': model.compiled_metrics.metrics}

        # Remove softmax
        model = Model(model.input, model.layers[-2].output, name=model.name)

        if require_compile:
            model.compile(**compile_params)

        model.summary()
        return trainresult._replace(model=model)
