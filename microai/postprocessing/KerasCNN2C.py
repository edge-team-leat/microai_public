import importlib
import math
from pathlib import Path
import subprocess
import numpy as np
from collections import namedtuple

from .PostProcessing import PostProcessing

class KerasCNN2C(PostProcessing):
    import microai.deployment.kerascnn2c as deployers # Suggested deployers

    ActivationRange = namedtuple('ActivationRange', ['input', 'output', 'input_q', 'activation_q', 'weights_q'])

    def __init__(self, quantize):

        self.__quantize = quantize

        if quantize == 'float32':
            self.__fixed_point = False
            self.__width = 32
            self.__number_type = 'float'
            self.__long_number_type = 'float'
        elif quantize == 'int16':
            self.__fixed_point = True
            self.__width = 16
            self.__number_type = 'int16_t'
            self.__long_number_type = 'int64_t'
        elif quantize == 'int8':
            self.__fixed_point = True
            self.__width = 8
            self.__number_type = 'int8_t'
            self.__long_number_type = 'int32_t'
        else:
            raise ValueError(f'KerasCNN2C only supports no (float32) quantization, int8 or int16 quantization, got {quantize}')


    def convert(self, model, model_name, representative_dataset):
        from third_party.kerascnn2c.kerascnn2c import Converter

        self.__name = f'{model_name}_q{self.__quantize}'

        converter = Converter(
            write_file=True,
            fixed_point=self.__fixed_point,
            width=self.__width,
            number_type=self.__number_type,
            long_number_type=self.__long_number_type,
            output_path=Path('out')/'kerascnn2c'/self.__name)

        activations_range = {}
        if self.__fixed_point: # Activation range only when using fixed-point quantization
            first_q = None
            with open(f'out/learningmodel/{model_name}_activations_range.h5.txt') as f:
                for l in f:
                    r = l.split(',')
                    activations_range[r[0]] = self.ActivationRange(float(r[1]), float(r[2]), int(r[3]), int(r[4]), int(r[5]))
                    if first_q is None:
                        first_q = int(r[3])

            max_in = np.abs(representative_dataset).max()
            activations_range[model.layers[0].name] = self.ActivationRange(max_in, max_in, first_q, first_q, 0) # Model input range

        model.summary()
        self.__h = converter.convert_model(model, activations_range)
        return self

    @property
    def h(self):
        return self.__h

    @property
    def name(self):
        return self.__name

    def process_mem_params(self, mem_params):
        return lambda framework, model: (framework.n_params(model) * self.__width) // 8
