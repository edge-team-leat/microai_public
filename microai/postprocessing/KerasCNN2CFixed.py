import importlib
import math
from pathlib import Path
import subprocess

from .PostProcessing import PostProcessing

class KerasCNN2CFixed(PostProcessing):
    import microai.deployment.kerascnn2c_fixed as deployers # Suggested deployers

    def __init__(self, quantize):

        self.__quantize = quantize

        if quantize == 'float32':
            self.__fixed_point = 0
            self.__width = 32
            self.__number_type = 'float'
            self.__long_number_type = 'float'
            self.__number_min = -math.inf
            self.__number_max = math.inf
        elif quantize == 'int16':
            self.__fixed_point = 9
            self.__width = 16
            self.__number_type = 'int16_t'
            self.__long_number_type = 'int32_t'
            self.__number_min = -(2**15)
            self.__number_max = (2**15) - 1
        elif quantize == 'int8':
            self.__fixed_point = 4
            self.__width = 8
            self.__number_type = 'int8_t'
            self.__long_number_type = 'int16_t'
            self.__number_min = -(2**7)
            self.__number_max = (2**7) - 1
        else:
            raise ValueError(f'KerasCNN2C only supports no (float32) quantization, int8 or int16 quantization, got {quantize}')


    def convert(self, model, model_name, representative_dataset):
        from third_party.kerascnn2c_fixed.kerascnn2c import Converter

        print(f'Warning: {self.__class__.__name__} does not take a representative_dataset')

        self.__name = f'{model_name}_q{self.__quantize}'

        converter = Converter(
            write_file=True,
            fixed_point=self.__fixed_point,
            number_type=self.__number_type,
            long_number_type=self.__long_number_type,
            number_min=self.__number_min,
            number_max=self.__number_max,
            output_path=Path('out')/'kerascnn2c_fixed'/self.__name)

        model.summary()
        self.__h = converter.convert_model(model)
        return self

    @property
    def h(self):
        return self.__h

    @property
    def name(self):
        return self.__name

    def process_mem_params(self, mem_params):
        return lambda framework, model: (framework.n_params(model) * self.__width) // 8
