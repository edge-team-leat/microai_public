from dataclasses import dataclass, fields, asdict, astuple

@dataclass
class Stats:
    name: str = '',
    i: int = -1
    quantization: str = 'float32'
    params: int = -1
    mem_params: int = -1
    accuracy: float = -1 # between 0 and 1
    avg_time: float = -1 # in seconds
    rom_size: int = -1 # in bytes

    @classmethod
    def fieldnames(cls):
        return tuple((f.name for f in fields(Stats)))

    def asdict(self):
        return asdict(self)

    def astuple(self):
        return astuple(self)
