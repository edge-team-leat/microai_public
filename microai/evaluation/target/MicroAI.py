from datetime import datetime
from pathlib import Path
from collections import namedtuple
import csv
import time
import statistics
from microai.evaluation.Stats import Stats

class MicroAI:
    '''Custom evaluation loop for MicroAI embedded implementations like TFLite Micro and KerasCNN2C'''
    def __init__(self,
        dev: str=None,
        baudrate: int=921600):

        self.__dev = dev or '/dev/ttyUSB0'
        self.__baudrate = baudrate
        self.__timeout = 30 # 30 seconds transmission timeout

    def evaluate(self, model_kind, dataset, target: str, tag: str, limit: int=None):
        import serial
        print('Reset the target…')
        s = serial.Serial(self.__dev, self.__baudrate, timeout=self.__timeout)
        print(s.name)
        r = s.readline().decode('cp437')
        if 'READY' not in r:
            return None


        # create log directory
        (Path('logs')/'evaluate'/target).mkdir(parents=True, exist_ok=True)

        Result = namedtuple('Result', ['i', 'y', 'score', 'time'], defaults=[-1, -1, -1, -1])
        results = [] # read from target
        with (Path('logs')/'evaluate'/target/f'{tag}_{datetime.now()}.txt').open('w', newline='') as logfile:
            logwriter = csv.writer(logfile)
            logwriter.writerow(Result._fields)

            for i, line in enumerate(dataset.sets.test.x):
                print(f'{i}: ', end='')
                line = ','.join(map(str, line.flatten())) + '\r\n'
                s.write(line.encode('cp437')) # Send test vector

                r = s.readline() # Read acknowledge
                tstart = time.time() # Start timer
                r = r.decode('cp437')
                if not r or int(r) != len(line): # Timed out or didn't receive all the data
                    print(f'Transmission error: {r} != {len(line)}')
                    return None

                r = s.readline() # Read result
                tstop = time.time() # Stop timer
                r = r.decode('cp437').rstrip().split(',')
                r = Result(*r, time=tstop-tstart)
                print(r)
                results.append(r)

                # Log result to file
                logwriter.writerow(r)

                # Only infer 'limit' vectors
                if limit is not None and i + 1 >= limit:
                    break

        avg_time = statistics.mean([r.time for r in results])

        # Compute accuracy
        correct = 0
        for line,result in zip(dataset.sets.test.y, results):
            if line.argmax() == int(result.y):
                correct += 1

        accuracy = correct / len(results)

        return Stats(avg_time=avg_time, accuracy=accuracy)

        # avg it/secs
        # ram usage
        # rom usage
        # cpu type
        # cpu model
        # cpu freq
        # accuracy
        # power consumption
