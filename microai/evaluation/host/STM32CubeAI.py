from pathlib import Path
import re
import sys
from microai.utils.process import subprocesstee                                                                                     
from microai.evaluation.Stats import Stats

import microai.evaluation.STM32CubeAI as STM32CubeAIBase

class STM32CubeAI(STM32CubeAIBase):
    def __init__(self, *args, **kwargs):
        super().__init__(mode='x86')
