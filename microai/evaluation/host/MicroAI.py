from datetime import datetime
from pathlib import Path
from collections import namedtuple
import time
import sys
import os
import concurrent.futures
from microai.utils.process import subprocesstee
from microai.evaluation import Stats
import numpy as np

class MicroAI:
    '''Custom evaluation loop for MicroAI host implementations like KerasCNN2C Linux example'''

    def __init__(self, *args, **kwargs):
        self.__deploydir = Path('out')/'deploy'/'Linux'

        self.__float_to_hex = np.vectorize(lambda x: float(x).hex())

    def __run(self, cmd, *args):
        print(cmd, *args)
        returncode, outputs = subprocesstee.run(str(cmd), *args)
        return outputs

    def __run_on_split(self, csvdir, tag, i, testX, testY):
        np.savetxt(csvdir/f'testX_{i}.csv', self.__float_to_hex(testX), delimiter=",", fmt='%s')
        np.savetxt(csvdir/f'testY_{i}.csv', testY, delimiter=",", fmt='%d')

        tstart = time.time() # Start timer
        outputs = self.__run(str(self.__deploydir/f'{tag}_Linux'), csvdir/f'testX_{i}.csv', csvdir/f'testY_{i}.csv')
        tstop = time.time() # Stop timer

        accuracy = float(outputs[2].decode().split(' ')[2])

        return namedtuple('Result', ['acc', 'time'])(accuracy, (tstop - tstart))

    def evaluate(self, model_kind, dataset, target: str, tag: str, limit: int=None):
        if limit is not None:
            raise ValueError(self.__class__.__name__, 'limit parameter unsupported')

        # create log directory
        (Path('logs')/'evaluate'/target).mkdir(parents=True, exist_ok=True)

        # create data CSV dir
        csvdir = Path('out')/'data'/dataset.name/'csv'
        csvdir.mkdir(parents=True, exist_ok=True)

        # Flatten test vectors
        testX = dataset.sets.test.x.reshape((dataset.sets.test.x.shape[0], -1))

        # Split test into chunks
        chunks = os.cpu_count()
        testX = np.array_split(testX, chunks)
        testY = np.array_split(dataset.sets.test.y, chunks)

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [executor.submit(self.__run_on_split, csvdir, tag, i, x, y) for i, (x, y) in enumerate(zip(testX, testY))]
            results = [f.result() for f in futures]

        avg_time = sum(r.time for r in results) / len(dataset.sets.test.y)
        accuracy = sum(r.acc * len(y) for r, y in zip(results, testY)) / len(dataset.sets.test.y)

        return Stats(avg_time=avg_time, accuracy=accuracy)

        # avg it/secs
        # ram usage
        # rom usage
        # cpu type
        # cpu model
        # cpu freq
        # accuracy
        # power consumption
