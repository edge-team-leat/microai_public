import logging
from collections import namedtuple
from datetime import datetime
from pathlib import Path

from .CSVFormatter import CSVFormatter

class CSVLogger:
    logpath = Path('logs')
    prefix = ''

    def __init__(self, name: str, file: Path=None):
        self.__name = name
        self.__file = file
        self.logger = None
        self.filehandler = None
        self.__content = []
        self.__fields = None
        self.__field_type = None

    def __lazy_init(self):
        """Lazy init allows setting configuring global parameters such as path/prefix even after instanciation
            (when classes with CSVLogger as class member are loaded) but before logger is called for the first time"""
        self.logger = logging.getLogger(f'microai.{self.__name}')
        self.logger.propagate = False # Don't use logger hierarchy since somehow root logger may have a handler already
        self.logger.setLevel(logging.DEBUG)

        if not self.logger.hasHandlers():
            if self.__file is None:
                self.__file = Path(self.__name)/f'{self.prefix}{datetime.now()}.csv'
            self.__file = self.logpath/self.__file
            self.__file.parent.mkdir(parents=True, exist_ok=True)

            self.filehandler = logging.FileHandler(self.__file, delay=True)
            self.filehandler.setLevel(logging.DEBUG)
            self.filehandler.setFormatter(CSVFormatter())
            self.logger.addHandler(self.filehandler)

    def __call__(self, *args):
        if self.logger is None:
            self.__lazy_init()
        self.logger.info(args)

        if self.__field_type is not None:
            self.__content.append(self.__field_type(*args))
        else:
            self.__content.append(args)

    def __del__(self):
        if self.filehandler is not None:
            self.filehandler.close()
        if self.logger is not None:
            self.logger.removeHandler(self.filehandler)

    def __repr__(self):
        return str(self.content)

    @property
    def fields(self):
        return self.__fields

    @fields.setter
    def fields(self, val):
        if self.__fields is not None:
            raise AttributeError(f'\'fields\' can only be assigned once for each instance of {self.__class__.__name__}')
        self.__fields = val
        self.__field_type = namedtuple(self.__name, self.__fields)

        # Write column names to file
        if self.logger is None:
            self.__lazy_init()
        self.logger.info(val)

    @property
    def content(self):
        return self.__content
