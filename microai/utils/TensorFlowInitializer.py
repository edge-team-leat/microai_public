class TensorFlowInitializer:
    def __init__(self):
        pass


    def set_seed(self, seed):
        # From https://www.reddit.com/r/MachineLearning/comments/ge32bi/how_to_reliably_compare_experiments_when_tuning/fpl4owx/
        import os
        import sys

        os.environ['TF_DETERMINISTIC_OPS'] = '1'
        # If PYTHONHASHSEED is not set to 0 restart interpreter (env var has to be set before python starts)
        if os.environ.get('PYTHONHASHSEED', '') != str(seed):
            os.execle(sys.executable, sys.executable, *sys.argv, {'PYTHONHASHSEED': str(seed), **os.environ})

        import random
        random.seed(seed)

        import numpy as np
        np.random.seed(seed)

        try:
            import tensorflow as tf
            tf.random.set_seed(seed)
        except ImportError:
            print('Warning: TensorFlow not loaded and not seeded')

        try:
            import torch
            torch.manual_seed(seed)

            import pytorch_lightning
            pytorch_lightning.seed_everything(seed)
        except ImportError:
            print('Warning: PyTorch not loaded and not seeded')

    def __call__(self, reserve_gpu=True, gpu_memory_growth=True, debug=False, seed=None):
        if not debug:
            import os
            # Needs to be done before the first tensorflow import ever
            os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # or any {'0', '1', '2'}

        if seed is not None:
            self.set_seed(seed)

        try:
            import tensorflow as tf
        except ImportError:
            print('Warning: TensorFlow not loaded, not setting up devices')
            return

        if debug:
            tf.debugging.set_log_device_placement(True)

        if not reserve_gpu:
            # Hide GPUs when not training to not allocate any resource
            tf.config.set_visible_devices([], 'GPU')
        else:
            # From https://www.tensorflow.org/guide/gpu
            # Limiting GPU memory growth
            gpus = tf.config.experimental.list_physical_devices('GPU')
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, gpu_memory_growth)
            if debug:
                logical_gpus = tf.config.experimental.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
