import copy

def merge_dict(dict_a, dict_b):
    '''Merge elements from dict_b into dict_a if they are missing'''
    def merger(table, template):
        for k, v in template.items():
            if not k in table:
                table[k] = copy.deepcopy(v)
            elif isinstance(v, dict):
                merger(table[k], template[k])

    merger(dict_a, dict_b)

