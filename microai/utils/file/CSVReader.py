import csv
from collections import namedtuple
from types import SimpleNamespace

class CSVReader:
    def read_callback(self, filename: str, callback, delimiter: str = ',', labels: namedtuple = None, has_header = True):
        with open(filename, newline='') as f:
            reader = csv.reader(f, delimiter=delimiter)

            if has_header:
                labelsrow = next(reader, [])
                if not labels:
                    labels = namedtuple('Labels', labelsrow)

            callback(filename=filename, content=(labels(*row) for row in reader))

    def read(self, filename, *args, **kwargs):
        full_content = SimpleNamespace(val=[])
        def callback(filename, content):
            full_content.val = list(content)

        self.read_callback(filename=filename, callback=callback, *args, **kwargs)
        return namedtuple('CSV', ['filename', 'content'])(filename, full_content.val)
