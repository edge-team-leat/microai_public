import asyncio
import os

class AsyncTeeProtocol(asyncio.SubprocessProtocol):
    def __init__(self, done_future, files: dict=None):
        super().__init__()
        self.done = done_future
        self.buffers = {}
        self.files = files if files is not None else {}

    def connection_made(self, transport):
        self.transport = transport

    def pipe_data_received(self, fd, data):
        os.write(fd, data)
        if fd not in self.buffers:
            self.buffers[fd] = bytearray()
        self.buffers[fd].extend(data)
        if fd in self.files:
            self.files[fd].write(data)

    def process_exited(self):
        return_code = self.transport.get_returncode()
        self.done.set_result((return_code, self.buffers))
