import asyncio

from .AsyncTeeProtocol import AsyncTeeProtocol

async def async_exec(*argv, files: dict=None):
    files = files if files is not None else {}
    loop = asyncio.get_running_loop()
    cmd_done = loop.create_future()
    proc = loop.subprocess_exec(lambda: AsyncTeeProtocol(cmd_done, files), *argv, stdin=None)
    try:
        transport, protocol = await proc
        await cmd_done
    finally:
        transport.close()

    return cmd_done.result()

def run(*argv, files: dict=None):
    files = files if files is not None else {}
    files = {fd.fileno() if hasattr(fd, 'fileno') else fd: f for fd, f in files.items()} # convert file objects to file descriptor
    return asyncio.run(async_exec(*argv, files=files))

