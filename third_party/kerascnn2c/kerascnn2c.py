#!/usr/bin/env python3

from pathlib import Path
import sys
import numpy as np
import tensorflow as tf
# We don't need a GPU, don't request it
tf.config.set_visible_devices([], 'GPU')
from tensorflow.keras.models import load_model
from kerascnn2c import Converter

from collections import namedtuple
ActivationRange = namedtuple('ActivationRange', ['input', 'output'])

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print(f'Usage: {sys.argv[0]} <model.h5> <output_dir>')
		sys.exit(1)

	#from tensorflow_addons.optimizers import SGDW
	custom_objects = {
		#'Addons>SGDW': SGDW # In case you need to load a model with a custom SGDW optimizer for example
	}

	model = load_model(sys.argv[1], custom_objects=custom_objects)

	representative_dataset = np.loadtxt(sys.argv[3], delimiter=',').reshape((-1, 128, 9))


	activations_range = {}
	with open(sys.argv[1] + '_activations_range.txt') as f:
	    for l in f:
	        r = l.split(',')
	        activations_range[r[0]] = ActivationRange(float(r[1]), float(r[2]))

	max_in = np.abs(representative_dataset).max()
	activations_range[model.layers[0].name] = ActivationRange(max_in, max_in) # Model input range

	width = int(sys.argv[4])

	if width == 8:
		Converter(output_path=Path(sys.argv[2]),
					fixed_point=True,
					width=8,
					number_type='int8_t',
					long_number_type='int16_t').convert_model(model, activations_range)

	elif width == 16:
		Converter(output_path=Path(sys.argv[2]),
					fixed_point=True,
					width=16,
					number_type='int16_t',
					long_number_type='int32_t').convert_model(model, activations_range)
	else:
		Converter(output_path=Path(sys.argv[2]),
					fixed_point=False,
					width=32,
					number_type='float',
					long_number_type='float').convert_model(model, activations_range)
