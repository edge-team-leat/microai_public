####################################################################################################
#----------------------------------------------MicroAI---------------------------------------------#
#-----------------------------------Version 1, 15 February 2021------------------------------------#
#..................................................................................................#
MicroAI - Copyright (C) 2021 CNRS/UCA/ELLCIE HEALTHY. All Rights Reserved.
#..................................................................................................#
MicroAI is a software framework for end-to-end deep neural networks training, quantization and deployment onto embedded devices. This framework is designed as an alternative to existing proprietary inference engines on microcontrollers. Our framework can be easily adjusted and/or extended for specific use cases. The training phase relies on Keras or PyTorch. Execution using single precision 32-bit floating-point as well as fixed-point on 8- and 16 bits integers are supported.
#..................................................................................................#
The software is under GNU Affero General Public License v3.0 license [GNU AGPLv3]. 
For more details about the license Terms and Conditions, please consult the “README-LICENCE” file.
#..................................................................................................#
Contributors: Benoit MIRAMOND; Alain PEGATOQUET; Pierre-Emmanuel NOVAC
Contributors email address: [Benoit.miramond@univ-cotedazur.fr, Alain.PEGATOQUET@univ-cotedazur.fr, Pierre-Emmanuel.Novac@univ-cotedazur.fr]
#..................................................................................................#
This software is being developed by the Contributors, employees of UCA and members of the Laboratory “Laboratoire d'Electronique, Antennes et Télécommunications” [LEAT, UMR7248], a joint research unit between UCA and CNRS, located at Bât. Forum – Campus SophiaTech – 930 route des Colles – 06903 SOPHIA ANTIPOLIS Cedex, [FRANCE]. 
#..................................................................................................#
CNRS: Centre National de la Recherche Scientifique, http://www.cnrs.fr/index.php/en 
UCA: Université Côte d’Azur, http://univ-cotedazur.fr/en 
ELLCIE HEALTHY: https://ellcie-healthy.com/en/home/
#..................................................................................................#