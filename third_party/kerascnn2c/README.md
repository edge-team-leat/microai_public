# KerasCNN2C

Converts a pre-trained Keras .h5 model to C code for inference.

Generates C code using `channels_first` data format from a model using `channels_last` (Keras default) data format.

## Supported layers

 - **Activation**: ReLU (combined to previous Conv1D, Dense, MaxPooling1D, AveragePooling1D AddV2), Softmax
 - **Conv1D**: optional bias, valid padding only
 - **Dense**: optional bias
 - **MaxPooling1D**: valid padding only
 - **AveragePooling1D**: valid padding only
 - **Flatten**: implies reordering next layer's kernel for data format conversion
 - **ZeroPadding1D**: combined with next Conv1D
 - **AddV2**


## Dependencies

```
python >= 3.6
python-jinja >= 2
python-numpy
python-tensorflow >= 2
```

## Usage

### Generate C code from Keras .h5

```
kerascnn2c.py <model.h5> <output directory>
```

### Use in your C code

Include the model: (can also be built as a separate object)
```
#include "model.h"
```

Allocate `inputs` and `outputs` arrays with correct dimensions. Remember that `inputs` must have `channels_first` data format.

Call it in your C code:
```
cnn(inputs, outputs);
```

Add the source file `model.c` to your build system. It includes all the other source files for layers, don't add these to the build system.

## Examples
See the `examples/Linux` directory for a demo console application to evaluate model accuracy.

`examples/KerasCNN2C-NucleoL476RG` contains an STM32CubeIDE project for the Nucleo-L476RG board that's currently broken due to some recent changes

## Documentation
Nothing much…

### Source tree
`kerascnn2c/Allocator.py`: manages activation buffer allocation. Tries to group all buffers into one, except when they cannot be overwritten (dependencies).

`kerascnn2c/Converter.py`: the actual conversion code, parses a Keras model and use the template file associated to each layer to generate C code. When weights have to be written, they are optionally quantized to fixed-point by setting the appropriate parameters of `Converter` constructor (see its definition)

`kerascnn2c/Validator.py`: work in progress, should contain functions to check if a model can be successfully converted. For now only check activation function.

`kerascnn2c/assets/`: contains the templates to generate C inference code

`kerascnn2c/assets/layers/`: contains the implementation of the various supported layers

`kerascnn2c/assets/layers/weights`: contains the support for the trainable layers weights
