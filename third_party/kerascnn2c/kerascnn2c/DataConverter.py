import numpy as np

class DataConverter:
	def __init__(self, fixed_point: bool, number_type: str):
		self.fixed_point = fixed_point
		self.number_type = number_type

	def dtype2ctype(self, dtype):
		if dtype == np.float32:
			return 'float'
		elif dtype == np.float64:
			return 'double'
		elif dtype == np.int32:
			return 'uint32_t'
		elif dtype == np.int64:
			return 'uint64_t'

	def ndarray2cinitializer(self, arr, scale_factor):

		if np.isscalar(arr):
			if self.fixed_point:
#			if self.fixed_point > 0: # Fixed point int16, scaling/clamping
#				arr *= (1<<scale_factor)
#				arr = int(arr)
#				arr = max(self.number_min, min(self.number_max, arr))
				return str(arr)
			else: # use hex for exact representation, no scaling/clamping, gets promoted to float64 from float32 but no big deal
				return float(arr).hex()

		text = ''
		text += '{'

		text += ', '.join([self.ndarray2cinitializer(subarr, scale_factor) for subarr in arr])

		text += '}'
		text += '\n'
		return text

	def tensor2carray(self, arr, name, scale_factor):
		arrdata = self.ndarray2cinitializer(arr, scale_factor)

		return {'name': name, 'data': arrdata, 'dtype': self.number_type, 'shape': arr.shape} #dtype2ctype(arr.dtype)}

