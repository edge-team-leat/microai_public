#TODO: Work in progress
class Validator:
	'''Class used to validate various parts of the model to make sure they conform to the limitation of the conversion tool'''

	def __init__(self):
		pass

	def validate_combined_activation(self, layer):
		from tensorflow.keras.layers import Activation
		from tensorflow.keras.activations import linear, relu, softmax

		if not hasattr(layer, 'activation'): # No activation, ok
			return True

		if layer.activation == softmax:
			if not isinstance(layer, Activation): # not a standalone softmax
				print(f'Error: softmax activation must be used as a standalone layer, not combined to another layer ({layer.__class__.__name__})')
				return False
		elif layer.activation not in [linear, relu]:
			print(f'Error: activation function {layer.activation} not supported')
			return False

		return True

	def validate_batchnorm(self, layer):
		from tensorflow.keras.layers import BatchNormalization

		if not isinstance(layer, BatchNormalization): # Not BatchNorm, ignore
			return True

		# Detect variable depth not matching input_shape channels
		wrong_shape_vars = [var for var in [layer.moving_mean, layer.moving_variance, layer.beta, layer.gamma] if var.shape != layer.input_shape[-1]]
		for var in wrong_shape_vars:
			print(f'Variable {var.name} with shape {var.shape} does not match last dimension of input shape {layer.input_shape[-1]}')

		return not wrong_shape_vars
	
	def validate_node(self, node):
		valid = True

		# Validate combined activation
		valid = valid and self.validate_combined_activation(node.layer)
		valid = valid and self.validate_batchnorm(node.layer)

		return valid
