import math
import numpy as np
import tensorflow as tf

class Quantizer:
	weights_name = ['kernel', 'bias']

	def __init__(self, width: int):
		self.width = width
		self.number_min = -(2**(width - 1))
		self.number_max = 2**(width -1 ) - 1

		if width == 8:
			self.type = tf.int8
		elif width == 16:
			self.type = tf.int16
		else:
			raise ValueError(f'Unsupported width {width}, supported with: 8, 16')

	def __maxWeight(self, maxi):
		#liste_max = []
		#index=0
		#maxi = 0
		#global train
		#if(maxi==0 or train==1):
		#	w = weight
		#	v = w.view(-1)
		#	maxi=torch.max(torch.abs(v))
		#maxi = torch.max(torch.abs(v)) #.cpu().data.numpy()
		n=0
		if(maxi<1 and maxi!=0):
			while(maxi<1):
				maxi*=2
				n+=1
			return n-1
		elif(maxi>=2):
			while(maxi>=2):
			   maxi/=2
			   n-=1
			return n-1
		else:
			return n-1

	def __scale_factor_from_max(self, max_value):
		return self.width - 1 + self.__maxWeight(abs(max_value))

	def __scale_factor_from_min_max(self, min_value, max_value):
		absolute_max_value = max(abs(max_value), abs(min_value)) # range is symmetric

		return self.width - 1 + self.__maxWeight(absolute_max_value)

		int_bits = 1 + int(math.log2(absolute_max_value))
		#print('int_bits', int_bits, absolute_max_value, self.width)
		frac_bits = self.width - int_bits - 1 # keep 1 bit for sign

		#frac_bits = int(math.ceil(-math.log2(3*max_value / (2 ** (self.width - 1)))))

		return frac_bits

	def __find_weights_scale_factor(self, node):
		min_value = math.inf
		max_value = -math.inf

		min_valueb = math.inf
		max_valueb = -math.inf


		for weights in node.layer.get_weights():
			min_value = min(min_value, weights.min())
			max_value = max(max_value, weights.max())
			#min_value = min(min_value, weights.mean() - 3*weights.std())
			#if node.layer.name == 'dense':
			#	print('weights.mean() - 3*weights.std()', weights.mean() - 3*weights.std())
			#max_value = max(max_value, weights.mean() + 3*weights.std())
			#if node.layer.name == 'dense':
			#	print('weights.mean() + 3*weights.std()', weights.mean() + 3*weights.std())
			#max_value = max(max_value, weights.std())

			#	print(f'{weights.std()=} {weights.mean()=}')

		#if node.layer.name == 'dense_1':
		#	print(min_value, max_value)

		#if node.layer.name == 'dense':
		#	print(node.layer.kernel[216][45], node.layer.kernel.numpy().max())
		#	print(f'{min_valueb} {max_valueb} {min_value} {max_value} ')

		return self.__scale_factor_from_min_max(min_value, max_value)

	def __quantize_weights(self, node, scale_factor):
		for weights_name in self.weights_name:
			weights = getattr(node.layer, weights_name)
			if weights is None:
				continue # This layer doesn't have this weights type

			new_weights = weights * (1 << scale_factor)
			new_weights = tf.math.floor(new_weights)
			new_weights = tf.clip_by_value(new_weights, self.number_min, self.number_max)
			new_weights = tf.cast(new_weights, self.type)

			# Register new weights to have them in layer.get_weights()
			weights_var = node.layer.add_weight(name=weights_name, shape=weights.shape, dtype=new_weights.dtype)
			weights_var.assign(new_weights)

			setattr(node.layer, weights_name, weights_var)

	def quantize_weights(self, node, activations_range):
		if len(node.layer.get_weights()) > 0:
			if activations_range is None or activations_range.weights_q is None:
				node.weights_scale_factor = self.__find_weights_scale_factor(node)
			else:
				node.weights_scale_factor = activations_range.weights_q
			#node.weights_scale_factor = max(node.weights_scale_factor, 4)
			#node.weights_scale_factor = 4
			#node.weights_scale_factor = 9
			#node.weights_scale_factor = 4
			#if node.layer.name == 'dense':
			#	node.weights_scale_factor -= 2
			print('Weights: ', node.layer.name, node.weights_scale_factor)
			self.__quantize_weights(node, node.weights_scale_factor)

	def quantize_activations(self, node, activation_range):
		#if self.width == 8:
		#	node.output_scale_factor = 7 # fixed quantization of activations for now
		#else:
		#	node.output_scale_factor = 9

		if activation_range is None:
			node.output_scale_factor = min([n.output_scale_factor for n in node.innodes])
		else:
			for innode in node.innodes:
				if innode.output_scale_factor is None:
					if activation_range.input_q is None:
						innode.output_scale_factor = self.__scale_factor_from_max(activation_range.input)
					else:
						innode.output_scale_factor = activation_range.input_q

			if activation_range.activation_q is None:
				node.output_scale_factor = self.__scale_factor_from_max(activation_range.output)
			else:
				node.output_scale_factor = activation_range.activation_q

		#if isinstance(node.layer, tf.keras.layers.Dense):
		#	node.output_scale_factor = 8
		#if node.layer.name == 'dense':
			#print(activation_range)
			#node.output_scale_factor = -2
		#node.output_scale_factor = max(node.output_scale_factor, 4)
		#node.output_scale_factor = 4
		#if node.layer.name in ['conv1d', 'input_1', 'max_pooling1d', 'conv1d_1', 'max_pooling1d_1', 'conv1d_2', 'flatten', 'dense']:
		#if node.layer.name in ['max_pooling1d_1', 'conv1d_2']:
		#	node.output_scale_factor -= 2
		#node.output_scale_factor = 9
		#node.output_scale_factor = 4
		print('Activations: ', node.layer.name, node.output_scale_factor, activation_range)

	def compute_activations_range(self, keras_model, representative_dataset):
		get_all_layer_outputs = tf.keras.backend.function(keras_model.inputs,
										  [l.output for l in keras_model.layers])
		layers_outputs = get_all_layer_outputs(representative_dataset)
		activations_range = { layer: (output.min(), output.max())
		#activations_range = { layer: (output.mean() - 3*output.std(), output.mean() + 3*output.std())
		#activations_range = { layer: (output.mean() - 3*output.std(), output.std())
								for layer, output in zip(keras_model.layers, layers_outputs) }
		print(activations_range)
		return activations_range
