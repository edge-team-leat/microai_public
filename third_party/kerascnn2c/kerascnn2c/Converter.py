import sys
import math
from pathlib import Path
from packaging import version
import numpy as np

import tensorflow as tf
from tensorflow.keras.activations import relu, softmax

# In TensorFlow 2.6.0, Keras is being extract out again
if version.parse(tf.__version__) >= version.parse('2.6.0'):
	from keras.layers import Layer, ZeroPadding1D, Conv1D, AveragePooling1D, MaxPooling1D, Activation, Flatten, Dense, BatchNormalization
	try:
		from keras.src.engine.input_layer import InputLayer # Keras >= 2.13.1
		from keras.src.layers.core import TFOpLambda
	except ImportError:
		from keras.engine.input_layer import InputLayer # Keras < 2.13.0
		from keras.layers.core import TFOpLambda
else:
	from tensorflow.keras.layers import Conv1D, MaxPooling1D, AveragePooling1D, Dense, Flatten, Activation, ZeroPadding1D, BatchNormalization, Input
	from tensorflow.python.keras.engine.input_layer import InputLayer
	from tensorflow.python.keras.layers.core import TFOpLambda

import jinja2

from .Allocator import Allocator
from .Validator import Validator
from .ModelGraph import ModelGraph
from .Quantizer import Quantizer
from .DataConverter import DataConverter

class Converter:
	layer_template_files = {
		AveragePooling1D: 'averagepool.cc',
		Conv1D: 'conv.cc',
		Dense: 'fc.cc',
		MaxPooling1D: 'maxpool.cc',
		Activation: 'activation.cc',
		Flatten: 'flatten.cc',
		TFOpLambda: 'operator.cc',
		ZeroPadding1D: 'zeropadding.cc',
		BatchNormalization: 'batchnorm.cc',
		InputLayer: None # Nothing to generate for input layer
	}

	def __init__(self,
		template_path: Path=Path(__file__).parent/'assets',
		output_path: Path=Path('output'),
		write_file: bool=True,

		# Default floating point config # fixed point 8 bits, fixed point 16 bits
		fixed_point: int=True,
		width: int=8, # 8, 16
		number_type: str='int8_t', # 'int8_t', 'int16_t'
		long_number_type: str='int16_t', # 'int16_t', 'int32_t'
		):

		self.validator = Validator()
		self.dataconverter = DataConverter(fixed_point=fixed_point, number_type=number_type)
		if fixed_point:
			self.quantizer = Quantizer(width=width)
			# For template
			self.number_max = self.quantizer.number_max
			self.number_min = self.quantizer.number_min

		self.output_data_format = 'channels_first'

		self.template_path = template_path
		self.output_path = output_path
		self.output_path_weights = output_path/'weights'

		self.output_path.mkdir(parents=True, exist_ok=True)
		self.output_path_weights.mkdir(parents=True, exist_ok=True)

		self.write_file = write_file

		self.fixed_point = fixed_point
		self.number_type = number_type
		self.long_number_type = long_number_type

	def get_layer_input_layer(self, layer):
		try:
			return [l for n in layer._inbound_nodes for l in n.inbound_layers]
		except TypeError:
			return [n.inbound_layers for n in layer._inbound_nodes]

	def weights2carray(self, node):
		weights = {}
		if hasattr(node.layer, 'kernel'):
			arr = node.layer.kernel.numpy()
			if self.output_data_format == 'channels_first':
				arr = arr.swapaxes(0, -1) # C implementation uses channels_first data format, keras is usually channels_last
			weights['kernel'] = self.dataconverter.tensor2carray(arr, node.layer.name + '_kernel', node.weights_scale_factor)
		if hasattr(node.layer, 'bias') and node.layer.use_bias:
			arr = node.layer.bias.numpy()
			weights['bias'] = self.dataconverter.tensor2carray(arr, node.layer.name + '_bias', node.weights_scale_factor)
		return weights

	def write_layer_function(self, template, node):
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path/'layers')).get_template(template)
		rendered = template.render(node=node, fixed_point=self.fixed_point, prod=math.prod)
		if self.write_file:
			with (self.output_path/f'{node.layer.name}.c').open('w') as f:
				f.write(rendered)
		return rendered

	def write_layer_weights(self, template, node):
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path/'layers'/'weights')).get_template(template)
		rendered = template.render(node=node, weights=self.weights2carray(node))
		if self.write_file:
			with (self.output_path_weights/f'{node.layer.name}.c').open('w') as f:
				f.write(rendered)
		return rendered

	def write_model(self, modelgraph, allocation):
		# Model header
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path)).get_template('model.hh')
		rendered1 = template.render(nodes=modelgraph.nodes, allocation=allocation, fixed_point=self.fixed_point)
		if self.write_file:
			with (self.output_path/'model.h').open('w') as f:
				f.write(rendered1)

		# Model implementation
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path)).get_template('model.cc')
		rendered2 = template.render(nodes=modelgraph.nodes, allocation=allocation)
		if self.write_file:
			with (self.output_path/'model.c').open('w') as f:
				f.write(rendered2)
		return rendered1 + '\n' + rendered2

	def write_numeric_header(self):
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path)).get_template('number.hh')
		rendered = template.render(**self.__dict__)
		if self.write_file:
			with (self.output_path/'number.h').open('w') as f:
				f.write(rendered)
		return rendered

	def combine_zeropadding(self, modelgraph):
		zeropaddingnodes = set(node for node in modelgraph.nodes if isinstance(node.layer, ZeroPadding1D))
		for zeropaddingnode in zeropaddingnodes:
			for outnode in zeropaddingnode.outnodes:
				outnode.layer.padding = zeropaddingnode.layer.padding
			modelgraph.delete_node(zeropaddingnode)
		return modelgraph

	def combine_relu(self, modelgraph, activations_range):
		relunodes = set(node for node in modelgraph.nodes if isinstance(node.layer, Activation) and node.layer.activation == relu)
		for relunode in relunodes:
			for innode in relunode.innodes: # warning: activations_range unsupported with multiple inputs to relu
				innode.layer.activation = relu
				if relunode.layer in activations_range:
					# copy activations range output of relu to the input layer
					activations_range[innode.layer] = activations_range[innode.layer]._replace(
						output=activations_range[relunode.layer].output,
						activation_q=activations_range[relunode.layer].activation_q
					)
			modelgraph.delete_node(relunode)
		return modelgraph

	def reformat_fc_weights_data(self, modelgraph):
		for node in modelgraph.nodes:
			if isinstance(node.layer, Flatten): # After Flatten comes Dense, must reshape weights and swap axes for channels_first
				# After Flatten comes Dense, must reshape weights and swap axes for channels_first
				for outnode in node.outnodes:
					dense = outnode.layer
					assert isinstance(dense, Dense)
					kernel = dense.kernel.numpy()
					kernel = kernel.reshape(node.layer.input_shape[1:] + (dense.units, )) # reshape using Flatten input shape (for example last Conv output)
					kernel = kernel.swapaxes(0, 1)
					kernel = kernel.reshape((-1, dense.units))
					dense.set_weights([kernel] + dense.get_weights()[1:])
		return modelgraph

	# Convert BatchNorm mean, variance, offset and scale to kernel and bias for optimization
	def convert_batchnorm_weights(self, modelgraph):
		for node in modelgraph.nodes:
			if not isinstance(node.layer, BatchNormalization):
				continue

			stdev = tf.math.sqrt(node.layer.moving_variance + node.layer.epsilon)
			node.layer.kernel = node.layer.gamma / stdev
			node.layer.bias = node.layer.beta - node.layer.gamma * node.layer.moving_mean / stdev
			node.layer.use_bias = True
		return modelgraph

	# Operators (Add…) layers have names invalid as C tokens
	def rename_operators(self, modelgraph):
		for node in modelgraph.nodes:
			if not isinstance(node.layer, TFOpLambda):
				continue

			node.layer._name = node.layer.name.replace('.', '')
		return modelgraph

	
	def convert_model(self, model, activations_range):
		activations_range = {model.get_layer(k): v for k, v in activations_range.items()}
		#if self.fixed_point:
		#	activations_range = self.quantizer.compute_activations_range(model, representative_dataset)
		#else:
		#	activations_range = {l: None for l in model.layers}

		modelgraph = ModelGraph.from_keras(model)
		print(modelgraph)


		modelgraph = self.combine_zeropadding(modelgraph) # Combine ZeroPadding with next layer (Conv1D)
		modelgraph = self.combine_relu(modelgraph, activations_range) # Combine ReLu with previous layer (Conv1D/Dense), activations range must be copied to previous layer

		print('After optimization:')
		print(modelgraph)

		if self.output_data_format == 'channels_first':
			modelgraph = self.reformat_fc_weights_data(modelgraph)
		modelgraph = self.convert_batchnorm_weights(modelgraph)

		modelgraph = self.rename_operators(modelgraph) # Rename operator layers that are not valid identifiers for C

		rendered = '#define SINGLE_FILE' + '\n' # Used to ignore includes in generated files for combined returned code

		# Write number.h numeric type configuration
		rendered += self.write_numeric_header() + '\n'

		for node in modelgraph.nodes:
			if not self.validator.validate_node(node):
				sys.exit(1)

			if self.fixed_point:
				self.quantizer.quantize_weights(node, activations_range[node.layer] if node.layer in activations_range else None)
				self.quantizer.quantize_activations(node, activations_range[node.layer] if node.layer in activations_range else None)

			# Skip layers with no code to generate
			if self.layer_template_files[node.layer.__class__] is None:
				continue

			rendered += self.write_layer_function(template=self.layer_template_files[node.layer.__class__], node=node) + '\n'
			if len(node.layer.get_weights()) > 0:
				rendered += self.write_layer_weights(template=self.layer_template_files[node.layer.__class__], node=node) + '\n'

		allocator = Allocator()
		allocation = allocator.allocate(modelgraph)

		rendered += self.write_model(modelgraph=modelgraph, allocation=allocation)

		return rendered
