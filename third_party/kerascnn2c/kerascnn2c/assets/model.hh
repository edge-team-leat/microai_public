/**
  ******************************************************************************
  * @file    model.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    08 july 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef __MODEL_H__
#define __MODEL_H__

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define MODEL_OUTPUT_SAMPLES {{ nodes[-1].output_shape[-1] }}
#define MODEL_INPUT_SAMPLES {{ nodes[0].output_shape[-2] }} // node 0 is InputLayer so use its output shape as input shape of the model
#define MODEL_INPUT_CHANNELS {{ nodes[0].output_shape[-1] }}
{% if fixed_point %}
#define MODEL_INPUT_SCALE_FACTOR {{ nodes[0].output_scale_factor }} // scale factor of InputLayer
{% else %}
#define MODEL_INPUT_SCALE_FACTOR 0
{% endif %}

void cnn(
  const number_t input[MODEL_INPUT_CHANNELS][MODEL_INPUT_SAMPLES],
  //{{ nodes[-1].layer.name }}_output_type {{ nodes[-1].layer.name }}_output);
  //float output[MODEL_OUTPUT_SAMPLES]);
  number_t output[MODEL_OUTPUT_SAMPLES]);

#endif//__MODEL_H__
