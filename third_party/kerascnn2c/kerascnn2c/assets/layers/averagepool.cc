/**
  ******************************************************************************
  * @file    averagepool.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define INPUT_CHANNELS  {{ node.input_shape[-1] }}
#define INPUT_SAMPLES   {{ node.input_shape[-2] }}
#define POOL_SIZE       {{ node.layer.pool_size[0] }}
#define POOL_STRIDE     {{ node.layer.strides[0] }}
#define POOL_PAD        0 // Unsupported
#define POOL_LENGTH	    ( ( (INPUT_SAMPLES - POOL_SIZE + (2*POOL_PAD) ) / POOL_STRIDE ) + 1 )

#define ACTIVATION_{{ node.layer.activation.__name__ | upper if node.layer.activation is defined else "LINEAR" }}

// For fixed point quantization
{% if fixed_point %}
#define INPUT_SCALE_FACTOR {{ node.innodes[0].output_scale_factor }}
#define OUTPUT_SCALE_FACTOR {{ node.output_scale_factor }}
{% else %}
#define INPUT_SCALE_FACTOR 0
#define OUTPUT_SCALE_FACTOR 0
{% endif %}

typedef number_t {{ node.layer.name }}_output_type[INPUT_CHANNELS][POOL_LENGTH];

void {{ node.layer.name }}(
  const number_t input[INPUT_CHANNELS][INPUT_SAMPLES], 	    // IN
  number_t output[INPUT_CHANNELS][POOL_LENGTH]) {	// OUT

  unsigned short pos_x, k; 	// loop indexes for output volume
  unsigned short x;
  long_number_t avg, tmp; 

  for (k = 0; k < INPUT_CHANNELS; k++) 
    for (pos_x = 0; pos_x < POOL_LENGTH; pos_x++) {
      tmp = 0;
      for (x = 0; x < POOL_SIZE; x++) {
        tmp += input[k][(pos_x*POOL_STRIDE)+x];
      }
#ifdef ACTIVATION_RELU
      if (tmp < 0) {
        tmp = 0;
      }
#endif
      avg = tmp / POOL_SIZE;
      avg = scale_number_t(avg, INPUT_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
      output[k][pos_x] = clamp_to_number_t(avg);
    }
}

#undef INPUT_CHANNELS  
#undef INPUT_SAMPLES
#undef POOL_SIZE
#undef POOL_STRIDE
#undef POOL_PAD
#undef POOL_LENGTH
#undef ACTIVATION_{{ node.layer.activation.__name__ | upper if node.layer.activation is defined else "LINEAR" }}
#undef INPUT_SCALE_FACTOR
#undef OUTPUT_SCALE_FACTOR
