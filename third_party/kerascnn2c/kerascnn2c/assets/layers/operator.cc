/**
  ******************************************************************************
  * @file    operator.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define OP_{{ node.layer.symbol[14:] | upper }}
#define ACTIVATION_{{ node.layer.activation.__name__ | upper if node.layer.activation is defined else "LINEAR" }}

// For fixed point quantization
{% if fixed_point %}
#define ACC_SCALE_FACTOR {{ node.innodes | max(attribute="output_scale_factor") | attr("output_scale_factor") }} // Get maximum scale factor of previous layers
#define OUTPUT_SCALE_FACTOR {{ node.output_scale_factor }}
{% else %}
#define ACC_SCALE_FACTOR 0
#define OUTPUT_SCALE_FACTOR 0
{% endif %}

//typedef float {{ node.layer.name }}_output_type{% for dim in node.output_shape[1:] %}[{{ dim }}]{% endfor %}; // doesn't work with inverted data_format
// for now always assume 2 dimensions with inverted dims
typedef number_t {{ node.layer.name }}_output_type[{{ node.output_shape[-1] }}][{{ node.output_shape[1] }}];

#ifdef OP_ADD
static inline void {{ node.layer.name }}(
{% for innode in node.innodes %}
  //const float vector_in_{{ loop.index }}{% for dim in node.input_shape[1:] %}[{{ dim }}]{% endfor %}, // doesn't work with inverted data_format
  // for now always assume 2 dimensions with inverted dims
  const number_t vector_in_{{ loop.index }}[{{ node.input_shape[-1] }}][{{ node.input_shape[1] }}],
{% endfor %}
  {{ node.layer.name }}_output_type vector_out) {    // OUT

  int x;
  long_number_t output_acc;

{% for innode in node.innodes %}
  number_t *i_{{ loop.index }} = (number_t*)vector_in_{{ loop.index }};
{% endfor %}

  number_t *o = (number_t*)vector_out;

  for (x = 0; x < {{ node.output_shape[1:] | join('*') }}; x++) {
    // scale all fixed point inputs to same factor and add them, negative factor is left shift
    output_acc = {% for s in node.innodes %}
                    + scale_number_t( (long_number_t)i_{{ loop.index }}[x], {{ s.output_scale_factor if fixed_point else 0 }} - ACC_SCALE_FACTOR)
                 {% endfor %};
    output_acc = scale_number_t(output_acc, ACC_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
#ifdef ACTIVATION_LINEAR
    o[x] = clamp_to_number_t(output_acc);
#elif defined(ACTIVATION_RELU)
    if (output_acc < 0)
      o[x] = 0;
    else
      o[x] = clamp_to_number_t(output_acc);
#endif
  }
}
#endif

#undef OP_{{ node.layer.symbol[14:] | upper }}
#undef ACTIVATION_{{ node.layer.activation.__name__ | upper if node.layer.activation is defined else "LINEAR" }}
#undef ACC_SCALE_FACTOR
#undef OUTPUT_SCALE_FACTOR
