/**
  ******************************************************************************
  * @file    conv.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define INPUT_CHANNELS      {{ node.input_shape[-1] }}
#define INPUT_SAMPLES       {{ node.input_shape[-2] }}
#define CONV_FILTERS        {{ node.layer.filters }}
#define CONV_KERNEL_SIZE    {{ node.layer.kernel_size[0] }}
#define CONV_STRIDE         {{ node.layer.strides[0] }}
{% if node.layer.padding == 'valid' %}
#define ZEROPADDING_LEFT    0
#define ZEROPADDING_RIGHT   0
{% else %}
#define ZEROPADDING_LEFT    {{ node.layer.padding[0] }}
#define ZEROPADDING_RIGHT   {{ node.layer.padding[1] }}
{% endif %}
#define CONV_OUTSAMPLES     ( ( (INPUT_SAMPLES - CONV_KERNEL_SIZE + ZEROPADDING_LEFT + ZEROPADDING_RIGHT) / CONV_STRIDE ) + 1 )

#define ACTIVATION_{{ node.layer.activation.__name__ | upper }}

// For fixed point quantization
{% if fixed_point %}
#define WEIGHTS_SCALE_FACTOR {{ node.weights_scale_factor }}
#define INPUT_SCALE_FACTOR {{ node.innodes[0].output_scale_factor }}
#define OUTPUT_SCALE_FACTOR {{ node.output_scale_factor }}
{% else %}
#define WEIGHTS_SCALE_FACTOR 0
#define INPUT_SCALE_FACTOR 0
#define OUTPUT_SCALE_FACTOR 0
{% endif %}

typedef number_t {{ node.layer.name }}_output_type[CONV_FILTERS][CONV_OUTSAMPLES];

static inline void {{ node.layer.name }}(
  const number_t input[INPUT_CHANNELS][INPUT_SAMPLES],               // IN
  const number_t kernel[CONV_FILTERS][INPUT_CHANNELS][CONV_KERNEL_SIZE], // IN
{% if node.layer.use_bias %}
  const number_t bias[CONV_FILTERS],						                // IN
{% endif %}
  number_t output[CONV_FILTERS][CONV_OUTSAMPLES]) {               // OUT

  unsigned short pos_x, z, k; 	// loop indexes for output volume
  unsigned short x;
  int input_x;
  long_number_t	kernel_mac;
  long_number_t tmp;
  static long_number_t	output_acc[CONV_OUTSAMPLES];

  for (k = 0; k < CONV_FILTERS; k++) { 
    for (pos_x = 0; pos_x < CONV_OUTSAMPLES; pos_x++) { 
{% if node.layer.use_bias %}
      output_acc[pos_x] = scale_number_t((long_number_t)bias[k], -INPUT_SCALE_FACTOR);
{% else %}
      output_acc[pos_x] = 0;
{% endif %}
	    for (z = 0; z < INPUT_CHANNELS; z++) {

        kernel_mac = 0; 
        for (x = 0; x < CONV_KERNEL_SIZE; x++) {
          input_x = pos_x * CONV_STRIDE - ZEROPADDING_LEFT + x;
          if (input_x < 0 || input_x >= INPUT_SAMPLES) // ZeroPadding1D
            tmp = 0;
          else
            tmp = (long_number_t)input[z][input_x] * (long_number_t)kernel[k][z][x]; 
          kernel_mac = kernel_mac + tmp; 
        }

	      output_acc[pos_x] = output_acc[pos_x] + kernel_mac;
      }
      //output_acc[pos_x] = scale_number_t(output_acc[pos_x], OUTPUT_SCALE_FACTOR - INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR);
      //output_acc[pos_x] = scale_number_t(output_acc[pos_x], INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
    }

    for (pos_x = 0; pos_x < CONV_OUTSAMPLES; pos_x++) {
#ifdef ACTIVATION_LINEAR
      output_acc[pos_x] = scale_number_t(output_acc[pos_x], INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
      output[k][pos_x] = clamp_to_number_t(output_acc[pos_x]);
#elif defined(ACTIVATION_RELU)
      // Activation function: ReLU
      if (output_acc[pos_x] < 0) {
        output[k][pos_x] = 0;
      } else {
        output_acc[pos_x] = scale_number_t(output_acc[pos_x], INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
        output[k][pos_x] = clamp_to_number_t(output_acc[pos_x]);
      }
#endif
    }
  }
}

#undef INPUT_CHANNELS
#undef INPUT_SAMPLES
#undef CONV_FILTERS
#undef CONV_KERNEL_SIZE
#undef CONV_STRIDE
#undef ZEROPADDING_LEFT
#undef ZEROPADDING_RIGHT
#undef CONV_OUTSAMPLES
#undef ACTIVATION_{{ node.layer.activation.__name__ | upper }}
#undef WEIGHTS_SCALE_FACTOR
#undef INPUT_SCALE_FACTOR
#undef OUTPUT_SCALE_FACTOR
