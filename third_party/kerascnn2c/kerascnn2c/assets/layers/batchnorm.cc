/**
  ******************************************************************************
  * @file    batchnorm.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 august 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define INPUT_CHANNELS      {{ node.input_shape[-1] }}
#define INPUT_SAMPLES       {{ node.input_shape[1] }}
#define ACTIVATION_{{ node.layer.activation.__name__ | upper if node.layer.activation is defined else "LINEAR" }}

// For fixed point quantization
{% if fixed_point %}
#define WEIGHTS_SCALE_FACTOR {{ node.weights_scale_factor }}
#define INPUT_SCALE_FACTOR {{ node.innodes[0].output_scale_factor }}
#define OUTPUT_SCALE_FACTOR {{ node.output_scale_factor }}
{% else %}
#define WEIGHTS_SCALE_FACTOR 0
#define INPUT_SCALE_FACTOR 0
#define OUTPUT_SCALE_FACTOR 0
{% endif %}

// for now always assume 2 dimensions with inverted dims
typedef number_t {{ node.layer.name }}_output_type[{{ node.output_shape[-1] }}][{{ node.output_shape[1] }}];

void {{ node.layer.name }}(
  const number_t input[INPUT_CHANNELS][INPUT_SAMPLES],  // IN
  const number_t kernel[INPUT_CHANNELS],                // IN
  const number_t bias[INPUT_CHANNELS],                  // IN
  {{ node.layer.name }}_output_type output) {                // OUT

  long_number_t tmp;

  for (int x = 0; x < INPUT_SAMPLES; x++) {
    for (int z = 0; z < INPUT_CHANNELS; z++) {
      tmp = kernel[z] * input[z][x];
      //tmp = scale_number_t(tmp, OUTPUT_SCALE_FACTOR - INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR);
      //tmp = scale_number_t(tmp, INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
      tmp = scale_number_t(tmp, INPUT_SCALE_FACTOR);
      tmp += bias[z];
      tmp = scale_number_t(tmp, WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);

      // Activation function
#ifdef ACTIVATION_LINEAR
      // Linear (MEANS NONE)
      output[z][x] = clamp_to_number_t(tmp);
#elif defined(ACTIVATION_RELU)
      // ReLU
      if (tmp < 0)
        output[z][x] = 0;
      else
        output[z][x] = clamp_to_number_t(tmp);
#endif
    }
  }
}

#undef INPUT_CHANNELS
#undef INPUT_SAMPLES
#undef ACTIVATION_{{ node.layer.activation.__name__ | upper if node.layer.activation is defined else "LINEAR" }}
#undef WEIGHTS_SCALE_FACTOR
#undef INPUT_SCALE_FACTOR
#undef OUTPUT_SCALE_FACTOR
