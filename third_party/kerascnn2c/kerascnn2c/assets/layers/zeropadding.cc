/*
 * UNUSED
 */

/**
  ******************************************************************************
  * @file    zeropadding.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#define INPUT_CHANNELS  {{ node.input_shape[-1] }}
#define INPUT_SAMPLES   {{ node.input_shape[-2] }}
#define PADDING_LEFT    {{ node.layer.padding[0] }}
#define PADDING_RIGHT    {{ node.layer.padding[1] }}
#define PADDING_LENGTH	( INPUT_SAMPLES + PADDING_LEFT + PADDING_RIGHT )

typedef float {{ node.layer.name }}_output_type[INPUT_CHANNELS][PADDING_LENGTH];

void {{ node.layer.name }}(
  const float input[INPUT_CHANNELS][INPUT_SAMPLES], 	    // IN
  float output[INPUT_CHANNELS][PADDING_LENGTH]) {	// OUT

  unsigned short k, x;

  for (k = 0; k < INPUT_CHANNELS; k++) 
    for (x = 0; x < PADDING_LEFT; x++) {
      output[k][x] = 0;
    }
    for (; x < PADDING_LEFT + INPUT_SAMPLES; x++) {
      output[k][x] = input[k][x - PADDING_LEFT];
    }
    for (; x < PADDING_LEFT + INPUT_SAMPLES + PADDING_RIGHT; x++) {
      output[k][x] = 0;
    }
}

#undef INPUT_CHANNELS  
#undef INPUT_SAMPLES
#undef PADDING_SIZE
#undef PADDING_LENGTH
