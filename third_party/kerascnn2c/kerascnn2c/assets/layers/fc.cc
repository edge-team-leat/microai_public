/**
  ******************************************************************************
  * @file    fc.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define INPUT_SAMPLES {{ node.input_shape[-1] }}
#define FC_UNITS {{ node.layer.units }}
#define ACTIVATION_{{ node.layer.activation.__name__ | upper }}

// For fixed point quantization
{% if fixed_point %}
#define WEIGHTS_SCALE_FACTOR {{ node.weights_scale_factor }}
#define INPUT_SCALE_FACTOR {{ node.innodes[0].output_scale_factor }}
#define OUTPUT_SCALE_FACTOR {{ node.output_scale_factor }}
{% else %}
#define WEIGHTS_SCALE_FACTOR 0
#define INPUT_SCALE_FACTOR 0
#define OUTPUT_SCALE_FACTOR 0
{% endif %}

typedef number_t {{ node.layer.name }}_output_type[FC_UNITS];

static inline void {{ node.layer.name }}(
  const number_t input[INPUT_SAMPLES], 			      // IN
	const number_t kernel[FC_UNITS][INPUT_SAMPLES],  // IN
{% if node.layer.use_bias %}
	const number_t bias[FC_UNITS],			              // IN
{% endif %}
	number_t output[FC_UNITS]) {			                // OUT

  unsigned short k, z; 
  long_number_t output_acc; 

  for (k = 0; k < FC_UNITS; k++) { 
{% if node.layer.use_bias %}
    output_acc = scale_number_t((long_number_t)bias[k], -INPUT_SCALE_FACTOR);
{% else %}
    output_acc = 0; 
{% endif %}
    for (z = 0; z < INPUT_SAMPLES; z++) 
      output_acc = output_acc + ( (long_number_t)kernel[k][z] * (long_number_t)input[z] ); 

    //output_acc = scale_number_t(output_acc, OUTPUT_SCALE_FACTOR - INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR);
    //output_acc = scale_number_t(output_acc, INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
    //output_acc = scale_number_t(output_acc, INPUT_SCALE_FACTOR);

    // Activation function
#ifdef ACTIVATION_LINEAR
    // Linear (MEANS NONE)
    output_acc = scale_number_t(output_acc, INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
    output[k] = clamp_to_number_t(output_acc);
#elif defined(ACTIVATION_RELU)
    // ReLU
    if (output_acc < 0) {
      output[k] = 0;
    } else {
      output_acc = scale_number_t(output_acc, INPUT_SCALE_FACTOR + WEIGHTS_SCALE_FACTOR - OUTPUT_SCALE_FACTOR);
      output[k] = clamp_to_number_t(output_acc);
    }
#endif
  }
}

#undef INPUT_SAMPLES
#undef FC_UNITS
#undef ACTIVATION_{{ node.layer.activation.__name__ | upper }}
#undef WEIGHTS_SCALE_FACTOR
#undef INPUT_SCALE_FACTOR
#undef OUTPUT_SCALE_FACTOR
