/**
  ******************************************************************************
  * @file    activation.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#include <math.h>

#ifndef SINGLE_FILE
#include "number.h"
#endif

#define SAMPLES {{ node.input_shape[-1] }}
#define ACTIVATION_{{ node.layer.activation.__name__ | upper }}

// For fixed point quantization
{% if fixed_point %}
#define INPUT_SCALE_FACTOR {{ node.innodes[0].output_scale_factor }}
{% else %}
#define INPUT_SCALE_FACTOR 0
{% endif %}

typedef float {{ node.layer.name }}_output_type[SAMPLES];

#ifdef ACTIVATION_SOFTMAX
void {{ node.layer.name }}(
  const number_t vector_in[SAMPLES], // INT
  float vector_out[SAMPLES]) {    // OUT

  unsigned short k; 
  double sum; 

  sum = 0; 
  for (k=0; k < SAMPLES; k++) 
    sum = sum + exp( (double)vector_in[k] / (1<<INPUT_SCALE_FACTOR) ); 

  for (k=0; k < SAMPLES; k++) 
    vector_out[k] = exp( (double)( (double)vector_in[k] / (1<<INPUT_SCALE_FACTOR) ) ) / sum; 

}
#endif

#undef SAMPLES
#undef ACTIVATION_{{ node.layer.activation.__name__ | upper }}
#undef INPUT_SCALE_FACTOR
