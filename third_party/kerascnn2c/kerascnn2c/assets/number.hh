/**
  ******************************************************************************
  * @file    number.hh
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    2 february 2021
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef __NUMBER_H__
#define __NUMBER_H__

#include <stdint.h>

#define FIXED_POINT	{{ 1 if fixed_point else 0 }}	// Fixed point scaling factor, set to 0 when using floating point
#define NUMBER_MIN	{{ number_min }}	// Max value for this numeric type
#define NUMBER_MAX	{{ number_max }}	// Min value for this numeric type
typedef {{ number_type }} number_t;		// Standard size numeric type used for weights and activations
typedef {{ long_number_type }} long_number_t;	// Long numeric type used for intermediate results

#ifndef min
static inline long_number_t min(long_number_t a, long_number_t b) {
	if (a <= b)
		return a;
	return b;
}
#endif

#ifndef max
static inline long_number_t max(long_number_t a, long_number_t b) {
	if (a >= b)
		return a;
	return b;
}
#endif

#if FIXED_POINT // Scaling/clamping for fixed-point representation

static inline long_number_t scale_number_t(long_number_t number, int scale_factor) {
  if (scale_factor < 0)
    return number << - scale_factor;
  else 
    return number >> scale_factor;
}
static inline number_t clamp_to_number_t(long_number_t number) {
	return (number_t) max(NUMBER_MIN, min(NUMBER_MAX, number));
}

//#define scale_number_t(number, fixed_point) (number >> fixed_point)
//#define clamp_to_number_t(number) ((number_t) max(NUMBER_MIN, min(NUMBER_MAX, number)))

#else // No scaling/clamping required for floating-point
static inline long_number_t scale_number_t(long_number_t number, [[maybe_unused]] int scale_factor) {
	return number;
}
static inline number_t clamp_to_number_t(long_number_t number) {
	return (number_t) number;
}

//#define scale_number_t(number, fixed_point) (number)
//#define clamp_to_number_t(number) ((number_t) number)
#endif

#endif //__NUMBER_H__
