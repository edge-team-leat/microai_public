#include "NeuralNetwork.h"

#include <string.h>
#include <cstdint>
#include <cmath>

extern "C" {
#include "model.h"
}

// input dimensions
static const int dims = MODEL_INPUT_SAMPLES*MODEL_INPUT_CHANNELS;

unsigned int inference_count = 0;

static float input[dims];

extern "C" {

float *serialBufToFloats(char buf[], size_t buflen) {
  auto *pbuf = buf;

  unsigned int i = 0;
  while ((pbuf - buf) < (int)buflen && *pbuf != '\r' && *pbuf != '\n') {
    input[i] = strtof(pbuf, &pbuf);
    i++;
    pbuf++;//skip delimiter
  }

  return input;
}

struct NNResult neuralNetworkInfer(float input[]) {
	number_t inputs[MODEL_INPUT_CHANNELS][MODEL_INPUT_SAMPLES];
	//float outputs[6];
	number_t outputs[MODEL_OUTPUT_SAMPLES];

	// Prepare inputs
	for (size_t i = 0; i < MODEL_INPUT_SAMPLES; i++) {
		for (size_t j = 0; j < MODEL_INPUT_CHANNELS; j++) {
			inputs[j][i] = clamp_to_number_t((long_number_t)floor(input[i*MODEL_INPUT_CHANNELS + j] * (1<<MODEL_INPUT_SCALE_FACTOR)));
		}
	}

	// Run inference
	cnn(inputs, outputs);

	// Get output class
	unsigned int label = 0;
	float max_val = outputs[0];
	for (unsigned int i = 1; i < MODEL_OUTPUT_SAMPLES; i++) {
		if (max_val < outputs[i]) {
			max_val = outputs[i];
			label = i;
		}
	}

	inference_count++;

	return {inference_count, label, max_val};
}

}
