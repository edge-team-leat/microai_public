#!/usr/bin/env python3

from pathlib import Path
import sys
import tensorflow as tf
# We don't need a GPU, don't request it
tf.config.set_visible_devices([], 'GPU')
from tensorflow.keras.models import load_model
from kerascnn2c import Converter

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print(f'Usage: {sys.argv[0]} <model.h5> <output_dir>')
		sys.exit(1)

	#from tensorflow_addons.optimizers import SGDW
	custom_objects = {
		#'Addons>SGDW': SGDW # In case you need to load a model with a custom SGDW optimizer for example
	}

	model = load_model(sys.argv[1], custom_objects=custom_objects)

	Converter(output_path=Path(sys.argv[2])).convert_model(model)

