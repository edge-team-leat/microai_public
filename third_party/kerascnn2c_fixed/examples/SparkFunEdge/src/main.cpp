extern "C" {
#include "am_bsp.h"  // NOLINT
}
#include <cstdlib>
#include "serial.h"

extern "C" {
#include "model.h"
}

int main()
{
	am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_SYSCLK_MAX, 0);
	am_hal_cachectrl_config(&am_hal_cachectrl_defaults);
	am_hal_cachectrl_enable();
	am_bsp_low_power_init();


	unsigned int inference_count = 0;
	uart_init();
	printf("%s\r\n", "READY");

	while (true) {
		static uint32_t msg_len = 0;
		static float input[MODEL_INPUT_SAMPLES*MODEL_INPUT_CHANNELS];
		static number_t inputs[MODEL_INPUT_CHANNELS][MODEL_INPUT_SAMPLES];
		static number_t outputs[MODEL_OUTPUT_SAMPLES];

		do { 
			msg_len = serialBufToFloats(input);
		} while (!msg_len);


		printf("%d\r\n", msg_len);
		// Prepare inputs
		for (size_t i = 0; i < MODEL_INPUT_SAMPLES; i++) {
			for (size_t j = 0; j < MODEL_INPUT_CHANNELS; j++) {
				inputs[j][i] = clamp_to_number_t((long_number_t)(input[i*MODEL_INPUT_CHANNELS + j] * (1<<FIXED_POINT)));
			}
		}

		// Run inference
		cnn(inputs, outputs);

		// Get output class
		unsigned int label = 0;
		float max_val = outputs[0];
		for (unsigned int i = 1; i < MODEL_OUTPUT_SAMPLES; i++) {
			if (max_val < outputs[i]) {
				max_val = outputs[i];
				label = i;
			}
		}

		inference_count++;


		printf("%d,%d,%f\r\n", inference_count, label, (double)max_val); // force double cast to workaround -Werror=double-promotion since printf uses variadic arguments so promotes to double automatically

	}
}
