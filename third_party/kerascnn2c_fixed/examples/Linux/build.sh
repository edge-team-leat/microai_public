#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo "Usage: $0 <C model directory>" 1>&2
	exit 1
fi
g++ -Wall -Wextra -pedantic -o main main.cpp "$1/model.c" -I"$1"
gcc -ftrapv -Wall -Wextra -std=c99 -pedantic -o single single.c "$1/model.c" -I"$1"
