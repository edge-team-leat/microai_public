#include <stdio.h>
#include <stdlib.h>

//#include "output/number.h"
#include "model.h"

int main(int argc, const char*argv[]) {
	if (argc < 2) {
		printf("Usage: %s <test vector>\n", argv[0]);
		return 1;
	}

	number_t input[9][128];
	number_t output[6];

	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 128; j++) {
			input[i][j] = clamp_to_number_t((long_number_t)(strtof(argv[i + j*9 + 1], NULL) * (1<<FIXED_POINT)));
		}
	}

	cnn((const number_t (*)[])input, output);

	for (int i = 0; i < 6; i++) {
		printf("%d\n", output[i]);
	}

	return 0;
}
