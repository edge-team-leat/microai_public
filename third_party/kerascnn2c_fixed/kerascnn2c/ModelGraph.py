from itertools import zip_longest
from tensorflow.keras.layers import InputLayer

class ModelGraph:
	class LayerNode:
		def __init__(self, layer, innodes = None, outnodes = None):
			self.layer = layer
			self.innodes = innodes or []
			self.outnodes = outnodes or []

		@property
		def input_shape(self):
			innode_output_shape = self.innodes[0].output_shape
			assert all(innode.output_shape == innode_output_shape for innode in self.innodes)
			return innode_output_shape

		@property
		def output_shape(self):
			if isinstance(self.layer.output_shape, list): # Sometimes output_shape is a list of tensor, like with InputLayer, return only one tensor
				shape = self.layer.output_shape[0]
				assert all(shape == shape for shape in self.layer.output_shape)
				return shape

			return self.layer.output_shape

		def __repr__(self):
			return self.layer.name

	def __init__(self, nodes = None):
		self.__nodes = nodes or []

	def add_node(self, node, innodes = None, outnodes = None):
		innodes = innodes or []
		outnodes = outnodes or []

		node.innodes.extend(innode for innode in innodes if innode not in node.innodes) # could  be nicer to use a set but we need to keep order
		node.outnodes.extend(outnode for outnode in outnodes if outnode not in node.outnodes)

		for innode in innodes:
			if node not in innode.outnodes:
				innode.outnodes.append(node)
		for outnode in outnodes:
			if node not in outnode.innodes:
				outnode.innodes.append(node)


		assert node not in self.__nodes # Make sure the node is unique
		self.__nodes.append(node)
	
	def delete_node(self, node):
		for innode in node.innodes:
			innode.outnodes.remove(node) # Disconnect layer to remove from output of previous layer
			innode.outnodes.extend(outnode for outnode in node.outnodes if outnode not in innode.outnodes) # Connect outputs from layer to remove to output of previous layer
		for outnode in node.outnodes:
			outnode.innodes.remove(node) # Disconnect layer to remove from input of next layer
			outnode.innodes.extend(innode for innode in node.innodes if innode not in outnode.innodes) # Connect inputs from layer to remove to input of next layer
		self.__nodes.remove(node) # Remove layer from list

	# Delete each node for which predicate function is true
	def delete_node_if(self, predicate):
		to_delete = [n for n in self.nodes if predicate(n)]
		for n in to_delete:
			self.delete_node(n)

	def replace_node(self, oldnode, newnode):
		self.add_node(newnode, oldnode.innodes, oldnode.outnodes)
		self.delete_node(node)

	def find_node_from_layer(self, layer):
		nodes = [node for node in self.nodes if node.layer == layer]
		assert len(nodes) == 1
		return nodes[0]

	def add_layer(self, layer, inlayers = None, outlayers = None):
		inlayers = inlayers or []
		outlayers = outlayers or []

		# Special case for missing InputLayer in case of Sequential model
		for inlayer in inlayers:
			# If InputLayer does not exist in graph
			if isinstance(inlayer, InputLayer) and inlayer not in [n.layer for n in self.nodes]:
				self.add_layer(inlayer) # No input, linking to output is handled by the next add_node

		innodes = [self.find_node_from_layer(inlayer) for inlayer in inlayers]
		outnodes = [self.find_node_from_layer(outlayer) for outlayer in outlayers]

		self.add_node(self.LayerNode(layer), innodes, outnodes)

	@classmethod
	def from_keras(cls, model):
		def get_layer_input_layers(layer):
			return [inb[0] for n in layer._inbound_nodes for inb in n.iterate_inbound()]

		graph = ModelGraph()
		for layer in model.layers:
			graph.add_layer(layer, inlayers=get_layer_input_layers(layer))
		return graph

	@property
	def nodes(self):
		return self.__nodes

	def __str__(self):
		header = f'{"Inputs": <32} | {"Layer": <32} | {"Outputs": <32}\n'
		s = '—'*len(header) + '\n'
		s += header
		s += '—'*len(header) + '\n'
		for node in self.nodes:
			for inlayername, layername, outlayername in zip_longest([n.layer.name for n in node.innodes], [node.layer.name], [n.layer.name for n in node.outnodes], fillvalue=''):
				s += f'{inlayername: <32} | {layername: <32} | {outlayername: <32}\n'
			s += '-'*len(header) + '\n'
		return s
