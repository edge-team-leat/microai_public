from collections import namedtuple

from tensorflow.keras.layers import Flatten

class Allocator:
	AllocInfo = namedtuple('AllocInfo', ['node', 'input_ai', 'keep_until', 'overwrite_input'])

	def __init__(self):
		pass

	def __get_layer_input_layer(self, layer):
		try:
			return [l for n in layer._inbound_nodes for l in n.inbound_layers]
		except TypeError:
			return [n.inbound_layers for n in layer._inbound_nodes]
	
	def allocate(self, modelgraph):
		pools = [[]]

		alloc_info_list = []

		for i, node in enumerate(modelgraph.nodes[:-1]): # No allocation for input and last layer, allocated by caller
			if isinstance(node.layer, Flatten):
				overwrite_input = True
			else:
				overwrite_input = False

			if i == 0: # first layer, assume it takes input from outside model
				inlayersi = []
			else:
				inlayersi = [alloc_info_list[modelgraph.nodes.index(innode)] for innode in node.innodes]

			outlayersi = [modelgraph.nodes.index(outnode) for outnode in node.outnodes]
			keep_until = max(outlayersi)

			alloc_info_list.append(Allocator.AllocInfo(
						node,
						inlayersi,
						keep_until,
						overwrite_input))

		for i, a in enumerate(alloc_info_list[1:]): # Skip InputLayer
			if i == 0: # first layer after input layer, assume it takes input from outside model
				pools[0].append(a)
			else:
				if a.overwrite_input:
					if len(a.input_ai) != 1:
						raise ValueError('Need exactly one input layer when overwriting input')
					# Find which pool contains input
					inp = [p for p in pools if a.input_ai[0] in p]
					if len(inp) != 1:
						raise ValueError('Input layer must be allocated in exactly one pool')
					inp[0].append(a)
				else:
					# Find pools not containing inputs
					ap = [p for p in pools for iai in a.input_ai if iai not in p]
					# Find pools that can be overwritten
					op = [p for p in ap if p[-1].keep_until <= i]

					if len(op) < 1: # no free pool, allocate new one
						pools.append([a])
					else: # Add to first usable pool — maybe possible to optimize allocation size
						op[0].append(a)
		return {
			'pools': [[a.node for a in p] for p in pools],
			'index': {a.node: (i + 1) for i, p in enumerate(pools) for a in p}
			}
