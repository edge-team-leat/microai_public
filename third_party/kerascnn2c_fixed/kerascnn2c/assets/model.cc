/**
  ******************************************************************************
  * @file    model.cc
  * @author  Pierre-Emmanuel Novac <penovac@unice.fr>, LEAT, CNRS, Université Côte d'Azur, France
  * @version 1.0.0
  * @date    24 march 2020
  * @brief   Template generating plain C code for the implementation of Convolutional Neural Networks on MCU
  */

#ifndef SINGLE_FILE
#include "number.h"
#include "model.h"

{% for node in nodes[1:] %} // InputLayer is excluded
#include "{{ node.layer.name }}.c"
  {%- if node.layer.get_weights() | length > 0 %}
#include "weights/{{ node.layer.name }}.c"
  {%- endif %}
{%- endfor %}
#endif

void cnn(
  const number_t input[MODEL_INPUT_CHANNELS][MODEL_INPUT_SAMPLES],
  {{ nodes[-1].layer.name }}_output_type {{ nodes[-1].layer.name }}_output) {

  // Output array allocation
{%- for pool in allocation.pools %}
  static union {
  {%- for node in pool %}
    {{ node.layer.name }}_output_type {{ node.layer.name }}_output;
  {%- endfor %}
  } activations{{ loop.index }};
{% endfor %}

  //static union {
//{% for node in nodes[:-1] %}
//    static {{ node.layer.name }}_output_type {{ node.layer.name }}_output;
//{%- endfor %}
  //} activations;

  // Model layers call chain
{% for node in nodes[1:] %} // InputLayer is excluded 
  {{ node.layer.name }}(
    {% if loop.index == 1 %} // First layer uses input passed as model parameter
    input,
    {%- else %}
      {%- for innode in node.innodes %}
    activations{{ allocation.index[innode] }}.{{ innode.layer.name }}_output,
      {%- endfor %}
    {%- endif %}
    {%- if node.layer.kernel is defined %}
    {{ node.layer.name}}_kernel,
    {%- endif %}
    {%- if node.layer.bias is defined and node.layer.use_bias %}
    {{ node.layer.name}}_bias,
    {%- endif %}
    {%- if node != nodes[-1] %}
    activations{{ allocation.index[node] }}.{{ node.layer.name }}_output
    {%- else %} // Last layer uses output passed as model parameter
    {{ node.layer.name }}_output
    {%- endif %}
  );
{% endfor %}
}

