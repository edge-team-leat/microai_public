import sys
import math
from pathlib import Path
from packaging import version
import numpy as np

import tensorflow as tf
from tensorflow.keras.activations import relu, softmax

# In TensorFlow 2.6.0, Keras is being extract out again
if version.parse(tf.__version__) >= version.parse('2.6.0'):
	from keras.layers import Layer, ZeroPadding1D, Conv1D, AveragePooling1D, MaxPooling1D, Activation, Flatten, Dense, BatchNormalization
	try:
		from keras.src.engine.input_layer import InputLayer # Keras >= 2.13.1
		from keras.src.layers.core import TFOpLambda
	except ImportError:
		from keras.engine.input_layer import InputLayer # Keras < 2.13.0
		from keras.layers.core import TFOpLambda
else:
	from tensorflow.keras.layers import Conv1D, MaxPooling1D, AveragePooling1D, Dense, Flatten, Activation, ZeroPadding1D, BatchNormalization, Input
	from tensorflow.python.keras.engine.input_layer import InputLayer
	from tensorflow.python.keras.layers.core import TFOpLambda

import jinja2

from .Allocator import Allocator
from .Validator import Validator
from .ModelGraph import ModelGraph

class Converter:
	layer_template_files = {
		AveragePooling1D: 'averagepool.cc',
		Conv1D: 'conv.cc',
		Dense: 'fc.cc',
		MaxPooling1D: 'maxpool.cc',
		Activation: 'activation.cc',
		Flatten: 'flatten.cc',
		TFOpLambda: 'operator.cc',
		ZeroPadding1D: 'zeropadding.cc',
		BatchNormalization: 'batchnorm.cc',
		InputLayer: None # Nothing to generate for input layer
	}

	def __init__(self,
		template_path: Path=Path(__file__).parent/'assets',
		output_path: Path=Path('output'),
		write_file: bool=True,

		# Default floating point config # fixed point 8 bits, fixed point 16 bits
		fixed_point: int=0, # 4, 9
		number_type: str='float', # 'int8_t', 'int16_t'
		long_number_type: str='float', # 'int16_t', 'int32_t'
		number_min: float=-math.inf, # -(2**7), -(2**15)
		number_max: float=math.inf # (2**7)-1, (2**15)-1
		):

		self.validator = Validator()

		self.output_data_format = 'channels_first'

		self.template_path = template_path
		self.output_path = output_path
		self.output_path_weights = output_path/'weights'

		self.output_path.mkdir(parents=True, exist_ok=True)
		self.output_path_weights.mkdir(parents=True, exist_ok=True)

		self.write_file = write_file

		self.fixed_point = fixed_point
		self.number_type = number_type
		self.long_number_type = long_number_type
		self.number_min = number_min
		self.number_max = number_max

	def get_layer_input_layer(self, layer):
		try:
			return [l for n in layer._inbound_nodes for l in n.inbound_layers]
		except TypeError:
			return [n.inbound_layers for n in layer._inbound_nodes]

	def dtype2ctype(self, dtype):
		if dtype == np.float32:
			return 'float'
		elif dtype == np.float64:
			return 'double'
		elif dtype == np.int32:
			return 'uint32_t'
		elif dtype == np.int64:
			return 'uint64_t'

	def ndarray2cinitializer(self, arr):

		if np.isscalar(arr):
			if self.fixed_point > 0: # Fixed point int16, scaling/clamping
				arr *= (1<<self.fixed_point)
				arr = int(arr)
				arr = max(self.number_min, min(self.number_max, arr))
				return str(arr)
			else: # use hex for exact representation, no scaling/clamping, gets promoted to float64 from float32 but no big deal
				return float(arr).hex()

		text = ''
		text += '{'

		text += ', '.join([self.ndarray2cinitializer(subarr) for subarr in arr])

		text += '}'
		text += '\n'
		return text

	def tensor2carray(self, arr, name):
		arrdata = self.ndarray2cinitializer(arr)

		return {'name': name, 'data': arrdata, 'dtype': self.number_type, 'shape': arr.shape} #dtype2ctype(arr.dtype)}

	def weights2carray(self, layer):
		weights = {}
		if hasattr(layer, 'kernel'):
			arr = layer.kernel.numpy()
			if self.output_data_format == 'channels_first':
				arr = arr.swapaxes(0, -1) # C implementation uses channels_first data format, keras is usually channels_last
			weights['kernel'] = self.tensor2carray(arr, layer.name + '_kernel')
		if hasattr(layer, 'bias') and layer.use_bias:
			arr = layer.bias.numpy()
			weights['bias'] = self.tensor2carray(arr, layer.name + '_bias')
		return weights

	def write_layer_function(self, template, node):
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path/'layers')).get_template(template)
		rendered = template.render(node=node)
		if self.write_file:
			with (self.output_path/f'{node.layer.name}.c').open('w') as f:
				f.write(rendered)
		return rendered

	def write_layer_weights(self, template, node):
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path/'layers'/'weights')).get_template(template)
		rendered = template.render(node=node, weights=self.weights2carray(node.layer))
		if self.write_file:
			with (self.output_path_weights/f'{node.layer.name}.c').open('w') as f:
				f.write(rendered)
		return rendered

	def write_model(self, modelgraph, allocation):
		# Model header
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path)).get_template('model.hh')
		rendered1 = template.render(nodes=modelgraph.nodes, allocation=allocation)
		if self.write_file:
			with (self.output_path/'model.h').open('w') as f:
				f.write(rendered1)

		# Model implementation
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path)).get_template('model.cc')
		rendered2 = template.render(nodes=modelgraph.nodes, allocation=allocation)
		if self.write_file:
			with (self.output_path/'model.c').open('w') as f:
				f.write(rendered2)
		return rendered1 + '\n' + rendered2

	def write_numeric_header(self):
		template = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.template_path)).get_template('number.hh')
		rendered = template.render(**self.__dict__)
		if self.write_file:
			with (self.output_path/'number.h').open('w') as f:
				f.write(rendered)
		return rendered

	def combine_zeropadding(self, modelgraph):
		zeropaddingnodes = set(node for node in modelgraph.nodes if isinstance(node.layer, ZeroPadding1D))
		for zeropaddingnode in zeropaddingnodes:
			for outnode in zeropaddingnode.outnodes:
				outnode.layer.padding = zeropaddingnode.layer.padding
			modelgraph.delete_node(zeropaddingnode)
		return modelgraph

	def combine_relu(self, modelgraph):
		relunodes = set(node for node in modelgraph.nodes if isinstance(node.layer, Activation) and node.layer.activation == relu)
		for relunode in relunodes:
			for innode in relunode.innodes:
				innode.layer.activation = relu
			modelgraph.delete_node(relunode)
		return modelgraph

	def reformat_fc_weights_data(self, modelgraph):
		for node in modelgraph.nodes:
			if isinstance(node.layer, Flatten): # After Flatten comes Dense, must reshape weights and swap axes for channels_first
				# After Flatten comes Dense, must reshape weights and swap axes for channels_first
				for outnode in node.outnodes:
					dense = outnode.layer
					assert isinstance(dense, Dense)
					kernel = dense.kernel.numpy()
					kernel = kernel.reshape(node.layer.input_shape[1:] + (dense.units, )) # reshape using Flatten input shape (for example last Conv output)
					kernel = kernel.swapaxes(0, 1)
					kernel = kernel.reshape((-1, dense.units))
					dense.set_weights([kernel] + dense.get_weights()[1:])
		return modelgraph

	# Convert BatchNorm mean, variance, offset and scale to kernel and bias for optimization
	def convert_batchnorm_weights(self, modelgraph):
		for node in modelgraph.nodes:
			if not isinstance(node.layer, BatchNormalization):
				continue

			stdev = tf.math.sqrt(node.layer.moving_variance + node.layer.epsilon)
			node.layer.kernel = node.layer.gamma / stdev
			node.layer.bias = node.layer.beta - node.layer.gamma * node.layer.moving_mean / stdev
			node.layer.use_bias = True
		return modelgraph

	# Operators (Add…) layers have names invalid as C tokens
	def rename_operators(self, modelgraph):
		for node in modelgraph.nodes:
			if not isinstance(node.layer, TFOpLambda):
				continue

			node.layer._name = node.layer.name.replace('.', '')
		return modelgraph

	def convert_model(self, model):
		modelgraph = ModelGraph.from_keras(model)
		print(modelgraph)

		modelgraph = self.combine_zeropadding(modelgraph) # Combine ZeroPadding with next layer (Conv1D)
		modelgraph = self.combine_relu(modelgraph) # Combine ReLu with previous layer (Conv1D/Dense)

		print('After optimization:')
		print(modelgraph)

		if self.output_data_format == 'channels_first':
			modelgraph = self.reformat_fc_weights_data(modelgraph)
		modelgraph = self.convert_batchnorm_weights(modelgraph)

		modelgraph = self.rename_operators(modelgraph)

		rendered = ''
		rendered += '#define SINGLE_FILE' + '\n' # Used to ignore includes in generated files

		# Write number.h numeric type configuration
		rendered += self.write_numeric_header() + '\n'

		for node in modelgraph.nodes:
			if not self.validator.validate_node(node):
				sys.exit(1)

			# Skip layers with no code to generate
			if self.layer_template_files[node.layer.__class__] is None:
				continue

			rendered += self.write_layer_function(template=self.layer_template_files[node.layer.__class__], node=node) + '\n'
			if len(node.layer.get_weights()) > 0:
				rendered += self.write_layer_weights(template=self.layer_template_files[node.layer.__class__], node=node) + '\n'

		allocator = Allocator()
		allocation = allocator.allocate(modelgraph)

		rendered += self.write_model(modelgraph=modelgraph, allocation=allocation)
		return rendered
