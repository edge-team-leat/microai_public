####################################################################################################
MicroAI - Copyright (C) 2021 CNRS/UCA/ELLCIE HEALTHY. All Rights Reserved
#..................................................................................................#
#----------------------------------------CITATION FORMAT-------------------------------------------#
@software{microai,                
  author       = {Novac, Pierre-Emmanuel and Pegatoquet, Alain and Miramond, Benoît},     
  title        = {{MicroAI, a software framework for end-to-end deep neural networks training, quantization and deployment onto embedded devices}},    
  month        = sept,
  year         = 2021,
  publisher    = {Zenodo},
  version      = {1.0},   
  doi          = {10.5281/zenodo.5507397},
  url          = {https://doi.org/10.5281/zenodo.5507397}
}
#..................................................................................................#
MicroAI is a software framework for end-to-end deep neural networks training, quantization and deployment onto embedded devices. This framework is designed as an alternative to existing proprietary inference engines on microcontrollers. Our framework can be easily adjusted and/or extended for specific use cases. The training phase relies on Keras or PyTorch. Execution using single precision 32-bit floating-point as well as fixed-point on 8- and 16 bits integers are supported.
#..................................................................................................#
#-------------------------------------------REFERENCES---------------------------------------------#
MicroAI software is involved in the following projects:
- PE Novac, GB Hacene, A Pegatoquet, B Miramond, V Gripon, Quantization and Deployment of Deep Neural Networks on Microcontrollers, MDPI Sensors 21 (9), 2984, april 2021
- PE Novac, A Castagnetti, A Russo, B Miramond, A Pegatoquet, F Verdier, Toward unsupervised Human Activity Recognition on Microcontroller Units, 2020 23rd Euromicro Conference on Digital System Design (DSD), 542-550.
#..................................................................................................#

