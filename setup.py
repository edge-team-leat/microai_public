from setuptools import setup, find_namespace_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="microai",
    version="1.0.0",
    author="Pierre-Emmanuel Novac, Université Côte d'Azur, CNRS, LEAT",
    author_email="penovac@unice.fr",
    description="MicroAI toolchain",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="AGPLv3",
    url="https://bitbucket.org/edge-team-leat/microai_public",
    project_urls={
        "Source Code": "https://bitbucket.org/edge-team-leat/microai_public",
    },
    packages=find_namespace_packages(),
    include_package_data=True,
    scripts=['bin/microai'],
    python_requires='>=3.8',
    install_requires=[
        'numpy',
        'scikit-learn',
        'pycryptodome',
        'pyserial',
        'tomlkit==0.7.0',
        'colorful',
        'gitpython',
    ],
    extras_require={
        'tensorflow': ['tensorflow'],
        'pytorch': ['pytorch', 'pytorch_lightning==1.2.8'],
        'gtsrb': ['imageio', 'scikit-image'],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3"
        "Operating System :: OS Independent",
    ]
)
